#!/bin/bash

# Stop docker containers containing l1p5 website and database

# @author  Fabrice Jammes

set -euo pipefail

DIR=$(cd "$(dirname "$0")"; pwd -P)
. $DIR/conf.sh

# Stop containers with label "$CONTAINER_L1P5" from conf.sh
containers=$(docker ps --format "{{.Names}}" --filter "label=$CONTAINER_L1P5")
if [ -z "$containers" ]
then
    echo "WARN: no container to stop"
else
    echo "Stop container(s):"
fi
for c in $containers
do
    docker stop "$c"
done

# Remove containers with label "$CONTAINER_L1P5" from conf.sh
containers=$(docker ps -a --format "{{.Names}}" --filter "label=$CONTAINER_L1P5")
if [ -z "$containers" ]
then
    echo "WARN: no container to remove"
else
    echo "Remove container(s):"
fi
for c in $containers
do
    docker rm "$c"
done
