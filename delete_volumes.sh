#!/bin/bash

# Remove docker volumes used by l1p5 database

# @author  Fabrice Jammes

set -euo pipefail

DIR=$(cd "$(dirname "$0")"; pwd -P)
. $DIR/conf.sh

# Delete Docker volumes with label "$CONTAINER_L1P5"
volumes=$(docker volume ls --filter "label=$CONTAINER_L1P5" --format "{{.Name}}")
if [ -z "$volumes" ]
then
    echo "WARN: no volumes to delete"
else
    echo "Delete volumes(s):"
fi
for c in $volumes
do
    docker volume rm "$c"
done

