import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

import MainNavbar from '@/layout/MainNavbar.vue'
import OnlyHeader from '@/layout/OnlyHeader.vue'
import MainFooter from '@/layout/MainFooter.vue'

import '../../node_modules/nprogress/nprogress.css'

Vue.use(VueRouter)

const requireAuthenticated = (to, from, next) => {
  if (!store.getters['authentication/isAuthenticated']) {
    next('/')
  } else {
    next()
  }
}

// const requireUnauthenticated = (to, from, next) => {
//   if (store.getters['authentication/isAuthenticated']) {
//     next('/')
//   } else {
//     next()
//   }
// }

const router = new VueRouter({
  mode: 'history',
  hashbang: false,
  linkExactActiveClass: 'active',
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return { selector: to.hash }
      // Or for Vue 3:
      // return {el: to.hash}
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: () => import('@/views/Home.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Plateforme Labos 1point5',
        titleen: 'Labos 1point5 plateform',
        subtitlefr: 'Bienvenue sur la plateforme applicative de Labos 1point5 qui offre une suite logicielle permettant de faire un bilan de gaz à effet de serre et de faire connaître les initiatives en lien avec la réduction de l’empreinte environnementale, en particulier sur le climat.',
        subtitleen: 'Welcome to the Labos 1point5 application platform, which offers a software suite that allows you to perform a greenhouse gas inventory and to publicize initiatives related to the reduction of your environmental footprint, particularly on the climate.',
        home: true
      }
    },
    {
      path: '/les-initiatives',
      name: 'les-initiatives',
      components: {
        default: () => import('@/views/LesInitiatives.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Les initiatives',
        titleen: 'Initiatives'
      }
    },
    {
      path: '/administration/',
      name: 'administration',
      components: {
        default: () => import('@/views/Administration.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      beforeEnter: requireAuthenticated,
      meta: {
        titlefr: 'Administration',
        titleen: 'Administration'
      }
    },
    {
      path: '/administration/:name',
      name: 'administration-named',
      components: {
        default: () => import('@/views/Administration.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Administration',
        titleen: 'Administration'
      }
    },
    {
      path: '/ges-1point5/',
      name: 'ges-1point5',
      components: {
        default: () => import('@/views/GES1point5.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'GES 1point5',
        titleen: 'GES 1point5'
      }
    },
    {
      path: '/ges-1point5/:id',
      name: 'ges-1point5-id',
      components: {
        default: () => import('@/views/GES1point5.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      beforeEnter: requireAuthenticated,
      meta: {
        titlefr: 'GES 1point5',
        titleen: 'GES 1point5'
      }
    },
    {
      path: '/ges-1point5-results/:uuid',
      name: 'ges-1point5-results',
      components: {
        default: () => import('@/views/GES1point5Results.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'GES 1point5',
        titleen: 'GES 1point5'
      }
    },
    {
      path: '/scenario-1point5/',
      name: 'scenario-1point5',
      components: {
        default: () => import('@/views/Scenario1point5.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Scénario 1point5',
        titleen: 'Scenario 1point5'
      }
    },
    {
      path: '/scenario-1point5/:uuid',
      name: 'scenario-1point5-uuid',
      components: {
        default: () => import('@/views/Scenario1point5.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      beforeEnter: requireAuthenticated,
      meta: {
        titlefr: 'Scénario 1point5',
        titleen: 'Scenario 1point5'
      }
    },
    {
      path: '/politique-de-confidentialite',
      name: 'politique-de-confidentialite',
      components: {
        default: () => import('@/views/PrivacyPolicy.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Politique de confidentialité',
        titleen: 'Privacy policy'
      }
    },
    {
      path: '/terms-of-use',
      name: 'terms-of-use',
      components: {
        default: () => import('@/views/TermsOfUse.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Charte d\'utilisation',
        titleen: 'Terms of use'
      }
    },
    {
      path: '/labos1point5-charter',
      name: 'labos1point5-charter',
      components: {
        default: () => import('@/views/Labos1point5Charter.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Charte Labos 1point5',
        titleen: 'Labos 1point5 charter'
      }
    },
    {
      path: '/commutes-survey/:uuid',
      name: 'commutes-survey',
      components: {
        default: () => import('@/views/CommutesSurvey.vue'),
        header: OnlyHeader,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Questionnaire domicile / travail',
        titleen: 'Commute survey'
      }
    },
    {
      path: '/commutes-simulator',
      name: 'commutes-simulator',
      components: {
        default: () => import('@/views/CommutesSimulator.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Simulateur',
        titleen: 'Commutes simulator'
      }
    },
    {
      path: '/travels-simulator',
      name: 'travels-simulator',
      components: {
        default: () => import('@/views/TravelsSimulator.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Simulateur',
        titleen: 'Travels simulator'
      }
    },
    {
      path: '/purchases-simulator',
      name: 'purchases-simulator',
      components: {
        default: () => import('@/views/PurchasesSimulator.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Simulateur',
        titleen: 'Purchases simulator'
      }
    },
    {
      path: '/reset-password',
      name: 'reset-password',
      components: {
        default: () => import('@/views/ResetPassword.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Mot de passe oublié ?',
        titleen: 'Forgot your password ?'
      }
    },
    {
      path: '/reset-password/accounts/reset/:id/:token',
      name: 'reset-password-link',
      components: {
        default: () => import('@/views/ResetPassword.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Mot de passe oublié ?',
        titleen: 'Forgot your password ?'
      }
    },
    {
      path: '/activation-compte/accounts/activate_account/:id/:token',
      name: 'activation-compte',
      components: {
        default: () => import('@/views/ActivationCompte.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Activation de compte',
        titleen: 'Account activation'
      }
    },
    {
      path: '*',
      name: 'not-found',
      components: {
        default: () => import('@/views/404.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Page non trouvé',
        titleen: 'Page not found'
      }
    }
  ]
})

router.beforeResolve((to, from, next) => {
  if (to.name && from.name !== null) {
    router.app.$nprogress.start()
  }
  next()
})

router.afterEach((to, from) => {
  setTimeout(() => router.app.$nprogress.done(), 500)
})

export default router
