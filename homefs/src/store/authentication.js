import authenticationService from '@/services/authenticationService'
import axios from 'axios'
import router from '@/router'

const state = {
  token: localStorage.getItem('user-token') || '',
  status: '',
  isSuperUser: false,
  isAuthModalOpen: false
}

const getters = {
  isAuthenticated: state => !!state.token,
  token: state => state.token,
  authStatus: state => state.status,
  isSuperUser: state => state.isSuperUser,
  isAuthModalOpen: state => state.isAuthModalOpen
}

const actions = {
  resetAllStore ({ dispatch, rootGetters, commit, state }) {
    dispatch('ghgi/resetState', null, { root: true })
    dispatch('introduction/resetState', null, { root: true })
    dispatch('vehicles/resetState', null, { root: true })
    dispatch('buildings/resetState', null, { root: true })
    dispatch('commutes/resetState', null, { root: true })
    dispatch('travels/resetState', null, { root: true })
    dispatch('superadmin/resetState', null, { root: true })
    dispatch('admin/resetState', null, { root: true })
    dispatch('computerdevices/resetState', null, { root: true })
    dispatch('purchases/resetState', null, { root: true })
    dispatch('api/resetState', null, { root: true })
    dispatch('scenario/resetState', null, { root: true })
  },
  authRequest ({ dispatch, rootGetters, commit, state }, user) {
    return authenticationService.authenticate(user)
      .then(data => {
        const token = data.access
        localStorage.setItem('user-token', token)
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
        commit('AUTH_SUCCESS', token)
        dispatch('resetAllStore')
        dispatch('authentication/isSuperUser', null, { root: true })
        router.push('/administration/all-ghgi').catch(() => {})
        return data
      })
      .catch(error => {
        commit('AUTH_ERROR')
        localStorage.removeItem('user-token')
        throw error
      })
  },
  authLogout ({ dispatch, rootGetters, commit, state }) {
    return new Promise((resolve, reject) => {
      commit('AUTH_LOGOUT')
      delete axios.defaults.headers.common['Authorization']
      localStorage.removeItem('user-token')
      dispatch('resetAllStore')
      router.push('/').catch(() => {})
      resolve()
    })
  },
  isSuperUser ({ commit }) {
    return authenticationService.isSuperUser()
      .then(data => {
        commit('IS_SUPER_USER', data.is_super_user)
        return data.is_super_user
      })
      .catch(error => {
        commit('IS_SUPER_USER', false)
        throw error
      })
  },
  openAuthModal ({ commit }) {
    commit('OPEN_AUTHMODAL')
  },
  closeAuthModal ({ commit }) {
    commit('CLOSE_AUTHMODAL')
  }
}

const mutations = {
  AUTH_REQUEST (state) {
    state.status = 'loading'
  },
  AUTH_SUCCESS (state, token) {
    state.status = 'success'
    state.token = token
  },
  AUTH_ERROR (state) {
    state.status = 'error'
  },
  AUTH_LOGOUT (state) {
    state.token = ''
  },
  IS_SUPER_USER (state, value) {
    state.isSuperUser = value
  },
  OPEN_AUTHMODAL (state) {
    state.isAuthModalOpen = true
  },
  CLOSE_AUTHMODAL (state) {
    state.isAuthModalOpen = false
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
