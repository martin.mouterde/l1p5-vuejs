/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import scenarioService from '@/services/scenarioService'
import Scenario from '@/models/scenario/Scenario.js'

function initialState () {
  return {
    item: {}
  }
}

const state = initialState()

const getters = {
  item: state => state.item
}

const actions = {
  set ({ dispatch, rootGetters, commit, state }, uuid) {
    return scenarioService.getScenario({
      uuid: uuid
    })
      .then(scenario => {
        let ghgi = rootGetters['admin/allGHGI'].filter(obj => obj.id === scenario[0].ghgi)[0]
        commit('SET', scenario[0])
        commit('SET_GHGI', ghgi)
        commit('COMPUTE', ghgi)
        return scenario[0]
      })
      .catch(error => {
        throw error
      })
  },
  setEmpty ({ commit }) {
    commit('SET_EMPTY')
  },
  setGHGI ({ commit }, ghgi) {
    commit('SET_GHGI', ghgi)
  },
  setVariables ({ commit }, variables) {
    commit('SET_VARIABLES', variables)
  },
  setLevel2 ({ commit }, level2) {
    commit('SET_LEVEL2', level2)
  },
  resetLevel2 ({ commit }, level2) {
    commit('RESET_LEVEL2', level2)
  },
  compute ({ dispatch, rootGetters, commit, state }) {
    state.item.compute()
  },
  save ({ dispatch, rootGetters, commit, state }, ghgiID) {
    return scenarioService.saveScenario({
      'scenario': state.item.toDatabase(),
      'ghgi_id': ghgiID
    })
      .then(scenario => {
        let ghgi = rootGetters['admin/allGHGI'].filter(obj => obj.id === scenario.ghgi)[0]
        commit('SET', scenario)
        commit('SET_GHGI', ghgi)
        commit('COMPUTE', ghgi)
        return scenario
      })
      .catch(error => {
        throw error
      })
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  SET (state, value) {
    state.item = Scenario.createFromObj(value)
  },
  SET_NAME (state, value) {
    state.item.name = value
  },
  SET_DESCRIPTION (state, value) {
    state.item.description = value
  },
  SET_GHGI (state, ghgi) {
    state.item.setGHGI(ghgi)
  },
  SET_VARIABLES (state, variables) {
    state.item.setVariables(variables)
  },
  SET_EMPTY (state) {
    state.item = new Scenario(null, Scenario.DEFAULT_NAME, null, null, [], null)
  },
  SET_LEVEL2 (state, level2) {
    let variable2update = state.item.variables.filter(obj => obj.name === level2.name)[0]
    for (let param of Object.keys(level2.values)) {
      variable2update.level2[param].value = level2.values[param]
    }
  },
  RESET_LEVEL2 (state, level2) {
    let variable2update = state.item.variables.filter(obj => obj.name === level2.name)[0]
    for (let param of Object.keys(variable2update.level2)) {
      variable2update.level2[param].value = null
    }
  },
  COMPUTE (state) {
    state.item.compute()
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
