
import carbonService from '@/services/carbonService'
import transitionService from '@/services/transitionService'
import GHGI from '@/models/carbon/GHGI.js'

function initialState () {
  return {
    allGHGI: [],
    allInitiatives: []
  }
}

const state = initialState()

const getters = {
  allGHGI: state => state.allGHGI,
  allInitiatives: state => state.allInitiatives
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  getAllGHGI ({ commit }, data) {
    return carbonService.getAllGHGIAdmin(data)
      .then(allGHGI => {
        commit('SET_ALL_GHGI', allGHGI)
        return allGHGI
      })
      .catch(error => {
        throw error
      })
  },
  unsubmitData ({ commit }, data) {
    return carbonService.unsubmitData(data)
      .then(allGHGI => {
        commit('SET_ALL_GHGI', allGHGI)
        return allGHGI
      })
      .catch(error => {
        throw error
      })
  },
  getAllGHGIWithConsumptions ({ commit }, data) {
    return carbonService.getAllGHGIWithConsumptionsAdmin(data)
      .then(allGHGI => {
        commit('SET_ALL_GHGI', allGHGI)
        return allGHGI
      })
      .catch(error => {
        throw error
      })
  },
  getAllInitiatives ({ commit }) {
    return transitionService.getInitiativesAdmin()
      .then(allInitiatives => {
        commit('SET_ALL_INITIATIVES', allInitiatives)
        return allInitiatives
      })
      .catch(error => {
        throw error
      })
  },
  valdidateInitiative ({ commit }, initiative) {
    return transitionService.validateInitiative(initiative)
      .then(allInitiatives => {
        commit('SET_ALL_INITIATIVES', allInitiatives)
        return allInitiatives
      })
      .catch(error => {
        throw error
      })
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  SET_ALL_GHGI (state, allGHGI) {
    state.allGHGI = []
    for (let ghgi of allGHGI) {
      let cghgi = GHGI.createFromObj(ghgi)
      cghgi.compute()
      state.allGHGI.unshift(cghgi)
    }
  },
  SET_ALL_INITIATIVES (state, allInitiatives) {
    state.allInitiatives = allInitiatives
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
