import apiService from '@/services/apiService'

const state = {
  laboratory: {},
  disciplines: [],
  administrations: []
}

const getters = {
  administrations: state => state.administrations,
  disciplines: state => state.disciplines,
  laboratory: state => state.laboratory
}

const actions = {
  saveLaboratory ({ commit }, laboratory) {
    return apiService.saveLaboratory(laboratory)
      .then(data => {
        data['sites'] = data.sites.map(function (obj) { return obj.name })
        commit('UPDATE_LABORATORY', data)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  updateLaboratory ({ commit }, laboratory) {
    commit('UPDATE_LABORATORY', laboratory)
  },
  getLaboratory ({ commit }) {
    return apiService.getLaboratory()
      .then(data => {
        if (data) {
          data['sites'] = data.sites.map(function (obj) { return obj.name })
          commit('UPDATE_LABORATORY', data)
        }
        return data
      })
      .catch(error => {
        throw error
      })
  },
  getDisciplines ({ commit }) {
    apiService.getDisciplines()
      .then(disciplines => {
        commit('SET_DISCIPLINES', disciplines)
      })
  },
  getAdministrations ({ commit }) {
    apiService.getAdministrations()
      .then(administrations => {
        commit('SET_ADMINISTRATIONS', administrations)
      })
  },
  addAdministration ({ commit }, administration) {
    apiService.addAdministration(administration)
      .then(() => {
        commit('ADD_ADMINISTRATION', administration)
      })
  }
}

const mutations = {
  UPDATE_LABORATORY (state, laboratory) {
    state.laboratory = laboratory
  },
  SET_DISCIPLINES (state, disciplines) {
    state.disciplines = disciplines
  },
  SET_ADMINISTRATIONS (state, administrations) {
    state.administrations = administrations
  },
  ADD_ADMINISTRATION (state, administration) {
    state.administrations.push(administration)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
