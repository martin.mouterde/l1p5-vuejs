
import carbonService from '@/services/carbonService'
import ComputerDevice from '@/models/carbon/ComputerDevice.js'

function initialState () {
  return {
    module: {
      isValid: false,
      isModified: false,
      erreur: '',
      computed: false
    },
    items: [],
    results: {},
    isActive: false
  }
}

const state = initialState()

const getters = {
  module: state => state.module,
  items: state => state.items,
  results: state => state.results,
  hasItems: state => state.items.length > 0,
  computed: state => state.module.computed,
  isActive: state => state.isActive
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  add ({ commit }, item) {
    commit('ADD', item)
  },
  update ({ commit }, items) {
    commit('UPDATE', items)
  },
  delete ({ commit }, items) {
    commit('DELETE', items)
  },
  updateAll ({ commit }, computerDevices) {
    commit('UPDATE_ALL', computerDevices)
  },
  save ({ dispatch, rootGetters, commit, state }) {
    let allData = {
      'devices': state.items.map(obj => obj.toDatabase()),
      'ghgi_id': rootGetters['ghgi/item']['id']
    }
    return carbonService.saveComputerDevices(allData)
      .then(data => {
        commit('UPDATE_ALL', data)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  compute ({ dispatch, rootGetters, commit, state }) {
    commit('UPDATE_RESULTS', {})
    let emissions = ComputerDevice.compute(state.items)
    commit('COMPUTED', true)
    commit('UPDATE_RESULTS', emissions)
  },
  forceComputation ({ commit }) {
    commit('COMPUTED', false)
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  UPDATE_RESULTS (state, computerDevices) {
    state.results = computerDevices
  },
  ADD (state, item) {
    state.items.unshift(ComputerDevice.fromEcodiag(item))
  },
  UPDATE (state, items) {
    for (let item of items) {
      let device = state.items.find(obj => Boolean(obj.ecodiagObject) && obj.ecodiagObject.id === item.id)
      if (device) {
        Object.assign(device, ComputerDevice.fromEcodiag(item))
      } else {
        state.items.unshift(ComputerDevice.fromEcodiag(item))
      }
    }
  },
  DELETE (state, items) {
    for (let item of items) {
      let index = state.items.findIndex(obj => Boolean(obj.ecodiagObject) && obj.ecodiagObject.id === item.id)
      if (index >= 0) {
        state.items.splice(index, 1)
      }
    }
  },
  UPDATE_ALL (state, computerDevices) {
    state.items = []
    for (let device of computerDevices) {
      state.items.unshift(new ComputerDevice(
        device.id,
        device.type,
        device.model,
        device.amount,
        device.acquisitionYear
      ))
    }
  },
  ISVALID (state, value) {
    state.module.isValid = value
  },
  ISMODIFIED (state, value) {
    state.module.isModified = value
    state.module.computed = false
  },
  ERREUR (state, value) {
    state.module.erreur = value
  },
  COMPUTED (state, value) {
    state.module.computed = value
  },
  RESET_MODULE (state) {
    state.module.isValid = false
    state.module.isModified = false
    state.module.erreur = ''
    state.module.computed = false
  },
  ISACTIVE (state, value) {
    state.isActive = value
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
