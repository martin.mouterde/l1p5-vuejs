/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import carbonService from '@/services/carbonService'
import Purchase from '@/models/carbon/Purchase.js'

function initialState () {
  return {
    module: {
      isValid: false,
      isModified: false,
      erreur: '',
      computed: false
    },
    items: [],
    results: {}
  }
}

const state = initialState()

const getters = {
  module: state => state.module,
  items: state => state.items,
  results: state => state.results,
  hasItems: state => state.items.length > 0,
  computed: state => state.module.computed,
  submitOk: state => state.items.length > 0,
  validItems: state => (source = null) => {
    if (source) {
      return state.items.filter(item => item.score === 1 && item.source === source)
    } else {
      return state.items.filter(item => item.score === 1)
    }
  },
  otherModulesItems: state => (source = null) => {
    if (source) {
      return state.items.filter(item => item.score === 0 && item.source === source)
    } else {
      return state.items.filter(item => item.score === 0)
    }
  },
  invalidItems: state => (source = null) => {
    if (source) {
      return state.items.filter(item => item.score === 2 && item.source === source)
    } else {
      return state.items.filter(item => item.score === 2)
    }
  },
  allItems: state => (source = null) => {
    if (source) {
      return state.items.filter(item => item.source === source)
    } else {
      return state.items
    }
  }
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  add ({ commit }, purchase) {
    commit('ADD', purchase)
  },
  update ({ commit }, purchase) {
    commit('UPDATE', purchase)
  },
  delete ({ commit }, purchase) {
    commit('DELETE', purchase)
  },
  deleteInvalidItems ({ commit }) {
    commit('DELETE_INVALID_ITEMS')
  },
  deleteSource ({ commit }, source) {
    commit('DELETE_SOURCE', source)
  },
  updateAll ({ commit }, purchases) {
    commit('UPDATE_ALL', purchases)
  },
  savePurchases ({ dispatch, rootGetters, commit, state }) {
    let toDatabase = []
    for (let item of state.items) {
      toDatabase.unshift(item.toDatabase())
    }
    toDatabase = Purchase.reduce(toDatabase)
    let allData = {
      'purchases': toDatabase,
      'ghgi_id': rootGetters['ghgi/item']['id']
    }
    return carbonService.savePurchases(allData)
      .then(data => {
        commit('UPDATE_ALL', data)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  compute ({ dispatch, rootGetters, commit, state }) {
    commit('UPDATE_RESULTS', {})
    let emissions = Purchase.compute(state.items, rootGetters['ghgi/item']['year'])
    commit('COMPUTED', true)
    commit('UPDATE_RESULTS', emissions)
  },
  forceComputation ({ commit }) {
    commit('COMPUTED', false)
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  ADD (state, purchase) {
    state.items.unshift(Purchase.createFromObj(purchase))
  },
  DELETE (state, purchase) {
    let index = null
    if (purchase.id === null) {
      index = state.items.findIndex(obj => obj.code === purchase.code)
    } else {
      index = state.items.findIndex(obj => obj.id === purchase.id)
    }
    state.items.splice(index, 1)
  },
  DELETE_INVALID_ITEMS (state) {
    let invalidIndex = []
    for (let index in state.items) {
      if (state.items[index].score === 2) {
        invalidIndex.unshift(index)
      }
    }
    for (let index of invalidIndex) {
      state.items.splice(index, 1)
    }
  },
  DELETE_SOURCE (state, source) {
    if (source) {
      let indexes = []
      for (let index in state.items) {
        if (state.items[index].source === source) {
          indexes.unshift(index)
        }
      }
      for (let index of indexes) {
        state.items.splice(index, 1)
      }
    } else {
      state.items = []
    }
  },
  UPDATE (state, iobj) {
    const item = state.items.find(obj => obj.code === iobj.from.code)
    Object.assign(item, Purchase.createFromObj(iobj.to))
  },
  UPDATE_ALL (state, purchases) {
    state.items = []
    for (let purchase of purchases) {
      state.items.unshift(Purchase.createFromObj(purchase))
    }
  },
  UPDATE_RESULTS (state, results) {
    state.results = results
  },
  ISVALID (state, value) {
    state.module.isValid = value
  },
  ISMODIFIED (state, value) {
    state.module.isModified = value
    state.module.computed = false
  },
  ERREUR (state, value) {
    state.module.erreur = value
  },
  COMPUTED (state, value) {
    state.module.computed = value
  },
  RESET_MODULE (state) {
    state.module.isValid = false
    state.module.isModified = false
    state.module.erreur = ''
    state.module.computed = false
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
