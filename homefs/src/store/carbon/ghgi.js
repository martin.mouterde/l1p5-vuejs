/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import GHGI from '@/models/carbon/GHGI.js'
import carbonService from '@/services/carbonService'

function initialState () {
  return {
    module: {
      isValid: false,
      isModified: false,
      erreur: ''
    },
    item: {
      id: null,
      uuid: null,
      buildingsSubmitted: false,
      commutesSubmitted: false,
      devicesSubmitted: false,
      travelsSubmitted: false,
      vehiclesSubmitted: false,
      purchasesSubmitted: false,
      surveyCloneYear: null,
      year: null,
      surveyMessage: null,
      nResearcher: 0,
      nProfessor: 0,
      nEngineer: 0,
      nStudent: 0,
      budget: 0
    }
  }
}

const state = initialState()

const getters = {
  module: state => state.module,
  item: state => state.item,
  isClonedSurvey: state => state.item.surveyCloneYear !== null,
  nMember: state => {
    return state.item.nResearcher +
      state.item.nProfessor +
      state.item.nEngineer +
      state.item.nStudent
  }
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  updateGHGI ({ dispatch, rootGetters, commit, state }, ghgi) {
    dispatch('admin/updateGHGIDescription', ghgi, { root: true })
    commit('UPDATE_GHGI', ghgi)
  },
  set ({ dispatch, rootGetters, commit, state }, id) {
    return carbonService.getGHGIConsumptions({ 'ghgi_id': id })
      .then(data => {
        dispatch('updateGHGI', data['ghgi'])
        dispatch('api/updateLaboratory', data['ghgi'].laboratory, { root: true })
        dispatch('vehicles/updateAll', data['vehicles'], { root: true })
        dispatch('buildings/updateAll', data['buildings'], { root: true })
        dispatch('commutes/updateAll', data['commutes'], { root: true })
        dispatch('travels/updateAll', data['travels'], { root: true })
        dispatch('computerdevices/updateAll', data['devices'], { root: true })
        dispatch('purchases/updateAll', data['purchases'], { root: true })
        return data
      })
      .catch(error => {
        throw error
      })
  },
  setFromUUID ({ dispatch, rootGetters, commit, state }, uuid) {
    return carbonService.getGHGIConsumptionsByUUID({ 'uuid': uuid })
      .then(data => {
        dispatch('updateGHGI', data['ghgi'])
        dispatch('api/updateLaboratory', data['ghgi'].laboratory, { root: true })
        dispatch('vehicles/updateAll', data['vehicles'], { root: true })
        dispatch('buildings/updateAll', data['buildings'], { root: true })
        dispatch('commutes/updateAll', data['commutes'], { root: true })
        dispatch('travels/updateAll', data['travels'], { root: true })
        dispatch('computerdevices/updateAll', data['devices'], { root: true })
        dispatch('purchases/updateAll', data['purchases'], { root: true })
        return data
      })
      .catch(error => {
        throw error
      })
  },
  saveGHGI ({ dispatch, rootGetters, commit, state }, ghgi) {
    return carbonService.saveGHGI(ghgi)
      .then(data => {
        dispatch('updateGHGI', JSON.parse(JSON.stringify(data)))
        dispatch('admin/getAllGHGI', null, { root: true })
        return data
      })
      .catch(error => {
        throw error
      })
  },
  submitData ({ dispatch, rootGetters, commit, state }, module) {
    return carbonService.submitData({
      'ghgi_id': state.item.id,
      'module': module
    })
      .then(data => {
        dispatch('updateGHGI', JSON.parse(JSON.stringify(data)))
        return data
      })
      .catch(error => {
        throw error
      })
  },
  resetGHGI ({ dispatch, rootGetters, commit, state }) {
    commit('RESET_GHGI')
    dispatch('introduction/resetState', null, { root: true })
    dispatch('vehicles/resetState', null, { root: true })
    dispatch('buildings/resetState', null, { root: true })
    dispatch('travels/resetState', null, { root: true })
    dispatch('commutes/resetState', null, { root: true })
    dispatch('computerdevices/resetState', null, { root: true })
    dispatch('purchases/resetState', null, { root: true })
  },
  forceModulesCumputation ({ dispatch, rootGetters, commit, state }) {
    // when year modified, force calculation to get the right emissions factors
    dispatch('buildings/forceComputation', null, { root: true })
    dispatch('commutes/forceComputation', null, { root: true })
    dispatch('travels/forceComputation', null, { root: true })
    dispatch('vehicles/forceComputation', null, { root: true })
    dispatch('computerdevices/forceComputation', null, { root: true })
    dispatch('purchases/forceComputation', null, { root: true })
  },
  cloneSurvey ({ dispatch, rootGetters, commit, state }, surveyCloneYear) {
    let allData = {
      'surveyCloneYear': surveyCloneYear,
      'ghgi_id': state.item['id']
    }
    return carbonService.cloneSurvey(allData)
      .then(data => {
        dispatch('updateGHGI', JSON.parse(JSON.stringify(data['ghgi'])))
        dispatch('commutes/updateAll', data['commutes'], { root: true })
        return data
      })
      .catch(error => {
        throw error
      })
  },
  activateGHGISurvey ({ dispatch, rootGetters, commit, state }) {
    return carbonService.activateGHGISurvey({ 'ghgi_id': state.item.id })
      .then(data => {
        dispatch('updateGHGI', JSON.parse(JSON.stringify(data)))
        return data
      })
      .catch(error => {
        throw error
      })
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  UPDATE_GHGI (state, ghgi) {
    state.item = GHGI.createFromObj(ghgi)
  },
  RESET_GHGI (state) {
    state.item = {
      id: null,
      year: null,
      nResearcher: 0,
      nProfessor: 0,
      nEngineer: 0,
      nStudent: 0,
      budget: 0
    }
  },
  UPDATE_YEAR (state, year) {
    state.item.year = year
  },
  UPDATE_MESSAGE (state, surveyMessage) {
    state.item.surveyMessage = surveyMessage
  },
  UPDATE_NRESEARCHER (state, nResearcher) {
    state.item.nResearcher = nResearcher
  },
  UPDATE_NPROFESSOR (state, nProfessor) {
    state.item.nProfessor = nProfessor
  },
  UPDATE_NENGINEER (state, nEngineer) {
    state.item.nEngineer = nEngineer
  },
  UPDATE_NSTUDENT (state, nStudent) {
    state.item.nStudent = nStudent
  },
  UPDATE_BUDGET (state, budget) {
    state.item.budget = budget
  },
  ISVALID (state, value) {
    state.module.isValid = value
  },
  ISMODIFIED (state, value) {
    state.module.isModified = value
  },
  ERREUR (state, value) {
    state.module.erreur = value
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
