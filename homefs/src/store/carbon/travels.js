/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import carbonService from '@/services/carbonService'
import TravelSection from '@/models/carbon/TravelSection.js'
import Travel from '@/models/carbon/Travel.js'

function initialState () {
  return {
    module: {
      isValid: false,
      isModified: false,
      erreur: '',
      computed: false
    },
    items: [],
    results: {}
  }
}

const state = initialState()

const getters = {
  module: state => state.module,
  items: state => state.items,
  hasItems: state => state.items.length > 0,
  results: state => state.results,
  computed: state => state.module.computed,
  hasPurposeOption: (state) => (source = null) => {
    let nbWithPurpose = 0
    if (source === null) {
      nbWithPurpose = state.items.filter(travel => travel.purpose !== null && travel.purpose !== Travel.PURPOSE_UNKNOWN).length
    } else {
      nbWithPurpose = state.items.filter(travel => travel.purpose !== null && travel.purpose !== Travel.PURPOSE_UNKNOWN && travel.source === source).length
    }

    return nbWithPurpose > 0
  },
  hasOneMotif: (state) => {
    return state.items.filter(travel => travel.purpose !== null).length !== 0
  },
  hasStatusOption: (state) => (source = null) => {
    let nbWithStatus = 0
    if (source === null) {
      nbWithStatus = state.items.filter(travel => travel.status !== null && travel.status !== Travel.STATUS_UNKNOWN).length
    } else {
      nbWithStatus = state.items.filter(travel => travel.status !== null && travel.status !== Travel.STATUS_UNKNOWN && travel.source === source).length
    }
    return nbWithStatus > 0
  },
  invalidTravels: state => {
    return state.items.filter(travel => !travel.isValid())
  },
  nbValidTravels: (state) => (source) => {
    let validTravels = []
    if (source === null) {
      validTravels = state.items.filter(travel => travel.isValid())
    } else {
      validTravels = state.items.filter(travel => travel.isValid() && travel.source === source)
    }
    let nbValidTravels = 0
    if (validTravels.length > 0) {
      nbValidTravels = validTravels.map(obj => obj.amount).reduce((a, b) => a + b)
    }
    return nbValidTravels
  },
  nbTravels: (state) => (source) => {
    let travels = []
    if (source === null) {
      travels = state.items
    } else {
      travels = state.items.filter(travel => travel.source === source)
    }
    let nbTravels = 0
    if (travels.length > 0) {
      nbTravels = travels.map(obj => obj.amount).reduce((a, b) => a + b)
    }
    return nbTravels
  },
  travelsLoadedFromDatabase: state => {
    return state.items.filter(travel => travel.isFromDatabase()).length !== 0
  },
  submitOk: state => state.items.length > 0
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  add ({ commit }, travel) {
    commit('ADD', travel)
  },
  addFromFile ({ commit }, travel) {
    commit('ADD_FROM_FILE', travel)
  },
  sortSections ({ commit }) {
    commit('SORT_SECTIONS')
  },
  update ({ commit }, obj) {
    commit('UPDATE', obj)
  },
  updateAll ({ commit }, travels) {
    commit('UPDATE_ALL', travels)
  },
  deleteInvalid ({ commit }) {
    commit('DELETE_INVALID')
  },
  deleteSource ({ commit }, source) {
    commit('DELETE_SOURCE', source)
  },
  delete ({ commit }, travel) {
    commit('DELETE', travel)
  },
  save ({ dispatch, rootGetters, commit, state }) {
    let travelsDatabase = []
    for (let travels of Travel.reduce(state.items)) {
      travelsDatabase.unshift(travels.toDatabase())
    }
    let allData = {
      'travels': travelsDatabase,
      'ghgi_id': rootGetters['ghgi/item']['id']
    }
    return carbonService.saveTravels(allData)
      .then(data => {
        commit('UPDATE_ALL', data)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  compute ({ dispatch, rootGetters, commit, state }) {
    commit('UPDATE_RESULTS', {})
    let carbonIntensity = Travel.compute(state.items, rootGetters['ghgi/item']['year'])
    commit('COMPUTED', true)
    commit('UPDATE_RESULTS', carbonIntensity)
  },
  forceComputation ({ commit }) {
    commit('COMPUTED', false)
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  ADD (state, travel) {
    let tindex = state.items.findIndex(obj => obj.names[0] === travel.names[0])
    if (tindex < 0) {
      state.items.unshift(new Travel(
        travel.names,
        travel.date,
        [],
        travel.amount,
        travel.purpose,
        travel.status,
        travel.source
      ))
      tindex = 0
    }
    for (let section of travel.sections) {
      state.items[tindex].addSection(TravelSection.createFromObj(section))
    }
    if (state.items[tindex].sections.length > 1) {
      state.items[tindex].sortSections()
    }
  },
  ADD_FROM_FILE (state, travel) {
    let tindex = state.items.findIndex(obj => obj.names[0] === travel.names[0])
    if (tindex < 0) {
      state.items.unshift(new Travel(
        travel.names,
        travel.date,
        [],
        travel.amount,
        travel.purpose,
        travel.status,
        travel.source
      ))
      tindex = 0
    }
    for (let section of travel.sections) {
      state.items[tindex].addSection(TravelSection.createFromObj(section))
    }
  },
  SORT_SECTIONS (state) {
    for (let tindex in state.items) {
      if (state.items[tindex].sections.length > 1) {
        state.items[tindex].sortSections()
      }
    }
  },
  UPDATE (state, iobj) {
    const item = state.items.find(obj => obj.isEqualTo(iobj.from))
    Object.assign(item, Travel.createFromObj(iobj.to))
  },
  UPDATE_ALL (state, travels) {
    state.items = []
    for (let travel of travels) {
      state.items.unshift(Travel.createFromObj(travel))
    }
  },
  DELETE_INVALID (state) {
    let invalidIndex = []
    for (let index in state.items) {
      if (!state.items[index].isValid()) {
        invalidIndex.unshift(index)
      }
    }
    for (let index of invalidIndex) {
      state.items.splice(index, 1)
    }
  },
  DELETE_SOURCE (state, source) {
    if (source) {
      let indexes = []
      for (let index in state.items) {
        if (state.items[index].source === source) {
          indexes.unshift(index)
        }
      }
      for (let index of indexes) {
        state.items.splice(index, 1)
      }
    } else {
      state.items = []
    }
  },
  DELETE (state, travel) {
    let index = state.items.findIndex(obj => obj.isEqualTo(travel))
    state.items.splice(index, 1)
  },
  UPDATE_RESULTS (state, travels) {
    state.results = travels
  },
  ISVALID (state, value) {
    state.module.isValid = value
  },
  ISMODIFIED (state, value) {
    state.module.isModified = value
    state.module.computed = false
  },
  ERREUR (state, value) {
    state.module.erreur = value
  },
  COMPUTED (state, value) {
    state.module.computed = value
  },
  RESET_MODULE (state) {
    state.module.isValid = false
    state.module.isModified = false
    state.module.erreur = ''
    state.module.computed = false
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
