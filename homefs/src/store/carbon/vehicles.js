/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import carbonService from '@/services/carbonService'
import Vehicle from '@/models/carbon/Vehicle.js'

function initialState () {
  return {
    module: {
      isValid: false,
      isModified: false,
      erreur: '',
      computed: false
    },
    items: [],
    results: {}
  }
}

const state = initialState()

const getters = {
  module: state => state.module,
  items: state => state.items,
  hasItems: state => state.items.length > 0,
  results: state => state.results,
  computed: state => state.module.computed
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  add ({ commit }, vehicle) {
    commit('ADD', vehicle)
  },
  delete ({ commit }, vehicle) {
    commit('DELETE', vehicle)
  },
  update ({ commit }, vehicle) {
    commit('UPDATE', vehicle)
  },
  updateAll ({ commit }, vehicles) {
    commit('UPDATE_ALL', vehicles)
  },
  save ({ dispatch, rootGetters, commit, state }) {
    let allData = {
      'vehicles': state.items,
      'ghgi_id': rootGetters['ghgi/item']['id']
    }
    return carbonService.saveVehicles(allData)
      .then(data => {
        commit('UPDATE_ALL', data)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  compute ({ dispatch, rootGetters, commit, state }) {
    commit('UPDATE_RESULTS', {})
    let emissions = Vehicle.compute(state.items, rootGetters['ghgi/item']['year'])
    commit('COMPUTED', true)
    commit('UPDATE_RESULTS', emissions)
  },
  forceComputation ({ commit }) {
    commit('COMPUTED', false)
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  ADD (state, vehicle) {
    state.items.unshift(Vehicle.createFromObj(vehicle))
  },
  DELETE (state, vehicle) {
    let index = null
    if (vehicle.id === null) {
      index = state.items.findIndex(obj => obj.name === vehicle.name)
    } else {
      index = state.items.findIndex(obj => obj.id === vehicle.id)
    }
    state.items.splice(index, 1)
  },
  UPDATE (state, iobj) {
    const item = state.items.find(obj => obj.name === iobj.from.name)
    Object.assign(item, Vehicle.createFromObj(iobj.to))
  },
  UPDATE_ALL (state, vehicles) {
    state.items = []
    for (let vehicle of vehicles) {
      state.items.unshift(Vehicle.createFromObj(vehicle))
    }
  },
  UPDATE_RESULTS (state, vehicles) {
    state.results = vehicles
  },
  ISVALID (state, value) {
    state.module.isValid = value
  },
  ISMODIFIED (state, value) {
    state.module.isModified = value
    state.module.computed = false
  },
  ERREUR (state, value) {
    state.module.erreur = value
  },
  COMPUTED (state, value) {
    state.module.computed = value
  },
  RESET_MODULE (state) {
    state.module.isValid = false
    state.module.isModified = false
    state.module.erreur = ''
    state.module.computed = false
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
