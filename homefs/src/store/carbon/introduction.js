/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

function initialState () {
  return {
    activeModule: 'introduction',
    module: {
      isValid: false,
      erreur: ''
    }
  }
}

const state = initialState()

const getters = {
  activeModule: state => state.activeModule,
  module: state => state.module
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  SET_ACTIVE_MODULE (state, value) {
    state.activeModule = value
  },
  ISVALID (state, value) {
    state.module.isValid = value
  },
  RESET_MODULE (state) {
    state = initialState()
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
