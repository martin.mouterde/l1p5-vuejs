/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import carbonService from '@/services/carbonService'
import Building from '@/models/carbon/Building.js'

function initialState () {
  return {
    module: {
      isValid: false,
      isModified: false,
      erreur: '',
      computed: false
    },
    items: [],
    results: {}
  }
}

const state = initialState()

const getters = {
  module: state => state.module,
  items: state => state.items,
  results: state => state.results,
  hasItems: state => state.items.length > 0,
  computed: state => state.module.computed,
  submitOk: state => {
    let bwe = state.items.filter(obj => {
      let heatingOK = true
      for (let heating of obj.heatings) {
        if (heating.type !== 'electric') {
          heatingOK = obj.getHeatingConsumption(heating) > 0
        }
      }
      return obj.getElectricityConsumption() > 0 && heatingOK
    })
    if (state.items.length > 0) {
      return state.items.length === bwe.length
    } else {
      return false
    }
  }
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  add ({ commit }, building) {
    commit('ADD', building)
  },
  delete ({ commit }, building) {
    commit('DELETE', building)
  },
  update ({ commit }, building) {
    commit('UPDATE', building)
  },
  updateAll ({ commit }, buildings) {
    commit('UPDATE_ALL', buildings)
  },
  save ({ dispatch, rootGetters, commit, state }) {
    let allData = {
      'buildings': state.items,
      'ghgi_id': rootGetters['ghgi/item']['id']
    }
    return carbonService.saveBuildings(allData)
      .then(data => {
        commit('UPDATE_ALL', data)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  compute ({ dispatch, rootGetters, commit, state }) {
    commit('UPDATE_RESULTS', {})
    let emissions = Building.compute(
      state.items,
      rootGetters['api/laboratory'].area,
      rootGetters['api/laboratory'].country,
      rootGetters['ghgi/item']['year']
    )
    commit('COMPUTED', true)
    commit('UPDATE_RESULTS', emissions)
  },
  forceComputation ({ commit }) {
    commit('COMPUTED', false)
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  ADD (state, building) {
    state.items.unshift(Building.createFromObj(building))
  },
  DELETE (state, building) {
    let index = null
    if (building.id === null) {
      index = state.items.findIndex(obj => obj.name === building.name)
    } else {
      index = state.items.findIndex(obj => obj.id === building.id)
    }
    state.items.splice(index, 1)
  },
  UPDATE (state, iobj) {
    const item = state.items.find(obj => obj.name === iobj.from.name)
    Object.assign(item, Building.createFromObj(iobj.to))
  },
  UPDATE_ALL (state, buildings) {
    state.items = []
    for (let building of buildings) {
      state.items.unshift(Building.createFromObj(building))
    }
  },
  UPDATE_RESULTS (state, buildings) {
    state.results = buildings
  },
  ISVALID (state, value) {
    state.module.isValid = value
  },
  ISMODIFIED (state, value) {
    state.module.isModified = value
    state.module.computed = false
  },
  ERREUR (state, value) {
    state.module.erreur = value
  },
  COMPUTED (state, value) {
    state.module.computed = value
  },
  RESET_MODULE (state) {
    state.module.isValid = false
    state.module.isModified = false
    state.module.erreur = ''
    state.module.computed = false
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
