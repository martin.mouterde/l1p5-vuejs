const state = {
  commutes: []
}

const getters = {
  commutes: state => state.commutes
}

const actions = {
}

const mutations = {
  ADD_COMMUTE (state, commute) {
    state.commutes.push(commute)
  },
  RESET_COMMUTES (state) {
    state.commutes = []
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
