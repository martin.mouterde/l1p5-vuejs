/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import scenarioService from '@/services/scenarioService'
import carbonService from '@/services/carbonService'

import Scenario from '@/models/scenario/Scenario.js'
import GHGI from '@/models/carbon/GHGI.js'

function initialState () {
  return {
    allGHGI: [],
    allScenarios: [],
    allVehicles: [],
    allBuildings: []
  }
}

const state = initialState()

const getters = {
  allGHGI: state => state.allGHGI,
  allScenarios: state => state.allScenarios,
  allVehicles: state => state.allVehicles,
  allBuildings: state => state.allBuildings
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  deleteGHGIDescription ({ commit, getters }, id) {
    return carbonService.deleteGHGIDescription({ 'ghgi_id': id })
      .then(ghgis => {
        return carbonService.getAllGHGI()
          .then(ghgis => {
            commit('UPDATE_ALL_GHGI', ghgis)
            commit('COMPUTE_ALL_GHGI')
            return ghgis
          })
          .catch(error => {
            throw error
          })
      })
      .catch(error => {
        throw error
      })
  },
  getAllGHGI ({ dispatch, rootGetters, commit, state }) {
    return carbonService.getAllGHGI()
      .then(ghgis => {
        commit('UPDATE_ALL_GHGI', ghgis)
        commit('COMPUTE_ALL_GHGI')
        return ghgis
      })
      .catch(error => {
        throw error
      })
  },
  computeAllGHGI ({ commit }, year) {
    commit('COMPUTE_ALL_GHGI', year)
  },
  getScenarios ({ commit }) {
    return scenarioService.getScenarios()
      .then(scenarios => {
        commit('SET_SCENARIOS', scenarios)
        return scenarios
      })
      .catch(error => {
        throw error
      })
  },
  deleteScenario ({ commit }, id) {
    return scenarioService.deleteScenario({
      id: id
    })
      .then(scenarios => {
        commit('SET_SCENARIOS', scenarios)
        return scenarios
      })
      .catch(error => {
        throw error
      })
  },
  getAllVehicles ({ commit }) {
    return carbonService.getAllVehicles()
      .then(data => {
        if (data) {
          commit('UPDATE_VEHICLES', data)
        }
        return data
      })
      .catch(error => {
        throw error
      })
  },
  getAllBuildings ({ commit }) {
    return carbonService.getAllBuildings()
      .then(data => {
        if (data) {
          commit('UPDATE_BUILDINGS', data)
        }
        return data
      })
      .catch(error => {
        throw error
      })
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  COMPUTE_ALL_GHGI (state, year = null) {
    for (let ghgi of state.allGHGI) {
      ghgi.compute(year)
    }
  },
  UPDATE_ALL_GHGI (state, ghgis) {
    state.allGHGI = []
    for (let ghgi of ghgis) {
      state.allGHGI.unshift(GHGI.createFromObj(ghgi))
    }
  },
  SET_SCENARIOS (state, scenarios) {
    state.allScenarios = []
    for (let scenario of scenarios) {
      state.allScenarios.unshift(Scenario.createFromObj(scenario))
    }
  },
  UPDATE_VEHICLES (state, allVehicles) {
    state.allVehicles = allVehicles
  },
  UPDATE_BUILDINGS (state, allBuildings) {
    state.allBuildings = allBuildings
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
