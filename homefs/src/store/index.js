import Vue from 'vue'
import Vuex from 'vuex'

import authentication from './authentication'
import superadmin from './superadmin'
import admin from './admin'

import api from './api'
import transition from './transition'
import scenario from './scenario'

import ghgi from './carbon/ghgi'
import introduction from './carbon/introduction'
import buildings from './carbon/buildings'
import computerdevices from './carbon/computerdevices'
import commutes from './carbon/commutes'
import travels from './carbon/travels'
import vehicles from './carbon/vehicles'
import purchases from './carbon/purchases'
import simulations from './carbon/simulations'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    authentication,
    superadmin,
    admin,
    api,
    transition,
    scenario,
    ghgi,
    introduction,
    buildings,
    commutes,
    computerdevices,
    travels,
    vehicles,
    purchases,
    simulations
  }
})
