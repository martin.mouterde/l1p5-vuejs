import Vue from 'vue'
import VeeValidate, { Validator } from 'vee-validate'
import VueValidationFR from 'vee-validate/dist/locale/fr'
import VueValidationEn from 'vee-validate/dist/locale/en'

Vue.use(VeeValidate, {
  locale: 'fr',
  inject: false,
  dictionary: {
    fr: VueValidationFR,
    en: VueValidationEn
  }
})
const dictionary = {
  fr: {
    messages: {
      required: () => 'Ce champ est obligatoire',
      email: () => 'E-mail non valide',
      numeric: () => 'Chiffres uniquement',
      decimal: () => 'Chiffres uniquement',
      min: (fields, value) => 'Ce champ doit contenir au minimum ' + value + ' caractères',
      min_value: (fields, value) => 'Ce champ doit être supérieur ou égal à ' + value
    }
  },
  en: {
    messages: {
      required: () => 'This field is required',
      email: () => 'Invalid E-mail',
      numeric: () => 'Numbers only',
      decimal: () => 'Numbers only',
      min: (fields, value) => 'This field must contain at least ' + value + ' characters',
      min_value: (fields, value) => 'This field must be greater than or equal to ' + value
    }
  }
}
Validator.localize(dictionary)
