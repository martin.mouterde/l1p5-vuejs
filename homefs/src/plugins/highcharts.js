import Vue from 'vue'
import HighchartsVue from 'highcharts-vue'
import Highcharts from 'highcharts'
import loadDrillDown from 'highcharts/modules/drilldown'
import exporting from 'highcharts/modules/exporting'
import more from 'highcharts/highcharts-more'

loadDrillDown(Highcharts)
exporting(Highcharts)
more(Highcharts)

Vue.use(HighchartsVue, {
  Highcharts
})

/* Highcharts.theme = {
  colors: ['#D14D65', '#8A4869', '#5F548D', '#3B589F'],
  chart: {},
  title: {
    style: {
      color: '#000',
      font: 'bold 2em "Trebuchet MS", Verdana, sans-serif'
    }
  },
  subtitle: {
    style: {
      color: '#666666',
      font: 'bold 14px "Trebuchet MS", Verdana, sans-serif'
    }
  },
  xAxis: {
    title: {
      style: {
        font: 'bold 14px "Trebuchet MS", Verdana, sans-serif',
        color: '#000',
        fontSize: '1.5em'
      }
    },
    labels: {
      style: {
        fontSize: '1.3em'
      }
    }
  },
  yAxis: {
    title: {
      style: {
        font: 'bold 14px "Trebuchet MS", Verdana, sans-serif',
        color: '#000',
        fontSize: '1.5em'
      }
    },
    labels: {
      style: {
        fontSize: '1.3em'
      }
    }
  },
  legend: {
    itemStyle: {
      font: '9pt Trebuchet MS, Verdana, sans-serif',
      color: 'black'
    },
    itemHoverStyle: {
      color: 'gray'
    }
  }
} */
// Apply the theme
// Highcharts.setOptions(Highcharts.theme)
