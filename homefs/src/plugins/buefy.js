import Vue from 'vue'
import Buefy from 'buefy'
import '@/assets/fonts/icomoon/icomoon.css'

Vue.use(Buefy, {
  defaultIconPack: 'sui',
  customIconPacks: {
    'sui': {
      iconPrefix: 'sui-',
      internalIcons: {
        'eye-off': 'eye-slash',
        'alert-circle': 'exclamation-circle',
        'close-circle': 'times-circle'
      },
      sizes: {
        'is-medium': 'is-size-4',
        'is-large': 'is-size-2'
      }
    },
    'icomoon': {
      sizes: {
        'default': 'is-size-4',
        'is-small': 'is-size-5',
        'is-medium': 'is-size-3',
        'is-large': 'is-size-1'
      },
      iconPrefix: 'icon-',
      internalIcons: {
        'motorcycle': 'motorcycle',
        'electric-scooter': 'electric-scooter',
        'electric-bike': 'electric-bike',
        'rer': 'rer',
        'tram': 'tram',
        'subway': 'subway',
        'walk': 'walk'
      }
    }
  }
})
