import api from '@/services/api'

export default {
  resetPassword (payload) {
    return api.post(`reset_password/`, payload)
      .then(response => response.data)
  },
  userExists (payload) {
    return api.post(`user_exists/`, payload)
      .then(response => response.data)
  }
}
