import api from '@/services/api'

export default {
  getCounts () {
    return api.get(`get_counts/`)
      .then(response => response.data)
  },
  saveGHGI (payload) {
    return api.post(`save_ghgi/`, payload)
      .then(response => response.data)
  },
  deleteGHGIDescription (payload) {
    return api.post(`delete_ghgi/`, payload)
      .then(response => response.data)
  },
  getAllGHGI () {
    return api.get(`get_all_ghgi/`)
      .then(response => response.data)
  },
  saveVehicles (payload) {
    return api.post(`save_vehicles/`, payload)
      .then(response => response.data)
  },
  saveComputerDevices (payload) {
    return api.post(`save_computer_devices/`, payload)
      .then(response => response.data)
  },
  savePurchases (payload) {
    return api.post(`save_purchases/`, payload)
      .then(response => response.data)
  },
  saveBuildings (payload) {
    return api.post(`save_buildings/`, payload)
      .then(response => response.data)
  },
  saveCommutes (payload) {
    return api.post(`save_commutes/`, payload)
      .then(response => response.data)
  },
  saveCommute (payload) {
    return api.post(`save_commute/`, payload)
      .then(response => response.data)
  },
  saveSurveyMessage (payload) {
    return api.post(`save_survey_message/`, payload)
      .then(response => response.data)
  },
  getGHGISurveyInfo (payload) {
    return api.post(`get_ghgi_survey_info/`, payload)
      .then(response => response.data)
  },
  saveTravels (payload) {
    return api.post(`save_travels/`, payload)
      .then(response => response.data)
  },
  getGHGIConsumptions (payload) {
    return api.post(`get_ghgi_consumptions/`, payload)
      .then(response => response.data)
  },
  getGHGIConsumptionsByUUID (payload) {
    return api.post(`get_ghgi_consumptions_by_uuid/`, payload)
      .then(response => response.data)
  },
  getAllVehicles () {
    return api.post(`get_all_vehicles/`)
      .then(response => response.data)
  },
  getAllBuildings () {
    return api.post(`get_all_buildings/`)
      .then(response => response.data)
  },
  getAllGHGIAdmin (payload) {
    return api.post(`get_all_ghgi_admin/`, payload)
      .then(response => response.data)
  },
  getAllGHGIWithConsumptionsAdmin (payload) {
    return api.post(`get_all_ghgi_with_consumptions_admin/`, payload)
      .then(response => response.data)
  },
  unsubmitData (payload) {
    return api.post(`unsubmit_data/`, payload)
      .then(response => response.data)
  },
  submitData (payload) {
    return api.post(`submit_data/`, payload)
      .then(response => response.data)
  },
  activateGHGISurvey (payload) {
    return api.post(`activate_ghgi_survey/`, payload)
      .then(response => response.data)
  },
  cloneSurvey (payload) {
    return api.post(`clone_survey/`, payload)
      .then(response => response.data)
  }
}
