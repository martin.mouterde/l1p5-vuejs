/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Travel from '@/models/carbon/Travel.js'
import Commute from '@/models/carbon/Commute.js'
import Variable from '@/models/scenario/Variable.js'

export const VARIABLES = [
  {
    name: 'replace.plane',
    module: 'travels',
    level1: {
      type: 'slider',
      unit: 'km',
      min: 0,
      max: function (travels) {
        let maxDistance = 0
        for (let travel of travels) {
          let cdistance = travel.getDistance()
          if (cdistance > maxDistance) {
            maxDistance = cdistance
          }
        }
        return Math.ceil(maxDistance / 100) * 100
      },
      step: 100,
      value: 0
    },
    level2: {
      position: {
        type: 'select',
        values: Travel.status,
        value: null
      },
      purpose: {
        type: 'select',
        values: Travel.purposes,
        value: null
      }
    },
    function: function (travels, level1, level2) {
      for (let travel of travels) {
        let travelModified = false
        for (let section of travel.sections) {
          if (section.transportation === Travel.MODE_PLANE && section.distance <= level1.value) {
            if (level2.position.value && level2.purpose.value) {
              if (level2.position.value === travel.status && level2.purpose.value === travel.purpose) {
                section.transportation = Travel.MODE_TRAIN
                travelModified = true
              }
            } else if (level2.position.value) {
              if (level2.position.value === travel.status) {
                section.transportation = Travel.MODE_TRAIN
                travelModified = true
              }
            } else if (level2.purpose.value) {
              if (level2.purpose.value === travel.purpose) {
                section.transportation = Travel.MODE_TRAIN
                travelModified = true
              }
            } else {
              section.transportation = Travel.MODE_TRAIN
              travelModified = true
            }
          }
        }
        if (travelModified) {
          this.logging(Variable.ACTION_MODIFIED, travel)
        }
      }
      return travels
    }
  },
  {
    name: 'limit.plane',
    module: 'travels',
    level1: {
      type: 'slider',
      unit: 'km',
      min: 0,
      max: function (travels) {
        let maxDistance = 0
        for (let travel of travels) {
          let cdistance = travel.getDistance()
          if (cdistance > maxDistance) {
            maxDistance = cdistance
          }
        }
        return Math.ceil(maxDistance / 100) * 100
      },
      step: 100,
      value: function (travels) {
        let maxDistance = 0
        for (let travel of travels) {
          let cdistance = travel.getDistance()
          if (cdistance > maxDistance) {
            maxDistance = cdistance
          }
        }
        return Math.ceil(maxDistance / 100) * 100
      }
    },
    level2: {
      position: {
        type: 'select',
        values: Travel.status,
        value: null
      },
      purpose: {
        type: 'select',
        values: Travel.purposes,
        value: null
      }
    },
    function: function (travels, level1, level2) {
      let filteredTravels = []
      for (let travel of travels) {
        if (travel.hasSectionWithPlane() && travel.getDistance() > level1.value) {
          if (level2.position.value && level2.purpose.value) {
            if (level2.position.value !== travel.status && level2.purpose.value !== travel.purpose) {
              filteredTravels.push(travel)
            } else {
              this.logging(Variable.ACTION_REMOVE, travel)
            }
          } else if (level2.position.value) {
            if (level2.position.value !== travel.status) {
              filteredTravels.push(travel)
            } else {
              this.logging(Variable.ACTION_REMOVE, travel)
            }
          } else if (level2.purpose.value) {
            if (level2.purpose.value !== travel.purpose) {
              filteredTravels.push(travel)
            } else {
              this.logging(Variable.ACTION_REMOVE, travel)
            }
          } else {
            this.logging(Variable.ACTION_REMOVE, travel)
          }
        } else {
          filteredTravels.push(travel)
        }
      }
      return filteredTravels
    }
  },
  {
    name: 'replace.plane.national',
    module: 'travels',
    level1: {
      type: 'switch',
      value: false
    },
    function: function (travels, level1, level2) {
      for (let travel of travels) {
        for (let section of travel.sections) {
          if (level1.value && section.type === 'NA' && section.transportation === Travel.MODE_PLANE) {
            section.transportation = Travel.MODE_TRAIN
          }
        }
      }
      return travels
    }
  },
  {
    name: 'annual.collective.quota',
    module: 'travels',
    level1: {
      type: 'slider',
      unit: 'km',
      min: 0,
      max: function (travels) {
        let cumDistance = 0
        for (let travel of travels) {
          if (travel.hasSectionWithPlane()) {
            cumDistance += travel.getDistance()
          }
        }
        return Math.ceil(cumDistance / 1000) * 1000
      },
      step: 1000,
      value: function (travels) {
        let cumDistance = 0
        for (let travel of travels) {
          cumDistance += travel.getDistance()
        }
        return Math.round(cumDistance)
      }
    },
    function: function (travels, level1, level2) {
      let filteredTravels = []
      let cumDistance = 0
      for (let travel of travels) {
        if (!travel.hasSectionWithPlane()) {
          filteredTravels.push(travel)
        } else if (cumDistance + travel.getDistance() <= level1.value) {
          filteredTravels.push(travel)
          cumDistance += travel.getDistance()
        } else {
          this.logging(Variable.ACTION_REMOVE, travel)
        }
      }
      return filteredTravels
    }
  },
  {
    name: 'carpooling',
    module: 'commutes',
    level1: {
      type: 'slider',
      unit: '',
      min: 1,
      max: 4,
      step: 1,
      value: 1
    },
    function: function (commutes, level1, level2) {
      for (let commute of commutes) {
        if (commute.car > 0 && commute.carpooling < level1.value) {
          commute.carpooling = level1.value
        }
        if (commute.car2 > 0 && commute.carpooling2 === 1) {
          commute.carpooling2 = level1.value
        }
      }
      return commutes
    }
  },
  {
    name: 'teleworking',
    module: 'commutes',
    level1: {
      type: 'slider',
      unit: '',
      min: 0,
      max: 5,
      step: 1,
      value: 0
    },
    function: function (commutes, level1, level2) {
      for (let commute of commutes) {
        if (commute.nWorkingDay > 5 - level1.value) {
          commute.nWorkingDay = 5 - level1.value
        }
      }
      return commutes
    }
  },
  {
    name: 'replace.car',
    module: 'commutes',
    level1: {
      type: 'slider',
      unit: '%',
      min: 0,
      max: 100,
      step: 1,
      value: 0
    },
    level2: {
      mode: {
        type: 'select',
        values: Commute.travelsModes.filter(val => val !== 'car'),
        value: null
      }
    },
    function: function (commutes, level1, level2) {
      for (let commute of commutes) {
        if (commute.car > 0 && level2.mode.value !== null) {
          let newDistance = parseInt(commute.car * (100 - level1.value) / 100)
          commute[level2.mode.value] += commute.car - newDistance
          commute.car = newDistance
        }
        if (commute.car2 > 0 && level2.mode.value !== null) {
          let newDistance2 = parseInt(commute.car2 * (100 - level1.value) / 100)
          commute[level2.mode.value + '2'] += commute.car2 - newDistance2
          commute.car2 = newDistance2
        }
      }
      return commutes
    }
  },
  {
    name: 'car.electrification',
    module: 'commutes',
    level1: {
      type: 'slider',
      unit: '%',
      min: 0,
      max: 100,
      step: 1,
      value: 0
    },
    function: function (commutes, level1, level2) {
      let nCar = 0
      for (let commute of commutes) {
        if ((commute.car > 0 && commute.engine !== 'electric') || (commute.car2 > 0 && commute.engine2 !== 'electric')) {
          nCar += 1
        }
      }
      let nCarToKeep = parseInt(nCar * level1.value / 100)
      let nCarElectrified = 0
      if (nCarToKeep > 0) {
        for (let commute of commutes) {
          if (nCarElectrified <= nCarToKeep) {
            let modified = false
            if (commute.car > 0 && commute.engine !== 'electric') {
              commute.engine = 'electric'
              modified = true
            }
            if (commute.car2 > 0 && commute.engine2 !== 'electric') {
              commute.engine2 = 'electric'
              modified = true
            }
            if (modified) {
              nCarElectrified += 1
            }
          }
        }
      }
      return commutes
    }
  }
]
