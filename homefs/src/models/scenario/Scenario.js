/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Travel from '@/models/carbon/Travel.js'
import Vehicle from '@/models/carbon/Vehicle.js'
import Building from '@/models/carbon/Building.js'
import Commute from '@/models/carbon/Commute.js'
import Purchase from '@/models/carbon/Purchase.js'
import Variable from '@/models/scenario/Variable.js'
import ComputerDevice from '@/models/carbon/ComputerDevice.js'

const clonedeep = require('lodash.clonedeep')

const SCENARIO_DEFAULT_NAME = '2030'
const SCENARIOS_MODULES = ['travels', 'commutes']

export default class Scenario {
  constructor (id, name, description, uuid, variables, ghgi = null) {
    this.id = id
    if (name) {
      this.name = name
    } else {
      this.name = SCENARIO_DEFAULT_NAME
    }
    this.description = description
    this.uuid = uuid
    this.setGHGI(ghgi)
    this.setVariables(variables)
  }

  setGHGI (ghgi) {
    if (ghgi !== null) {
      this.ghgi = clonedeep(ghgi)
    } else {
      this.ghgi = null
    }
  }

  setVariables (variables) {
    this.variables = variables.map((obj) => Variable.createFromObj(obj))
    for (let variable of this.variables) {
      variable.setFunctionValues({
        travels: this.ghgi.travels,
        commutes: this.ghgi.commutes
      })
    }
  }
  compute () {
    if (this.ghgi) {
      for (let module of SCENARIOS_MODULES) {
        for (let variable of this.variables.filter(obj => obj.module === module)) {
          this.ghgi[module] = variable.function(this.ghgi[module], variable.level1, variable.level2)
        }
      }
      this.ghgi.emissions = {
        vehicles: Vehicle.compute(this.ghgi.vehicles, this.ghgi.year),
        travels: Travel.compute(this.ghgi.travels, this.ghgi.year),
        commutes: Commute.compute(
          this.ghgi.commutes,
          this.ghgi.laboratory.citySize,
          this.ghgi.nResearcher,
          this.ghgi.nProfessor,
          this.ghgi.nEngineer,
          this.ghgi.nStudent,
          this.ghgi.year
        ),
        buildings: Building.compute(
          this.ghgi.buildings,
          this.ghgi.laboratory.area,
          this.ghgi.laboratory.country,
          this.ghgi.year
        ),
        devices: ComputerDevice.compute(this.ghgi.devices),
        purchases: Purchase.compute(this.ghgi.purchases, this.ghgi.year)
      }
    }
  }

  toDatabase () {
    let variables = []
    for (let variable of this.variables) {
      variables.push(variable.toDatabase())
    }
    return {
      'id': this.id,
      'name': this.name,
      'description': this.description,
      'uuid': this.uuid,
      'variables': variables
    }
  }

  static createFromObj (scenario) {
    let cscenario = new Scenario(
      scenario.id,
      scenario.name,
      scenario.description,
      scenario.uuid,
      scenario.variables,
      scenario.ghgi
    )
    return cscenario
  }
}
