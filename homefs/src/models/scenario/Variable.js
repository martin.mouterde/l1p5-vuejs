/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { VARIABLES } from '@/models/scenario/variables.js'

const ACTION_REMOVE = 'remove'
const ACTION_MODIFIED = 'modified'

export default class Variable {
  constructor (name, level1, level2 = {}) {
    this.name = name
    let cvariable = VARIABLES.filter(obj => obj.name === name)
    if (cvariable.length === 1) {
      Object.assign(this, cvariable[0])
      if (typeof level1 === 'object') {
        this.level1.value = level1.value
      } else {
        this.level1.value = level1
      }
      if (level2.length > 0) {
        for (let key2 of Object.keys(this.level2)) {
          if (Object.keys(level2).includes(key2)) {
            this.level2[key2] = level2[key2]
          }
        }
      }
    }
    this.removed = []
    this.modified = []
  }

  logging (action, item) {
    if (action === ACTION_REMOVE) {
      this.removed.push(item)
    } else if (action === ACTION_MODIFIED) {
      this.modified.push(item)
    }
  }

  setFunctionValues (data) {
    // TODO : do the same with all attributes defined as function
    if (typeof this.level1.max === 'function' && data[this.module] !== undefined) {
      this.level1.max = this.level1.max(data[this.module])
    }
    if (typeof this.level1.value === 'function' && data[this.module] !== undefined) {
      this.level1.value = this.level1.value(data[this.module])
    }
  }

  toDatabase () {
    if (Object.keys(this).includes('level2')) {}
    let toReturn = {
      'name': this.name,
      'level1': this.level1.value
    }
    if (Object.keys(this).includes('level2')) {
      let self = this
      toReturn['level2'] = Object.keys(this.level2).map(function (key, index) {
        let params = {}
        params[key] = self.level2[key].value
        return params
      })
    }
    return toReturn
  }

  static get ACTION_REMOVE () {
    return ACTION_REMOVE
  }

  static get ACTION_MODIFIED () {
    return ACTION_MODIFIED
  }

  static getVariables () {
    return VARIABLES
  }

  static createFromObj (variable) {
    let level2 = variable.level2
    if (level2 === null) {
      level2 = {}
    }
    return new Variable(
      variable.name,
      variable.level1,
      level2
    )
  }
}
