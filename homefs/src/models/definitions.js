/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

export const HEATING_COLOR = '#c84896'
export const ELECTRICITY_COLOR = '#e88bc3'
export const REFRIGERANTS_COLOR = '#fdc2e5'
export const VEHICLES_COLOR = '#fd8e62'
export const TRAVELS_COLOR = '#67c3a5'
export const COMMUTES_COLOR = '#8ea0cc'
export const COMPUTERDEVICES_COLOR = '#a6d953'
export const PURCHASES_COLOR = '#ffda2c'
