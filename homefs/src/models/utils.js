/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

export function filterEFByYear (ef = null, year = null) {
  let yearKey = null
  if (ef.decomposition.length === 1) {
    yearKey = Object.keys(ef.decomposition)[0]
  } else {
    if (year !== null) {
      if (Object.keys(ef.decomposition).includes(String(year))) {
        yearKey = String(year)
      } else {
        // if does not exist, find the closest year available
        let beforeYears = Object.keys(ef.decomposition).filter(val => parseInt(val) <= parseInt(year))
        if (beforeYears.length === 1) {
          yearKey = beforeYears[0]
        } else if (beforeYears.length > 1) {
          let closest = beforeYears.reduce((a, b) => {
            return Math.abs(parseInt(b) - parseInt(year)) < Math.abs(parseInt(a) - parseInt(year)) ? b : a
          })
          yearKey = String(closest)
        } else {
          let closest = Object.keys(ef.decomposition).reduce((a, b) => {
            return Math.abs(parseInt(b) - parseInt(year)) < Math.abs(parseInt(a) - parseInt(year)) ? b : a
          })
          yearKey = String(closest)
        }
      }
    } else {
      // find the closest to current year
      let cYear = new Date().getFullYear()
      let closest = Object.keys(ef.decomposition).reduce((a, b) => {
        return Math.abs(parseInt(b) - parseInt(cYear)) < Math.abs(parseInt(a) - parseInt(cYear)) ? b : a
      })
      yearKey = String(closest)
    }
  }
  ef.decomposition[yearKey].group = ef.group
  return ef.decomposition[yearKey]
}
