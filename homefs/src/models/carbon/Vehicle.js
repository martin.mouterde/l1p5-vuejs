/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { filterEFByYear } from '@/models/utils'
import VEHICLES_FACTORS from '@/../data/vehiclesFactors.json'
import {
  DetailedCarbonIntensity,
  DetailedCarbonIntensities,
  CarbonIntensities
} from '@/models/carbon/CarbonIntensity.js'

const ICONS = {
  'car': 'car',
  'motorbike': 'motorcycle',
  'bike': 'bicycle',
  'scooter': 'electric-scooter',
  'aircraft': 'plane',
  'boat': 'ship'
}

const ICONS_PACK = {
  'car': 'sui',
  'motorbike': 'sui',
  'bike': 'sui',
  'scooter': 'icomoon',
  'aircraft': 'sui',
  'boat': 'sui'
}

export default class Vehicle {
  constructor (id, type, name, engine, consumption, unit, power, noEngine, shp, controled) {
    this.id = id
    this.name = name
    this.type = type
    this.engine = engine
    this.consumption = consumption
    this.unit = unit
    this.power = power
    this.noEngine = noEngine
    this.shp = shp
    this.controled = controled
  }

  _getHelicopterTConsumption (_shp) {
    let cCarburant = 0
    if (this.shp > 1000) {
      cCarburant = 4.0539 * Math.pow(10, -18) * Math.pow(_shp, 5) - 3.16298 * Math.pow(10, -14) * Math.pow(_shp, 4)
      cCarburant = cCarburant + 9.2087 * Math.pow(10, -11) * Math.pow(_shp, 3) - 1.2156 * Math.pow(10, -7) * Math.pow(_shp, 2)
      cCarburant = cCarburant + 1.1476 * Math.pow(10, -4) * _shp + 0.01256
    } else if (this.shp > 600) {
      cCarburant = 3.3158 * Math.pow(10, -16) * Math.pow(_shp, 5) - 1.0175 * Math.pow(10, -12) * Math.pow(_shp, 4)
      cCarburant = cCarburant + 1.1627 * Math.pow(10, -9) * Math.pow(_shp, 3) - 5.9528 * Math.pow(10, -7) * Math.pow(_shp, 2)
      cCarburant = cCarburant + 1.8168 * Math.pow(10, -4) * _shp + 0.0062945
    } else {
      cCarburant = 2.197 * Math.pow(10, -15) * Math.pow(_shp, 5) - 4.4441 * Math.pow(10, -12) * Math.pow(_shp, 4)
      cCarburant = cCarburant + 3.4208 * Math.pow(10, -9) * Math.pow(_shp, 3) - 1.2138 * Math.pow(10, -6) * Math.pow(_shp, 2)
      cCarburant = cCarburant + 2.414 * Math.pow(10, -4) * _shp + 0.004583
    }
    return this.getConsumption() * this.noEngine * 3600 * cCarburant * 1.25
  }

  _getHelicopterPConsumption (_shp) {
    let cCarburant = 1.9 * Math.pow(10, -12) * Math.pow(_shp, 4) - Math.pow(10, -9) * Math.pow(_shp, 3)
    cCarburant = cCarburant + 2.6 * Math.pow(10, -7) * Math.pow(_shp, 2) + 4 * Math.pow(10, -5) * _shp + 0.0006
    return this.getConsumption() * this.noEngine * 3600 * cCarburant * 1.39
  }

  _getPlaneConsumption () {
    return this.getConsumption() * (0.22 * this.power + 0.54)
  }

  getConsumption () {
    let consumption = 0
    if (this.consumption.isMonthly) {
      consumption += parseInt(this.consumption.january) + parseInt(this.consumption.february)
      consumption += parseInt(this.consumption.march) + parseInt(this.consumption.april)
      consumption += parseInt(this.consumption.may) + parseInt(this.consumption.june)
      consumption += parseInt(this.consumption.july) + parseInt(this.consumption.august)
      consumption += parseInt(this.consumption.septembre) + parseInt(this.consumption.octobre)
      consumption += parseInt(this.consumption.novembre) + parseInt(this.consumption.decembre)
    } else {
      consumption = this.consumption.total
    }
    return consumption
  }

  getEmissionFactor (type, engine, year) {
    let ef = filterEFByYear(VEHICLES_FACTORS[type][engine], year)
    return ef
  }

  getCarbonIntensity (GHGIYear) {
    let consumption = this.getConsumption()
    let _shp = null
    if (this.engine === 'helicopter.pistons') {
      _shp = this.shp * 0.9
    } else if (this.engine === 'helicopter.turbines') {
      if (this.noEngine === 1) {
        _shp = this.shp * 0.8
      } else {
        if (this.shp > 1000) {
          _shp = this.shp * 0.62
        } else {
          _shp = this.shp * 0.65
        }
      }
    } else {
      _shp = this.shp
    }
    if (this.type === 'aircraft') {
      if (this.engine === 'helicopter.pistons') {
        consumption = this._getHelicopterPConsumption(_shp)
      } else if (this.engine === 'helicopter.turbines') {
        consumption = this._getHelicopterTConsumption(_shp)
      } else {
        consumption = this._getPlaneConsumption()
      }
    }
    let ef = this.getEmissionFactor(
      this.type,
      this.engine,
      GHGIYear
    )
    return new DetailedCarbonIntensity(
      consumption * ef.combustion.co2,
      consumption * ef.combustion.ch4,
      consumption * ef.combustion.n2o,
      consumption * ef.combustion.others,
      consumption * ef.combustion.total,
      consumption * ef.combustion.total * ef.combustion.uncertainty,
      consumption * ef.upstream.total,
      consumption * ef.upstream.total * ef.upstream.uncertainty,
      consumption * ef.manufacturing.total,
      consumption * ef.manufacturing.total * ef.manufacturing.uncertainty,
      ef.group
    )
  }

  static getIcon (type) {
    return ICONS[type]
  }

  static getIconPack (type) {
    return ICONS_PACK[type]
  }

  static getTypes () {
    let types = Object.keys(VEHICLES_FACTORS)
    return Array.from(new Set(types))
  }

  static getEngines (type) {
    let engines = Object.keys(VEHICLES_FACTORS[type])
    return Array.from(new Set(engines))
  }

  static getUnits (type, engine) {
    return VEHICLES_FACTORS[type][engine].unit
  }

  static createFromObj (vehicle) {
    return new Vehicle(
      vehicle.id,
      vehicle.type,
      vehicle.name,
      vehicle.engine,
      vehicle.consumption,
      vehicle.unit,
      vehicle.power,
      vehicle.noEngine,
      vehicle.shp,
      vehicle.controled
    )
  }

  static compute (vehicles, GHGIYear) {
    let controled = new DetailedCarbonIntensities()
    let uncontroled = new DetailedCarbonIntensities()
    let intensities = new CarbonIntensities()
    let meta = []
    for (let vehicle of vehicles) {
      let intensity = vehicle.getCarbonIntensity(GHGIYear)
      intensities.add(intensity)
      if (vehicle.controled) {
        controled.add(intensity)
      } else {
        uncontroled.add(intensity)
      }
      meta.unshift({
        'name': vehicle.name,
        'type': vehicle.type,
        'engine': vehicle.engine,
        'consumption': vehicle.getConsumption(),
        'unit': vehicle.unit,
        'intensity': intensity
      })
    }
    return {
      'controled': controled.sum(),
      'uncontroled': uncontroled.sum(),
      'intensity': intensities.sum(),
      'meta': meta
    }
  }
}
