/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { filterEFByYear } from '@/models/utils'
import {
  CarbonIntensities,
  DetailedCarbonIntensity
} from '@/models/carbon/CarbonIntensity.js'
import VEHICLES_FACTORS from '@/../data/vehiclesFactors.json'
import TRANSPORTS_FACTORS from '@/../data/transportsFactors.json'

const TRAVELS_MODES = [
  'walking',
  'bike',
  'ebike',
  'escooter',
  'motorbike',
  'car',
  'bus',
  'busintercity',
  'tram',
  'train',
  'expressrailway',
  'subway'
]

const MAX_DISTANCES = {
  'walking': 60,
  'bike': 200,
  'ebike': 200,
  'escooter': 200,
  'motorbike': 2000,
  'car': 2000,
  'bus': 2000,
  'busintercity': 2000,
  'tram': 300,
  'train': 2000,
  'expressrailway': 2000,
  'subway': 250
}

const ICONS = {
  'walking': 'walk',
  'bike': 'bicycle',
  'ebike': 'electric-bike',
  'escooter': 'electric-scooter',
  'motorbike': 'motorcycle',
  'car': 'car',
  'bus': 'bus',
  'busintercity': 'bus',
  'tram': 'tram',
  'train': 'train',
  'expressrailway': 'rer',
  'subway': 'subway'
}

const ICONS_PACK = {
  'walking': 'icomoon',
  'bike': 'sui',
  'ebike': 'icomoon',
  'escooter': 'icomoon',
  'motorbike': 'icomoon',
  'car': 'sui',
  'bus': 'sui',
  'busintercity': 'sui',
  'tram': 'icomoon',
  'train': 'sui',
  'expressrailway': 'icomoon',
  'subway': 'icomoon'
}

const NUMBER_OF_WORKED_WEEK = {
  default: 41,
  2020: 27,
  2021: 37
}

export default class Commute {
  constructor (seqID, position, nWorkingDay, message, walking, bike, ebike, escooter,
    motorbike, car, bus, busintercity, tram, train, expressrailway, subway, motorbikepooling, carpooling, engine,
    hasWorkingDay2, nWorkingDay2, walking2, bike2, ebike2, escooter2, motorbike2, car2,
    bus2, busintercity2, tram2, train2, expressrailway2, subway2, motorbikepooling2, carpooling2, engine2, deleted) {
    this.seqID = seqID
    this.position = position
    this.nWorkingDay = nWorkingDay
    this.message = message

    this.walking = walking
    this.bike = bike
    this.ebike = ebike
    this.escooter = escooter
    this.motorbike = motorbike
    this.car = car
    this.bus = bus
    this.busintercity = busintercity
    this.tram = tram
    this.train = train
    this.expressrailway = expressrailway
    this.subway = subway
    this.motorbikepooling = motorbikepooling
    this.carpooling = carpooling
    this.engine = engine

    this.hasWorkingDay2 = hasWorkingDay2
    this.nWorkingDay2 = nWorkingDay2
    this.walking2 = walking2
    this.bike2 = bike2
    this.ebike2 = ebike2
    this.escooter2 = escooter2
    this.motorbike2 = motorbike2
    this.car2 = car2
    this.bus2 = bus2
    this.busintercity2 = busintercity2
    this.tram2 = tram2
    this.train2 = train2
    this.expressrailway2 = expressrailway2
    this.subway2 = subway2
    this.motorbikepooling2 = motorbikepooling2
    this.carpooling2 = carpooling2
    this.engine2 = engine2

    this.deleted = deleted
    this.isValid = true
  }

  getNumberOfWorkedWeek (year) {
    let nbWeek = NUMBER_OF_WORKED_WEEK.default
    if (year in NUMBER_OF_WORKED_WEEK) {
      nbWeek = NUMBER_OF_WORKED_WEEK[year]
    }
    return nbWeek
  }

  isCarpooling () {
    let isCarpooling = false
    if (this.car > 0 && this.carpooling > 1) {
      isCarpooling = true
    } else if (this.car2 > 0 && this.carpooling2 > 1) {
      isCarpooling = true
    } else if (this.motorbike > 0 && this.motorbikepooling > 1) {
      isCarpooling = true
    } else if (this.motorbike2 > 0 && this.motorbikepooling2 > 1) {
      isCarpooling = true
    }
    return isCarpooling
  }

  getNbDayType1 () {
    let nbDayType1 = this.nWorkingDay
    if (this.hasWorkingDay2) {
      nbDayType1 = this.nWorkingDay - this.nWorkingDay2
    }
    return nbDayType1
  }

  getRawDistance1 () {
    let total = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let index = 0
    for (let mode of TRAVELS_MODES) {
      if (this[mode] !== 0 && !isNaN(this[mode])) {
        total[index] += parseFloat(this[mode])
      }
      index += 1
    }
    return total
  }

  getRawDistance2 () {
    // let self = this
    // TRAVELS_MODES.map(key => self[key])
    let total = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let index = 0
    for (let mode of TRAVELS_MODES) {
      if (this.hasWorkingDay2) {
        if (this[mode + '2'] !== 0 && !isNaN(this[mode + '2'])) {
          total[index] += parseFloat(this[mode + '2'])
        }
      }
      index += 1
    }
    return total
  }

  getRawDistance (GHGIYear) {
    let total = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let index = 0
    let faAnnuel1 = this.nWorkingDay * this.getNumberOfWorkedWeek(GHGIYear)
    if (this.hasWorkingDay2) {
      faAnnuel1 = (this.nWorkingDay - this.nWorkingDay2) * this.getNumberOfWorkedWeek(GHGIYear)
    }
    for (let mode of TRAVELS_MODES) {
      total[index] += this[mode] * faAnnuel1
      if (this.hasWorkingDay2) {
        let faAnnuel2 = this.nWorkingDay2 * this.getNumberOfWorkedWeek(GHGIYear)
        total[index] += this[mode + '2'] * faAnnuel2
      }
      index += 1
    }
    return total
  }

  getRawWeekDistance1 () {
    let total = 0
    for (let mode of TRAVELS_MODES) {
      if (this[mode] !== 0 && !isNaN(this[mode])) {
        total += parseFloat(this[mode])
      }
    }
    if (this.hasWorkingDay2) {
      total = total * (this.nWorkingDay - this.nWorkingDay2)
    } else {
      total = total * this.nWorkingDay
    }
    return total
  }

  getRawWeekDistance2 () {
    let total = 0
    if (this.hasWorkingDay2) {
      for (let mode of TRAVELS_MODES) {
        if (this[mode + '2'] !== 0 && !isNaN(this[mode + '2'])) {
          total += parseFloat(this[mode + '2'])
        }
      }
    }
    return total * this.nWorkingDay2
  }

  getRawWeekDistance () {
    return this.getRawWeekDistance1() + this.getRawWeekDistance2()
  }

  getDistanceTotale (GHGIYear = null) {
    return this.getRawDistance(GHGIYear).reduce((a, b) => a + b)
  }

  getTotalIntensity (GHGIYear = null, citySize = 0) {
    let intensities = new CarbonIntensities()
    for (let intensity of this.getCarbonIntensity(citySize, GHGIYear)) {
      intensities.add(intensity)
    }
    return intensities.sum()
  }

  getEmissionFactor (mode, engine, citySize, GHGIYear) {
    let ef = null
    if (mode === 'walking') {
      let efbase = {
        'co2': 0.0000,
        'ch4': 0.0000,
        'n2o': 0.0000,
        'others': 0.0000,
        'unit': 'kgCO2e/passager.km',
        'total': 0.0000,
        'uncertainty': 0.0
      }
      ef = {
        'total': efbase,
        'combustion': efbase,
        'upstream': efbase,
        'manufacturing': efbase
      }
      ef.group = 'walking'
    } else if (mode === 'bike') {
      ef = filterEFByYear(VEHICLES_FACTORS['bike']['muscular'], GHGIYear)
    } else if (mode === 'ebike') {
      ef = filterEFByYear(VEHICLES_FACTORS['bike']['electric'], GHGIYear)
    } else if (mode === 'escooter') {
      ef = filterEFByYear(VEHICLES_FACTORS['scooter']['electric'], GHGIYear)
    } else if (mode === 'motorbike') {
      ef = filterEFByYear(VEHICLES_FACTORS['motorbike']['gasoline'], GHGIYear)
    } else if (mode === 'car') {
      ef = filterEFByYear(VEHICLES_FACTORS['car'][engine], GHGIYear)
    } else if (mode === 'bus') {
      if (citySize === 0) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['bus']['bus.smallcity'], GHGIYear)
      } else if (citySize === 1) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['bus']['bus.mediumcity'], GHGIYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['bus']['bus.bigcity'], GHGIYear)
      }
    } else if (mode === 'busintercity') {
      ef = filterEFByYear(TRANSPORTS_FACTORS['bus']['bus.intercity'], GHGIYear)
    } else if (mode === 'train') {
      // The survey asks for dailyDistance, to choose the emission
      // factor, we considere its go and back
      let commuteDistance = this[mode] / 2
      if (commuteDistance < 200) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.shortdistance'], GHGIYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.longdistance'], GHGIYear)
      }
    } else if (mode === 'tram') {
      if (citySize === 0) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['tram.mediumcity'], GHGIYear)
      } else if (citySize === 1) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['tram.mediumcity'], GHGIYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['tram.bigcity'], GHGIYear)
      }
    } else if (mode === 'expressrailway') {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['rer'], GHGIYear)
    } else if (mode === 'subway') {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['subway'], GHGIYear)
    }
    return ef
  }

  getCarbonIntensity (citySize, GHGIYear = null) {
    let intensities = new Array(Commute.travelsModes.length).fill(0)
    for (let i in intensities) {
      intensities[i] = new CarbonIntensities()
    }
    let index = 0
    if (!this.deleted) {
      for (let mode of TRAVELS_MODES) {
        if (this[mode] !== 0 && !isNaN(this[mode])) {
          let ef = this.getEmissionFactor(mode, this.engine, citySize, GHGIYear)
          let distance = this[mode]
          if (mode === 'car') {
            distance = distance / this.carpooling
          } else if (mode === 'motorbike') {
            distance = distance / this.motorbikepooling
          }
          let faAnnuel = this.nWorkingDay * this.getNumberOfWorkedWeek(GHGIYear)
          if (this.hasWorkingDay2) {
            faAnnuel = (this.nWorkingDay - this.nWorkingDay2) * this.getNumberOfWorkedWeek(GHGIYear)
          }
          intensities[index].add(new DetailedCarbonIntensity(
            0.0,
            0.0,
            0.0,
            0.0,
            distance * faAnnuel * ef.combustion.total,
            distance * faAnnuel * ef.combustion.total * ef.combustion.uncertainty,
            distance * faAnnuel * ef.upstream.total,
            distance * faAnnuel * ef.upstream.total * ef.upstream.uncertainty,
            distance * faAnnuel * ef.manufacturing.total,
            distance * faAnnuel * ef.manufacturing.total * ef.manufacturing.uncertainty,
            ef.group
          ))
        }
        if (this.hasWorkingDay2) {
          if (this[mode + '2'] !== 0 && !isNaN(this[mode + '2'])) {
            let ef2 = this.getEmissionFactor(mode, this.engine2, citySize, GHGIYear)
            let distance2 = this[mode + '2']
            if (mode === 'car') {
              distance2 = distance2 / this.carpooling2
            } else if (mode === 'motorbike') {
              distance2 = distance2 / this.motorbikepooling2
            }
            let faAnnuel2 = this.nWorkingDay2 * this.getNumberOfWorkedWeek(GHGIYear)
            intensities[index].add(new DetailedCarbonIntensity(
              0.0,
              0.0,
              0.0,
              0.0,
              distance2 * faAnnuel2 * ef2.combustion.total,
              distance2 * faAnnuel2 * ef2.combustion.total * ef2.combustion.uncertainty,
              distance2 * faAnnuel2 * ef2.upstream.total,
              distance2 * faAnnuel2 * ef2.upstream.total * ef2.upstream.uncertainty,
              distance2 * faAnnuel2 * ef2.manufacturing.total,
              distance2 * faAnnuel2 * ef2.manufacturing.total * ef2.manufacturing.uncertainty,
              ef2.group
            ))
          }
        }
        index += 1
      }
    }
    // to correctly compute the uncertainty propagation, returns
    // CarbonIntensities instead of CarbonIntensity object
    // return intensities.map(obj => obj.sum())
    return intensities
  }

  toDatabase () {
    let nWorkingDay2 = 0
    if (this.nWorkingDay2 !== null) {
      nWorkingDay2 = this.nWorkingDay2
    }
    return {
      'seqID': this.seqID,
      'position': this.position,
      'nWorkingDay': this.nWorkingDay,
      'message': this.message,
      'walking': this.walking,
      'bike': this.bike,
      'ebike': this.ebike,
      'escooter': this.escooter,
      'motorbike': this.motorbike,
      'car': this.car,
      'bus': this.bus,
      'busintercity': this.busintercity,
      'tram': this.tram,
      'train': this.train,
      'expressrailway': this.expressrailway,
      'subway': this.subway,
      'motorbikepooling': this.motorbikepooling,
      'carpooling': this.carpooling,
      'engine': this.engine,
      'hasWorkingDay2': this.hasWorkingDay2,
      'nWorkingDay2': nWorkingDay2,
      'walking2': this.walking2,
      'bike2': this.bike2,
      'ebike2': this.ebike2,
      'escooter2': this.escooter2,
      'motorbike2': this.motorbike2,
      'car2': this.car2,
      'bus2': this.bus2,
      'busintercity2': this.busintercity2,
      'tram2': this.tram2,
      'train2': this.train2,
      'expressrailway2': this.expressrailway2,
      'subway2': this.subway2,
      'motorbikepooling2': this.motorbikepooling2,
      'carpooling2': this.carpooling2,
      'engine2': this.engine2,
      'deleted': this.deleted
    }
  }

  static get travelsModes () {
    return TRAVELS_MODES
  }

  static getIcon (mode) {
    return ICONS[mode]
  }

  static getIconPack (mode) {
    return ICONS_PACK[mode]
  }

  static getMaxDistance (mode) {
    return MAX_DISTANCES[mode]
  }

  static createFromObj (commute) {
    return new Commute(
      commute.seqID,
      commute.position,
      commute.nWorkingDay,
      commute.message,
      commute.walking,
      commute.bike,
      commute.ebike,
      commute.escooter,
      commute.motorbike,
      commute.car,
      commute.bus,
      commute.busintercity,
      commute.tram,
      commute.train,
      commute.expressrailway,
      commute.subway,
      commute.motorbikepooling,
      commute.carpooling,
      commute.engine,
      commute.hasWorkingDay2,
      commute.nWorkingDay2,
      commute.walking2,
      commute.bike2,
      commute.ebike2,
      commute.escooter2,
      commute.motorbike2,
      commute.car2,
      commute.bus2,
      commute.busintercity2,
      commute.tram2,
      commute.train2,
      commute.expressrailway2,
      commute.subway2,
      commute.motorbikepooling2,
      commute.carpooling2,
      commute.engine2,
      commute.deleted
    )
  }

  static initiStatusTable (length, fillIntensities = false) {
    let table = {
      'researcher': new Array(length).fill(0),
      'engineer': new Array(length).fill(0),
      'student': new Array(length).fill(0)
    }
    if (fillIntensities) {
      for (let key of Object.keys(table)) {
        for (let i in table[key]) {
          table[key][i] = new CarbonIntensities()
        }
      }
      return table
    } else {
      return table
    }
  }

  static compute (
    commutes,
    citySize,
    nresearcher,
    nteacher,
    nengineer,
    nstudent,
    ghgiYear
  ) {
    let intensities = Commute.initiStatusTable(Commute.travelsModes.length, true)
    let distances = Commute.initiStatusTable(Commute.travelsModes.length)
    let cdays = {
      'researcher': 0,
      'engineer': 0,
      'student': 0
    }
    let nbAnswers = {
      'researcher': 0,
      'engineer': 0,
      'student': 0
    }
    let totalPerCommute = []
    let rawDistance1 = []
    let rawDistance2 = []
    let meta = []
    for (let commute of commutes) {
      if (!commute.deleted) {
        let intensity = commute.getCarbonIntensity(citySize, ghgiYear)
        let cDistances = commute.getRawDistance(ghgiYear)
        totalPerCommute.unshift([commute.seqID].concat(intensity))
        rawDistance1.unshift(commute.getRawDistance1())
        rawDistance2.unshift(commute.getRawDistance2())
        meta.unshift({
          'seqID': commute.seqID,
          'nWorkingDay': commute.nWorkingDay,
          'position': commute.position,
          'nWorkingDay2': commute.nWorkingDay2
        })
        nbAnswers[commute.position] += 1
        for (let index = 0; index < Commute.travelsModes.length; index++) {
          intensities[commute.position][index].add(intensity[index])
          distances[commute.position][index] += cDistances[index]
          cdays[commute.position] += commute.nWorkingDay * commute.getNumberOfWorkedWeek(ghgiYear)
        }
      }
    }

    // convert CarbonIntensities to CarbonIntensity for normalization
    Object.keys(intensities).map(function (key) {
      intensities[key] = intensities[key].map(obj => obj.sum())
    })
    // normalize intensity by the number of lab members
    for (let status of Object.keys(intensities)) {
      for (let index = 0; index < Commute.travelsModes.length; index++) {
        let normalizationFactor = 0
        if (nbAnswers[status] !== 0 && status === 'researcher') {
          normalizationFactor = (nresearcher + nteacher) / nbAnswers[status]
        } else if (nbAnswers[status] !== 0 && status === 'engineer') {
          normalizationFactor = nengineer / nbAnswers[status]
        } else if (nbAnswers[status] !== 0 && status === 'student') {
          normalizationFactor = nstudent / nbAnswers[status]
        }
        intensities[status][index].multiply(normalizationFactor)
        distances[status][index] = distances[status][index] * normalizationFactor
      }
    }
    let total = new CarbonIntensities()
    for (let status of Object.keys(intensities)) {
      for (let index = 0; index < Commute.travelsModes.length; index++) {
        total.add(intensities[status][index])
      }
    }
    return {
      'intensity': total.sum(),
      'totalPerCommute': totalPerCommute,
      'status': {
        'intensity': intensities,
        'distances': distances
      },
      'rawDistance1': rawDistance1,
      'rawDistance2': rawDistance2,
      'nbAnswers': nbAnswers,
      'cdays': cdays,
      'meta': meta
    }
  }
}
