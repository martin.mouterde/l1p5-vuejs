
/* eslint-disable camelcase */

import { device_utils } from '@/ext/ecodiag/device_utils.js'
import { CarbonIntensity, CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

export default class ComputerDevice {
  constructor (id, type, model, amount, acquisitionYear, ecodiagObject = undefined) {
    this.id = id
    this.type = type
    this.model = model
    this.amount = amount
    this.acquisitionYear = parseInt(acquisitionYear)
    this.ecodiagObject = ecodiagObject
  }

  toEcodiag () {
    if (!this.ecodiagObject) {
      this.ecodiagObject = device_utils.methods.create_device_item(this.type, {
        id: this.id,
        model: this.model,
        nb: this.amount,
        year: this.acquisitionYear,
        score: 2
      })
    }
    return this.ecodiagObject
  }

  toDatabase () {
    return {
      'type': this.type,
      'model': this.model,
      'amount': this.amount,
      'acquisitionYear': this.acquisitionYear
    }
  }

  static type2group = function (type) {
    try {
      return {
        'desktop': 'desktopgroup',
        'server': 'desktopgroup',
        'allinone': 'desktopgroup',
        'smartphone': 'smartphonegroup',
        'pad': 'smartphonegroup' }[type]
    } catch (e) {
      return type
    }
  }

  getCarbonIntensity () {
    let up = [90]
    let intensity = device_utils.methods.compute_device_co2e(this.toEcodiag(), 'flux', up)
    let uncertainty = (intensity.sups[0] - intensity.infs[0]) / 2.0
    return new CarbonIntensity(
      intensity.grey,
      uncertainty,
      null,
      null,
      ComputerDevice.type2group(this.type)
    )
  }

  static createFromObj (device) {
    return new ComputerDevice(
      device.id,
      device.type,
      device.model,
      device.amount,
      device.acquisitionYear,
      device.ecodiagObject
    )
  }

  static compute (devices) {
    let intensities = new CarbonIntensities()
    let meta = []
    for (let device of devices) {
      let intensity = device.getCarbonIntensity()
      intensities.add(intensity)
      meta.unshift({
        'type': device.type,
        'model': device.model,
        'amount': device.amount,
        'intensity': intensity
      })
    }
    return {
      'intensity': intensities.sum(),
      'meta': meta
    }
  }

  static fromEcodiag (ecodiagObject) {
    return new ComputerDevice(
      null,
      ecodiagObject.type,
      ecodiagObject.model,
      ecodiagObject.nb,
      ecodiagObject.year,
      ecodiagObject
    )
  }
}
