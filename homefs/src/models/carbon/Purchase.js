/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { filterEFByYear } from '@/models/utils'
import { CarbonIntensity, CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'
import PURCHASES_FACTORS from '@/../data/purchasesFactors.json'

const MODULE_NAME = 'purchases'
const TRAVELLING_EXPENSES_CODES = ['xa01', 'xa02', 'xa11', 'xa12']
const WARNING_RATE = 20

export default class Purchase {
  constructor (id, code, amount, source = null) {
    this.score = 1
    this.id = id
    this.setCode(code)
    this.amount = this.setAmount(amount)
    this.source = source
    this.module = this.setModule()
  }

  setCode (value) {
    let lvalue = value.toLowerCase().replace('.', '').trim()
    if (!Object.keys(PURCHASES_FACTORS).includes(lvalue)) {
      this.score = 2
    }
    this.code = lvalue
  }

  setAmount (value) {
    let amount = value
    if (typeof amount === 'string') {
      amount = amount.replace(',', '.').trim().replace(' ', '')
    }
    if (!isNaN(amount)) {
      amount = parseFloat(amount)
    } else {
      this.score = 2
    }
    return amount
  }

  setModule () {
    let module = null
    try {
      module = this.getFactorModule()
      if (module !== Purchase.MODULE_NAME && module !== null) {
        this.score = 0
      }
    } catch {
      this.score = 2
    }
    return module
  }

  getCarbonIntensity (GHGIYear) {
    let ef = this.getEmissionFactor(this.code, GHGIYear)
    return new CarbonIntensity(
      this.amount * ef.total.total,
      this.amount * ef.total.total * ef.total.uncertainty,
      null,
      null,
      ef.group
    )
  }

  getEmissionFactor (code, year) {
    let ef = filterEFByYear(PURCHASES_FACTORS[code], year)
    return ef
  }

  getFactorDescription () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].description_FR
    } else {
      return '-'
    }
  }

  getFactorModule () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].module
    } else {
      return null
    }
  }

  getScope3 () {
    let scope3 = '9'
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      if (PURCHASES_FACTORS[this.code].scope3 !== null) {
        scope3 = PURCHASES_FACTORS[this.code].scope3
      }
    }
    return scope3
  }

  getCategory () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].category
    } else {
      return null
    }
  }

  getDescription () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].description_FR
    } else {
      return null
    }
  }

  getCodeFromDescription (value) {
    let lvalue = value.toLowerCase()
    let fcode = null
    for (let code of Object.keys(PURCHASES_FACTORS)) {
      if (PURCHASES_FACTORS[code].description_FR.toLowerCase() === lvalue) {
        fcode = code
      }
    }
    return fcode
  }

  isValid () {
    return Object.keys(PURCHASES_FACTORS).includes(this.code) && !isNaN(this.amount)
  }

  toDatabase () {
    return {
      'code': this.code,
      'amount': this.amount
    }
  }

  static reduce (purchases) {
    let reducedPurchases = []
    for (let purchase of purchases) {
      let index = reducedPurchases.findIndex(obj => obj.code === purchase.code)
      if (index < 0) {
        reducedPurchases.push(purchase)
      } else {
        reducedPurchases[index].amount += purchase.amount
      }
    }
    return reducedPurchases
  }

  static getTravellingExpensesCodes () {
    return TRAVELLING_EXPENSES_CODES
  }

  static getWarningRate () {
    return WARNING_RATE
  }

  static getScope3 () {
    let scope3 = Object.keys(PURCHASES_FACTORS).map(function (code) {
      return PURCHASES_FACTORS[code].scope3
    })
    return Array.from(new Set(scope3)).filter(val => val !== null)
  }

  static getCodes () {
    let codes = Object.keys(PURCHASES_FACTORS)
    return Array.from(new Set(codes))
  }

  static getDescriptions () {
    let descriptions = Object.keys(PURCHASES_FACTORS).map(function (code) {
      return PURCHASES_FACTORS[code].description_FR
    })
    return Array.from(new Set(descriptions))
  }

  static get MODULE_NAME () {
    return MODULE_NAME
  }

  static createFromObj (item) {
    return new Purchase(
      item.id,
      item.code,
      item.amount,
      item.source
    )
  }

  static compute (purchases, GHGIYear) {
    let intensities = new CarbonIntensities()
    let meta = []
    let scope3 = {}
    let categories = {}
    let totalXA = 0
    for (let purchase of purchases) {
      let intensity = purchase.getCarbonIntensity(GHGIYear)
      let cscope3 = purchase.getScope3()
      let category = purchase.getCategory()
      meta.unshift({
        'code': purchase.code,
        'scope3': cscope3,
        'category': category,
        'module': purchase.getFactorModule(),
        'description': purchase.getDescription(),
        'amount': purchase.amount,
        'intensity': intensity
      })
      if (purchase.getFactorModule() === Purchase.MODULE_NAME) {
        intensities.add(intensity)
        if (Object.keys(scope3).includes(cscope3.toString())) {
          scope3[cscope3.toString()].add(intensity)
        } else {
          scope3[cscope3.toString()] = new CarbonIntensities()
          scope3[cscope3.toString()].add(intensity)
        }
        if (Object.keys(categories).includes(category)) {
          categories[category].add(intensity)
        } else {
          categories[category] = new CarbonIntensities()
          categories[category].add(intensity)
        }
        if (TRAVELLING_EXPENSES_CODES.includes(purchase.code)) {
          totalXA += intensity.intensity
        }
      }
    }
    Object.keys(categories).map(function (key) {
      categories[key] = categories[key].sum()
    })
    Object.keys(scope3).map(function (key) {
      scope3[key] = scope3[key].sum()
    })
    return {
      'intensity': intensities.sum(),
      'meta': meta,
      'scope3': scope3,
      'categories': categories,
      'totalXA': totalXA
    }
  }
}
