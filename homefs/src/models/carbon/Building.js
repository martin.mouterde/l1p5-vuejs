/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { filterEFByYear } from '@/models/utils'
import HEATING_FACTORS from '@/../data/heatingFactors.json'
import ELECTRICITY_FACTORS from '@/../data/electricityFactors.json'
import REFRIGERANTS_FACTORS from '@/../data/refrigerantsFactors.json'
import {
  DetailedCarbonIntensity,
  DetailedCarbonIntensities,
  CarbonIntensity,
  CarbonIntensities
} from '@/models/carbon/CarbonIntensity.js'

const URBAN_NETWORK = 'urban.network'

export default class Building {
  constructor (id, name, area, share, heatings,
    refrigerants, electricity, selfProduction, selfConsumption, site) {
    this.id = id
    this.name = name
    this.area = area
    this.share = share
    this.heatings = heatings
    this.refrigerants = refrigerants
    this.electricity = electricity
    this.selfProduction = selfProduction
    this.selfConsumption = selfConsumption
    this.site = site
  }

  getTotalHeatingConsumption () {
    var consumption = 0
    for (let heating of this.heatings) {
      consumption += Math.round(this.getHeatingConsumption(heating))
    }
    return consumption
  }

  getHeatingConsumption (heating) {
    let consumption = 0
    if (heating.isMonthly) {
      consumption += parseInt(heating.january) + parseInt(heating.february)
      consumption += parseInt(heating.march) + parseInt(heating.april)
      consumption += parseInt(heating.may) + parseInt(heating.june)
      consumption += parseInt(heating.july) + parseInt(heating.august)
      consumption += parseInt(heating.septembre) + parseInt(heating.octobre)
      consumption += parseInt(heating.novembre) + parseInt(heating.decembre)
    } else {
      consumption = heating.total
    }
    return consumption * this.share / 100
  }

  getElectricityConsumption () {
    let consumption = 0
    if (this.electricity.isMonthly) {
      consumption += parseInt(this.electricity.january) + parseInt(this.electricity.february)
      consumption += parseInt(this.electricity.march) + parseInt(this.electricity.april)
      consumption += parseInt(this.electricity.may) + parseInt(this.electricity.june)
      consumption += parseInt(this.electricity.july) + parseInt(this.electricity.august)
      consumption += parseInt(this.electricity.septembre) + parseInt(this.electricity.octobre)
      consumption += parseInt(this.electricity.novembre) + parseInt(this.electricity.decembre)
    } else {
      consumption = this.electricity.total
    }
    if (this.selfProduction) {
      consumption = (consumption - this.selfConsumption) * this.share / 100
    } else {
      consumption = consumption * this.share / 100
    }
    return consumption
  }

  getEmissionFactor (type, subtype, year) {
    let ef = null
    if (type === 'heating') {
      if (subtype === 'naturalgas' || subtype === 'propane') {
        ef = filterEFByYear(HEATING_FACTORS['gas'][subtype], year)
      } else if (subtype === 'heating.oil') {
        ef = filterEFByYear(HEATING_FACTORS['heating.oil'][subtype], year)
      } else if (subtype === 'biomethane') {
        ef = filterEFByYear(HEATING_FACTORS['biomethane'][subtype], year)
      } else if (subtype.startsWith('wood')) {
        ef = filterEFByYear(HEATING_FACTORS['wood.heating'][subtype], year)
      } else if (subtype.startsWith('network')) {
        ef = filterEFByYear(HEATING_FACTORS['urban.network'][subtype], year)
      }
    } else if (type === 'electricity') {
      if (subtype in ELECTRICITY_FACTORS) {
        ef = filterEFByYear(ELECTRICITY_FACTORS[subtype], year)
      }
    } else if (type === 'refrigerants') {
      ef = filterEFByYear(REFRIGERANTS_FACTORS[subtype], year)
    }
    return ef
  }

  getHeatingCarbonIntensity (GHGIYear, heating) {
    let consumption = this.getHeatingConsumption(heating)
    let intensity = new DetailedCarbonIntensity()
    if (heating.type === Building.urbanNetwork) {
      let ef = this.getEmissionFactor('heating', heating.urbanNetwork, GHGIYear)
      intensity = new DetailedCarbonIntensity(
        0.0,
        0.0,
        0.0,
        0.0,
        consumption * ef.total.total,
        consumption * ef.total.total * ef.total.uncertainty,
        0.0,
        0.0,
        0.0,
        0.0,
        ef.group
      )
    } else if (heating.type !== null && heating.type !== 'electric') {
      let ef = this.getEmissionFactor('heating', heating.type, GHGIYear)
      intensity = new DetailedCarbonIntensity(
        consumption * ef.combustion.co2,
        consumption * ef.combustion.ch4,
        consumption * ef.combustion.n2o,
        consumption * ef.combustion.others,
        consumption * ef.combustion.total,
        consumption * ef.combustion.total * ef.combustion.uncertainty,
        consumption * ef.upstream.total,
        consumption * ef.upstream.total * ef.upstream.uncertainty,
        0.0,
        0.0,
        ef.group
      )
    }
    return intensity
  }

  getElectricityCarbonIntensity (area, country, GHGIYear) {
    let ef = this.getEmissionFactor('electricity', country + '.' + area, GHGIYear)
    if (ef === null) {
      ef = this.getEmissionFactor('electricity', country, GHGIYear)
    }
    let consumption = this.getElectricityConsumption()
    return new DetailedCarbonIntensity(
      0.0,
      0.0,
      0.0,
      0.0,
      consumption * ef.combustion.total,
      consumption * ef.combustion.total * ef.combustion.uncertainty,
      consumption * ef.upstream.total,
      consumption * ef.upstream.total * ef.upstream.uncertainty,
      0.0,
      0.0,
      ef.group
    )
  }

  getRefrigerantsCarbonIntensity (GHGIYear) {
    let intensities = new CarbonIntensities()
    for (let gas of this.refrigerants) {
      let ef = this.getEmissionFactor('refrigerants', gas.name, GHGIYear)
      intensities.add(new CarbonIntensity(
        gas.total * ef.total.total,
        gas.total * ef.total.total * ef.total.uncertainty,
        null,
        null,
        ef.group
      ))
    }
    return intensities
  }

  static get urbanNetwork () {
    return URBAN_NETWORK
  }

  static getRefrigerants () {
    let refrigerants = Object.keys(REFRIGERANTS_FACTORS)
    return Array.from(new Set(refrigerants))
  }

  static getUrbanNetworks () {
    let urbanNetworks = []
    for (let network of Object.keys(HEATING_FACTORS['urban.network'])) {
      urbanNetworks.push({
        'network': network,
        'description_FR': HEATING_FACTORS['urban.network'][network]['description_FR'],
        'description_EN': HEATING_FACTORS['urban.network'][network]['description_EN']
      })
    }
    return urbanNetworks
  }

  static getHeatings () {
    let heatings = []
    for (let subtype of Object.keys(HEATING_FACTORS)) {
      if (subtype !== 'urban.network') {
        for (let subsubtype of Object.keys(HEATING_FACTORS[subtype])) {
          heatings.push(subsubtype)
        }
      }
    }
    heatings.unshift(URBAN_NETWORK)
    heatings.unshift('electric')
    return Array.from(new Set(heatings))
  }

  static getHeatingUnit (type) {
    let unit = null
    if (type === 'naturalgas' || type === 'propane') {
      unit = HEATING_FACTORS['gas'][type].unit
    } else if (type === 'heating.oil') {
      unit = HEATING_FACTORS['heating.oil'][type].unit
    } else if (type === 'biomethane') {
      unit = HEATING_FACTORS['biomethane'][type].unit
    } else if (type.startsWith('wood')) {
      unit = HEATING_FACTORS['wood.heating'][type].unit
    } else if (type.startsWith('network')) {
      unit = HEATING_FACTORS['urban.network'][type].unit
    }
    return unit
  }

  static getDescription (type, subtype, lang = 'FR') {
    let desc = ''
    if (type === 'heating') {
      if (subtype === 'naturalgas' || subtype === 'propane') {
        desc = HEATING_FACTORS['gas'][subtype]['description_' + lang]
      } else if (subtype === 'heating.oil') {
        desc = HEATING_FACTORS['heating.oil'][subtype]['description_' + lang]
      } else if (subtype === 'biomethane') {
        desc = HEATING_FACTORS['biomethane'][subtype]['description_' + lang]
      } else if (subtype.startsWith('wood')) {
        desc = HEATING_FACTORS['wood.heating'][subtype]['description_' + lang]
      } else if (subtype.startsWith('network')) {
        desc = HEATING_FACTORS['urban.network'][subtype]['description_' + lang]
      }
    } else if (type === 'electricity') {
      desc = ELECTRICITY_FACTORS[subtype]['description_' + lang]
    } else if (type === 'refrigerants') {
      desc = REFRIGERANTS_FACTORS[subtype]['description_' + lang]
    }
    return desc
  }

  static createHeating () {
    return {
      id: null,
      type: null,
      urbanNetwork: null,
      january: 0,
      february: 0,
      march: 0,
      april: 0,
      may: 0,
      june: 0,
      july: 0,
      august: 0,
      septembre: 0,
      octobre: 0,
      novembre: 0,
      decembre: 0,
      total: 0,
      isMonthly: true
    }
  }

  static createFromObj (building) {
    let heatings = []
    if ('heatings' in building) {
      for (let heating of building.heatings) {
        let heatingNew = Building.createHeating()
        heatingNew = heating
        heatings.push(heatingNew)
      }
    }
    return new Building(
      building.id,
      building.name,
      building.area,
      building.share,
      heatings,
      building.refrigerants,
      building.electricity,
      building.selfProduction,
      building.selfConsumption,
      building.site
    )
  }

  static compute (buildings, area, country, GHGIYear) {
    let heatingc = new DetailedCarbonIntensities()
    let heatingu = new DetailedCarbonIntensities()
    let heatingi = new CarbonIntensities()
    let electricityi = new DetailedCarbonIntensities()
    let refrigerantsi = new CarbonIntensities()
    let intensities = new CarbonIntensities()
    let meta = []
    for (let building of buildings) {
      // heating carbon intensity
      let heatingIntensity = new DetailedCarbonIntensities()
      for (let heating of building.heatings) {
        let cheatingIntensity = building.getHeatingCarbonIntensity(GHGIYear, heating)
        heatingIntensity.add(cheatingIntensity)
        if (heating.isOwnedByLab) {
          heatingc.add(cheatingIntensity)
        } else {
          heatingu.add(cheatingIntensity)
        }
        heatingi.add(cheatingIntensity)
      }
      // electricity carbon intensity
      let electricityIntensity = building.getElectricityCarbonIntensity(area, country, GHGIYear)
      electricityi.add(electricityIntensity)

      // refrigerants carbon intensity
      let refrigerantsIntensity = building.getRefrigerantsCarbonIntensity(GHGIYear)
      refrigerantsi.add(refrigerantsIntensity)

      // total carbon intensity
      intensities.add(heatingIntensity)
      intensities.add(electricityIntensity)
      intensities.add(refrigerantsIntensity)

      // all data for export
      meta.unshift({
        'name': building.name,
        'site': building.site,
        'area': building.area,
        'share': building.share,
        'selfProduction': building.selfProduction,
        'selfConsumption': building.selfConsumption,
        'heatingIntensity': heatingIntensity,
        'electricityIntensity': electricityIntensity,
        'refrigerantsIntensity': refrigerantsIntensity
      })
    }
    return {
      'intensity': intensities.sum(),
      'heatings': {
        'controled': heatingc.sum(),
        'uncontroled': heatingu.sum(),
        'intensity': heatingi.sum()
      },
      'electricity': electricityi.sum(),
      'refrigerants': refrigerantsi.sum(),
      'meta': meta
    }
  }
}
