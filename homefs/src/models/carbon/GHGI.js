/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Building from '@/models/carbon/Building'
import Commute from '@/models/carbon/Commute'
import ComputerDevice from '@/models/carbon/ComputerDevice'
import Purchase from '@/models/carbon/Purchase'
import Travel from '@/models/carbon/Travel'
import Vehicle from '@/models/carbon/Vehicle'

export default class GHGI {
  constructor (id, uuid, year, created, nResearcher, nProfessor,
    nEngineer, nStudent, budget, surveyMessage, surveyActive,
    laboratory, buildingsSubmitted, commutesSubmitted,
    travelsSubmitted, vehiclesSubmitted, devicesSubmitted,
    purchasesSubmitted, surveyCloneYear, buildings = [],
    purchases = [], devices = [], vehicles = [], travels = [],
    commutes = [], emissions = {}) {
    this.id = id
    this.uuid = uuid
    this.year = year
    this.created = created
    this.nResearcher = nResearcher
    this.nProfessor = nProfessor
    this.nEngineer = nEngineer
    this.nStudent = nStudent
    this.budget = budget
    this.surveyMessage = surveyMessage
    this.surveyActive = surveyActive
    this.laboratory = laboratory
    this.buildingsSubmitted = buildingsSubmitted
    this.commutesSubmitted = commutesSubmitted
    this.travelsSubmitted = travelsSubmitted
    this.vehiclesSubmitted = vehiclesSubmitted
    this.devicesSubmitted = devicesSubmitted
    this.purchasesSubmitted = purchasesSubmitted
    this.surveyCloneYear = surveyCloneYear
    this.buildings = buildings.map((obj) => Building.createFromObj(obj))
    this.purchases = purchases.map((obj) => Purchase.createFromObj(obj))
    this.devices = devices.map((obj) => ComputerDevice.createFromObj(obj))
    this.vehicles = vehicles.map((obj) => Vehicle.createFromObj(obj))
    this.travels = travels.map((obj) => Travel.createFromObj(obj))
    this.commutes = commutes.map((obj) => Commute.createFromObj(obj))
    this.emissions = emissions
  }

  compute (year = null) {
    let cyear = this.year
    if (year !== null) {
      cyear = year
    }
    this.emissions.buildings = Building.compute(
      this.buildings,
      this.laboratory.area,
      this.laboratory.country,
      cyear
    )
    this.emissions.purchases = Purchase.compute(
      this.purchases
    )
    this.emissions.devices = ComputerDevice.compute(
      this.devices
    )
    this.emissions.vehicles = Vehicle.compute(
      this.vehicles,
      cyear
    )
    this.emissions.travels = Travel.compute(
      this.travels,
      cyear
    )
    this.emissions.commutes = Commute.compute(
      this.commutes,
      this.laboratory.citySize,
      this.nResearcher,
      this.nProfessor,
      this.nEngineer,
      this.nStudent,
      cyear
    )
  }

  static createFromObj (item) {
    return new GHGI(
      item.id,
      item.uuid,
      item.year,
      item.created,
      item.nResearcher,
      item.nProfessor,
      item.nEngineer,
      item.nStudent,
      item.budget,
      item.surveyMessage,
      item.surveyActive,
      item.laboratory,
      item.buildingsSubmitted,
      item.commutesSubmitted,
      item.travelsSubmitted,
      item.vehiclesSubmitted,
      item.devicesSubmitted,
      item.purchasesSubmitted,
      item.surveyCloneYear,
      item.buildings,
      item.purchases,
      item.devices,
      item.vehicles,
      item.travels,
      item.commutes,
      item.emissions
    )
  }
}
