/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

export class CarbonIntensity {
  constructor (
    intensity = 0.0,
    uncertainty = 0.0,
    intensitywc = null,
    uncertaintywc = null,
    group = null
  ) {
    this.intensity = intensity
    this.uncertainty = uncertainty
    if (intensitywc !== null) {
      this.intensitywc = intensitywc
      this.uncertaintywc = uncertaintywc
    } else {
      this.intensitywc = intensity
      this.uncertaintywc = uncertainty
    }
    this.group = group
  }

  multiply (factor) {
    this.intensity = this.intensity * factor
    this.uncertainty = this.uncertainty * factor
    this.intensitywc = this.intensitywc * factor
    this.uncertaintywc = this.uncertaintywc * factor
  }
}

export class DetailedCarbonIntensity {
  constructor (
    co2 = 0.0,
    ch4 = 0.0,
    n2o = 0.0,
    other = 0.0,
    combustioni = 0.0,
    combustionu = 0.0,
    upstreami = 0.0,
    upstreamu = 0.0,
    manufacturingi = 0.0,
    manufacturingu = 0.0,
    group = null
  ) {
    this.co2 = co2
    this.ch4 = ch4
    this.n2o = n2o
    this.other = other
    this.combustion = new CarbonIntensity(combustioni, combustionu)
    this.upstream = new CarbonIntensity(upstreami, upstreamu)
    this.manufacturing = new CarbonIntensity(manufacturingi, manufacturingu)
    this.group = group
  }

  get intensity () {
    return this.combustion.intensity + this.upstream.intensity + this.manufacturing.intensity
  }

  get intensitywc () {
    return this.intensity
  }

  get uncertainty () {
    return this.combustion.uncertainty + this.upstream.uncertainty + this.manufacturing.uncertainty
  }

  get uncertaintywc () {
    return this.uncertainty
  }

  toCarbonIntensity () {
    return new CarbonIntensity(
      this.combustion.intensity + this.upstream.intensity + this.manufacturing.intensity,
      this.combustion.uncertainty + this.upstream.uncertainty + this.manufacturing.uncertainty,
      null,
      null,
      this.group
    )
  }
}

export class CarbonIntensities {
  constructor () {
    this._intensities = new CarbonIntensity()
    this._intensitiesPerMeta = {}
  }

  _addIntensity (intensity, squared = false) {
    if (intensity.group === null) {
      this._intensities.intensity += intensity.intensity
      this._intensities.intensitywc += intensity.intensitywc
      if (squared) {
        this._intensities.uncertainty += intensity.uncertainty
        this._intensities.uncertaintywc += intensity.uncertaintywc
      } else {
        this._intensities.uncertainty += Math.pow(intensity.uncertainty, 2)
        this._intensities.uncertaintywc += Math.pow(intensity.uncertaintywc, 2)
      }
    } else if (Object.keys(this._intensitiesPerMeta).includes(intensity.group)) {
      this._intensitiesPerMeta[intensity.group].intensity += intensity.intensity
      this._intensitiesPerMeta[intensity.group].uncertainty += intensity.uncertainty
      this._intensitiesPerMeta[intensity.group].intensitywc += intensity.intensitywc
      this._intensitiesPerMeta[intensity.group].uncertaintywc += intensity.uncertaintywc
    } else {
      this._intensitiesPerMeta[intensity.group] = new CarbonIntensity(
        intensity.intensity,
        intensity.uncertainty,
        intensity.intensitywc,
        intensity.uncertaintywc,
        intensity.group
      )
    }
  }

  add (intensity) {
    if (intensity instanceof CarbonIntensity) {
      this._addIntensity(intensity)
    } else if (intensity instanceof DetailedCarbonIntensity) {
      this._addIntensity(intensity.toCarbonIntensity())
    } else if (intensity instanceof CarbonIntensities) {
      this._addIntensity(intensity._intensities, true)
      for (let key of Object.keys(intensity._intensitiesPerMeta)) {
        this._addIntensity(intensity._intensitiesPerMeta[key])
      }
    } else if (intensity instanceof DetailedCarbonIntensities) {
      this._addIntensity(intensity._intensities.toCarbonIntensity(), true)
      for (let key of Object.keys(intensity._intensitiesPerMeta)) {
        this._addIntensity(intensity._intensitiesPerMeta[key].toCarbonIntensity())
      }
    }
  }

  get intensity () {
    let total = this._intensities.intensity
    for (let key of Object.keys(this._intensitiesPerMeta)) {
      total += this._intensitiesPerMeta[key].intensity
    }
    return total
  }

  get intensitywc () {
    let total = this._intensities.intensitywc
    for (let key of Object.keys(this._intensitiesPerMeta)) {
      total += this._intensitiesPerMeta[key].intensitywc
    }
    return total
  }

  get uncertainty () {
    let total = this._intensities.uncertainty
    for (let key of Object.keys(this._intensitiesPerMeta)) {
      total += Math.pow(this._intensitiesPerMeta[key].uncertainty, 2)
    }
    return Math.sqrt(total)
  }

  get uncertaintywc () {
    let total = this._intensities.uncertaintywc
    for (let key of Object.keys(this._intensitiesPerMeta)) {
      total += Math.pow(this._intensitiesPerMeta[key].uncertaintywc, 2)
    }
    return Math.sqrt(total)
  }

  sum () {
    let intensity = this._intensities.intensity
    let uncertainty = this._intensities.uncertainty
    let intensitywc = this._intensities.intensitywc
    let uncertaintywc = this._intensities.uncertaintywc
    for (let key of Object.keys(this._intensitiesPerMeta)) {
      intensity += this._intensitiesPerMeta[key].intensity
      uncertainty += Math.pow(this._intensitiesPerMeta[key].uncertainty, 2)
      intensitywc += this._intensitiesPerMeta[key].intensitywc
      uncertaintywc += Math.pow(this._intensitiesPerMeta[key].uncertaintywc, 2)
    }
    if (intensitywc === 0.0) {
      intensitywc = intensity
      uncertaintywc = uncertainty
    }
    return new CarbonIntensity(
      intensity,
      Math.sqrt(uncertainty),
      intensitywc,
      Math.sqrt(uncertaintywc)
    )
  }
}

export class DetailedCarbonIntensities {
  constructor () {
    this._intensities = new DetailedCarbonIntensity()
    this._intensitiesPerMeta = {}
  }

  _addIntensity (intensity, squared = false) {
    if (intensity.group === null) {
      this._intensities.co2 += intensity.co2
      this._intensities.ch4 += intensity.ch4
      this._intensities.n2o += intensity.n2o
      this._intensities.other += intensity.other
      this._intensities.combustion.intensity += intensity.combustion.intensity
      this._intensities.upstream.intensity += intensity.upstream.intensity
      this._intensities.manufacturing.intensity += intensity.manufacturing.intensity
      if (squared) {
        this._intensities.combustion.uncertainty += intensity.combustion.uncertainty
        this._intensities.upstream.uncertainty += intensity.upstream.uncertainty
        this._intensities.manufacturing.uncertainty += intensity.manufacturing.uncertainty
      } else {
        this._intensities.combustion.uncertainty += Math.pow(intensity.combustion.uncertainty, 2)
        this._intensities.upstream.uncertainty += Math.pow(intensity.upstream.uncertainty, 2)
        this._intensities.manufacturing.uncertainty += Math.pow(intensity.manufacturing.uncertainty, 2)
      }
    } else if (Object.keys(this._intensitiesPerMeta).includes(intensity.group)) {
      this._intensitiesPerMeta[intensity.group].co2 += intensity.co2
      this._intensitiesPerMeta[intensity.group].ch4 += intensity.ch4
      this._intensitiesPerMeta[intensity.group].n2o += intensity.n2o
      this._intensitiesPerMeta[intensity.group].other += intensity.other
      this._intensitiesPerMeta[intensity.group].combustion.intensity += intensity.combustion.intensity
      this._intensitiesPerMeta[intensity.group].combustion.uncertainty += intensity.combustion.uncertainty
      this._intensitiesPerMeta[intensity.group].upstream.intensity += intensity.upstream.intensity
      this._intensitiesPerMeta[intensity.group].upstream.uncertainty += intensity.upstream.uncertainty
      this._intensitiesPerMeta[intensity.group].manufacturing.intensity += intensity.manufacturing.intensity
      this._intensitiesPerMeta[intensity.group].manufacturing.uncertainty += intensity.manufacturing.uncertainty
    } else {
      this._intensitiesPerMeta[intensity.group] = new DetailedCarbonIntensity(
        intensity.co2,
        intensity.ch4,
        intensity.n2o,
        intensity.other,
        intensity.combustion.intensity,
        intensity.combustion.uncertainty,
        intensity.upstream.intensity,
        intensity.upstream.uncertainty,
        intensity.manufacturing.intensity,
        intensity.manufacturing.uncertainty
      )
    }
  }

  add (intensity) {
    if (intensity instanceof DetailedCarbonIntensity) {
      this._addIntensity(intensity)
    } else if (intensity instanceof DetailedCarbonIntensities) {
      this._addIntensity(intensity._intensities, true)
      for (let key of Object.keys(intensity._intensitiesPerMeta)) {
        this._addIntensity(intensity._intensitiesPerMeta[key])
      }
    }
  }

  sum () {
    let co2 = this._intensities.co2
    let ch4 = this._intensities.ch4
    let n2o = this._intensities.n2o
    let other = this._intensities.other
    let combustioni = this._intensities.combustion.intensity
    let combustionu = this._intensities.combustion.uncertainty
    let upstreami = this._intensities.upstream.intensity
    let upstreamu = this._intensities.upstream.uncertainty
    let manufacturingi = this._intensities.manufacturing.intensity
    let manufacturingu = this._intensities.manufacturing.uncertainty
    for (let key of Object.keys(this._intensitiesPerMeta)) {
      co2 += this._intensitiesPerMeta[key].co2
      ch4 += this._intensitiesPerMeta[key].ch4
      n2o += this._intensitiesPerMeta[key].n2o
      other += this._intensitiesPerMeta[key].other
      combustioni += this._intensitiesPerMeta[key].combustion.intensity
      combustionu += Math.pow(this._intensitiesPerMeta[key].combustion.uncertainty, 2)
      upstreami += this._intensitiesPerMeta[key].upstream.intensity
      upstreamu += Math.pow(this._intensitiesPerMeta[key].upstream.uncertainty, 2)
      manufacturingi += this._intensitiesPerMeta[key].manufacturing.intensity
      manufacturingu += Math.pow(this._intensitiesPerMeta[key].manufacturing.uncertainty, 2)
    }
    return new DetailedCarbonIntensity(
      co2,
      ch4,
      n2o,
      other,
      combustioni,
      Math.sqrt(combustionu),
      upstreami,
      Math.sqrt(upstreamu),
      manufacturingi,
      Math.sqrt(manufacturingu)
    )
  }
}
