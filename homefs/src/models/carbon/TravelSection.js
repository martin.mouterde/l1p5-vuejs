/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Travel from '@/models/carbon/Travel.js'
import { filterEFByYear } from '@/models/utils'
import VEHICLES_FACTORS from '@/../data/vehiclesFactors.json'
import TRANSPORTS_FACTORS from '@/../data/transportsFactors.json'
import { CarbonIntensity } from '@/models/carbon/CarbonIntensity.js'

const ROUND_TRIP_DICTIONARY = {
  'oui': true,
  'o': true,
  'yes': true,
  'y': true,
  'non': false,
  'no': false,
  'n': false
}

export default class TravelSection {
  constructor (
    transportation = null, departureCity = null,
    departureCountry = null, destinationCity = null, destinationCountry = null,
    carpooling = 1, distance = 0, departureCityLat = 0.0,
    departureCityLng = 0.0, destinationCityLat = 0.0,
    destinationCityLng = 0.0, type = null, isRoundTrip = true
  ) {
    this.setTransportation(transportation)
    this.departureCity = departureCity
    this.setDepartureCountry(departureCountry)
    this.destinationCity = destinationCity
    this.setDestinationCountry(destinationCountry)
    this.setCarpooling(carpooling)
    this.setDepartureCityLat(departureCityLat)
    this.setDepartureCityLng(departureCityLng)
    this.setDestinationCityLat(destinationCityLat)
    this.setDestinationCityLng(destinationCityLng)
    this.distance = distance
    if (type !== null) {
      this.type = type
    } else {
      this.computeType()
    }
    this.setIsRoundTrip(isRoundTrip)
  }

  setDepartureCountry (value) {
    this.departureCountry = Travel.iso3166Country(value)
  }

  setDestinationCountry (value) {
    this.destinationCountry = Travel.iso3166Country(value)
  }

  setTransportation (value) {
    this.transportation = Travel.getTransportation(value)
  }

  setCarpooling (value) {
    if (isNaN(value) || value === null || value === 'null' || value === '') {
      this.carpooling = 1
    } else if (value === '0' || value === 0) {
      this.carpooling = parseInt(value)
    } else {
      this.carpooling = parseInt(value)
    }
  }

  setIsRoundTrip (value) {
    if (typeof value === 'boolean') {
      this.isRoundTrip = value
    } else {
      this.isRoundTrip = TravelSection.getIsRoundTrip(value)
    }
  }

  setDepartureCityLat (value) {
    if (value === null) {
      this.setDepartureCountry(null)
    }
    this.departureCityLat = value
  }

  setDepartureCityLng (value) {
    if (value === null) {
      this.setDepartureCountry(null)
    }
    this.departureCityLng = value
  }

  setDestinationCityLat (value) {
    if (value === null) {
      this.setDestinationCountry(null)
    }
    this.destinationCityLat = value
  }

  setDestinationCityLng (value) {
    if (value === null) {
      this.setDestinationCountry(null)
    }
    this.destinationCityLng = value
  }

  isValid () {
    let isValid = true
    if (this.departureCountry === null) {
      isValid = false
    } else if (this.destinationCountry === null) {
      isValid = false
    } else if (this.transportation === null) {
      isValid = false
    } else if (this.carpooling === 0) {
      isValid = false
    } else if (typeof this.isRoundTrip !== 'boolean') {
      isValid = false
    } else if (this.departureCityLat === null) {
      isValid = false
    } else if (this.departureCityLng === null) {
      isValid = false
    } else if (this.destinationCityLat === null) {
      isValid = false
    } else if (this.destinationCityLng === null) {
      isValid = false
    }
    return isValid
  }

  latLngIsValid () {
    return this.departureCityLat !== null && this.departureCityLng !== null &&
      this.destinationCityLat !== null && this.destinationCityLng !== null
  }

  updateDistance () {
    if (this.departureCityLat !== 0 && this.departureCityLng !== 0 && this.destinationCityLat !== 0 && this.destinationCityLng !== 0) {
      this.distance = TravelSection.geodesicDist(
        this.departureCityLat,
        this.departureCityLng,
        this.destinationCityLat,
        this.destinationCityLng
      )
    }
  }

  getCorrectedDistance () {
    this.updateDistance()
    let cdistance = this.distance
    if (this.transportation === Travel.MODE_PLANE) {
      cdistance += 95
    }
    // facteur multiplicateur en fonction du mode de deplacement
    cdistance = cdistance * Travel.transportationsCorrectionFactors[this.transportation]
    return cdistance
  }

  getFullDistance () {
    let cdistance = this.getCorrectedDistance()
    if (this.transportation === Travel.MODE_CAB) {
      cdistance = cdistance * (1 + 1 / this.carpooling)
    } else if (this.transportation === Travel.MODE_CAR) {
      cdistance = cdistance / this.carpooling
    }
    if (this.isRoundTrip) {
      cdistance = cdistance * 2
    }
    return cdistance
  }

  computeGeodesicDist (departureCityLat, departureCityLng, destinationCityLat, destinationCityLng) {
    if (departureCityLat !== undefined && departureCityLat !== null && departureCityLat !== 0) {
      this.departureCityLat = departureCityLat
      this.departureCityLng = departureCityLng
      this.destinationCityLat = destinationCityLat
      this.destinationCityLng = destinationCityLng
      this.distance = TravelSection.geodesicDist(departureCityLat, departureCityLng, destinationCityLat, destinationCityLng)
    }
  }

  computeType () {
    if (this.departureCountry !== null && this.destinationCountry !== null) {
      this.type = 'IN'
      if (this.departureCountry === 'FR' && this.destinationCountry === 'FR') {
        this.type = 'NA'
      } else if (this.departureCountry === 'FR' || this.destinationCountry === 'FR') {
        this.type = 'MX'
      }
    }
  }

  getEmissionFactor (distance, GHGIYear, withContrails = false) {
    let ef = null
    this.computeType()
    if (!withContrails && this.transportation === Travel.MODE_PLANE) {
      if (distance < 1000) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['shorthaul'], GHGIYear)
      } else if (distance < 3501) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['mediumhaul'], GHGIYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['longhaul'], GHGIYear)
      }
    } else if (withContrails && this.transportation === Travel.MODE_PLANE) {
      if (distance < 1000) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['shorthaul.contrails'], GHGIYear)
      } else if (distance < 3501) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['mediumhaul.contrails'], GHGIYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['longhaul.contrails'], GHGIYear)
      }
    } else if (this.transportation === Travel.MODE_CAR || this.transportation === Travel.MODE_CAB) {
      ef = filterEFByYear(VEHICLES_FACTORS['car']['unknown.engine'], GHGIYear)
    } else if (this.transportation === Travel.MODE_BUS) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['bus']['bus.bigcity'], GHGIYear)
    } else if (this.transportation === Travel.MODE_TRAIN) {
      if (this.type === 'NA') {
        if (distance < 200) {
          ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.shortdistance'], GHGIYear)
        } else {
          ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.longdistance'], GHGIYear)
        }
      } else if (this.type === 'MX') {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.mixed'], GHGIYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.international'], GHGIYear)
      }
    } else if (this.transportation === Travel.MODE_TRAM) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['tram.bigcity'], GHGIYear)
    } else if (this.transportation === Travel.MODE_RER) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['rer'], GHGIYear)
    } else if (this.transportation === Travel.MODE_SUBWAY) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['subway'], GHGIYear)
    } else if (this.transportation === Travel.MODE_FERRY) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['boat']['ferry'], GHGIYear)
    }
    return ef
  }

  getCarbonIntensity (GHGIYear, amount) {
    let cdistance = this.getCorrectedDistance()
    if (this.transportation === Travel.MODE_CAB) {
      cdistance = cdistance * (1 + 1 / this.carpooling)
    } else if (this.transportation === Travel.MODE_CAR) {
      cdistance = cdistance / this.carpooling
    }
    // choose ef considering the corrected distance
    let ef = this.getEmissionFactor(cdistance, GHGIYear)
    let efwc = this.getEmissionFactor(cdistance, GHGIYear, true)
    if (this.isRoundTrip) {
      cdistance = cdistance * 2
    }
    if (this.transportation === Travel.MODE_PLANE) {
      return new CarbonIntensity(
        cdistance * amount * ef.total.total,
        cdistance * amount * ef.total.total * ef.total.uncertainty,
        cdistance * amount * efwc.total.total,
        cdistance * amount * efwc.total.total * efwc.total.uncertainty,
        ef.group
      )
    } else {
      return new CarbonIntensity(
        cdistance * amount * ef.total.total,
        cdistance * amount * ef.total.total * ef.total.uncertainty,
        null,
        null,
        ef.group
      )
    }
  }

  toDatabase () {
    this.computeType()
    return {
      'type': this.type,
      'isRoundTrip': this.isRoundTrip,
      'distance': this.distance,
      'transportation': this.transportation,
      'carpooling': this.carpooling
    }
  }

  static getIsRoundTrip (roundTrip) {
    let modifiedRoundTrip = roundTrip.toLowerCase()
    if (modifiedRoundTrip in ROUND_TRIP_DICTIONARY) {
      return ROUND_TRIP_DICTIONARY[modifiedRoundTrip]
    } else {
      return roundTrip
    }
  }

  static createFromObj (section) {
    let nsection = new TravelSection(
      section.transportation,
      section.departureCity,
      section.departureCountry,
      section.destinationCity,
      section.destinationCountry,
      section.carpooling,
      section.distance,
      section.departureCityLat,
      section.departureCityLng,
      section.destinationCityLat,
      section.destinationCityLng,
      section.type,
      section.isRoundTrip
    )
    nsection.computeGeodesicDist(
      section.departureCityLat,
      section.departureCityLng,
      section.destinationCityLat,
      section.destinationCityLng
    )
    nsection.computeType()
    return nsection
  }

  static geodesicDist (lat1, lon1, lat2, lon2) {
    if ((lat1 === lat2) && (lon1 === lon2)) {
      return 0
    } else {
      var radlat1 = Math.PI * lat1 / 180
      var radlat2 = Math.PI * lat2 / 180
      var theta = lon1 - lon2
      var radtheta = Math.PI * theta / 180
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
      if (dist > 1) {
        dist = 1
      }
      dist = Math.acos(dist)
      dist = dist * 180 / Math.PI
      dist = dist * 60 * 1.1515
      dist = dist * 1.609344
      return dist
    }
  }
}
