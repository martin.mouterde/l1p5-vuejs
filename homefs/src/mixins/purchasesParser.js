import * as d3 from 'd3-dsv'

export const purchasesParser = {
  methods: {
    loadFile: function (dataTxt) {
      // Find separator from first line
      let headerLine = dataTxt.split('\n')[0]
      let separators = [',', ';', '\t']
      let bestSep = null
      let bestSepCount = 0
      for (let sep of separators) {
        let count = (headerLine.match(new RegExp(sep, 'gi')) || []).length
        if (count > bestSepCount) {
          bestSepCount = count
          bestSep = sep
        }
      }
      if (!bestSep) {
        return [[], {}, 'bad separator']
      }

      let psv = d3.dsvFormat(bestSep)
      let data = psv.parse(dataTxt)

      let headerMap = this.parseHeaders(data)

      if (!(headerMap.icode || headerMap.iamount)) {
        return [[], {}, 'no code or amount']
      }
      return [data, headerMap, 'ok']
      // return this.mergeRawItems(data, headerMap)
    },

    // Merge rows of data having the exact same code
    // and sum the amount
    mergeRawItems: function (data, headerMap) {
      let newData = []
      data = data.sort((a, b) => a[headerMap.icode].localeCompare(b[headerMap.icode]))
      for (let item of data) {
        if (item[headerMap.iamount] >= 0) {
          if (newData.length > 0 && newData[newData.length - 1][headerMap.icode] === item[headerMap.icode]) {
            newData[newData.length - 1][headerMap.iamount] += parseFloat(item[headerMap.iamount])
          } else {
            newData.push(item)
            newData[newData.length - 1][headerMap.iamount] = parseFloat(item[headerMap.iamount])
          }
        } else {
          return [[], {}, 'negative value']
        }
      }
      return [newData, headerMap, 'ok']
    },

    // Search for code nacre and amount columns
    // within data.columns, and return the matches as
    // {icode,iamount}
    parseHeaders: function (data) {
      let findHeader = function (regex) {
        for (let k in data.columns) {
          var name = data.columns[k]
          if (name.search(regex) >= 0) {
            return name
          }
        }
        return null
      }
      return {
        icode: findHeader(/(code|nacre|code.nacre|codenacre|nacrecode)/i),
        iamount: findHeader(/(montant|amount|somme|total|€)/i)
      }
    }
  }
}
