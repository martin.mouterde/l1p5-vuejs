/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

export const outliersDetection = {
  data () {
    return {
      outliers: [],
      upperLim: 0,
      lowerLim: 0,
      asymetricCoefficient: 0
    }
  },
  methods: {
    quantile: function (arr, q) {
      let sorted = arr.sort((a, b) => a - b)
      let pos = (sorted.length - 1) * q
      let base = Math.floor(pos)
      let rest = pos - base
      if (sorted[base + 1] !== undefined) {
        return sorted[base] + rest * (sorted[base + 1] - sorted[base])
      } else {
        return sorted[base]
      }
    },
    detectOutliers: function (allValues) {
      this.outliers = []
      let ldata = allValues.map(val => Math.log(val + 1))
      let q1 = this.quantile(ldata, 0.25)
      let q3 = this.quantile(ldata, 0.75)
      let iqr = q3 - q1
      this.upperLim = q3 + (3 * iqr)
      this.lowerLim = q1 - (3 * iqr)
      for (let val of allValues) {
        let lval = Math.log(val + 1)
        // the value is outlier
        if (lval > this.upperLim || lval < this.lowerLim) {
          this.outliers.push(val)
        }
      }
      this.computeAsymetricCoefficient(allValues)
    },
    computeAsymetricCoefficient: function (allValues) {
      this.asymetricCoefficient = 0
      let mean = allValues.reduce((a, b) => a + b, 0) / allValues.length
      let median = this.quantile(allValues, 0.5)
      let std = Math.sqrt(allValues.map(val => Math.pow((val - mean), 2)).reduce((a, b) => a + b, 0) / (allValues.length - 1))
      this.asymetricCoefficient = Math.round((3 * (mean - median) / std) * 100) / 100
    },
    isOutlier: function (value) {
      return this.outliers.includes(value)
    },
    isAsymetric: function () {
      return this.asymetricCoefficient > 0.5
    }
  }
}
