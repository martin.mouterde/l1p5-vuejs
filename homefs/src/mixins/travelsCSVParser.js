
import Travel from '@/models/carbon/Travel.js'
import * as d3 from 'd3-dsv'

const columnDescriptions = {
  names: {
    en: '# trip',
    fr: '# mission',
    geslab: 'Numéro mission',
    pattern: /(# |Num.ro |)(mission|trip)/i,
    converter: x => [x] },
  date: { en: 'Departure date', fr: 'Date de départ' },
  departureCity: { en: 'Departure city', fr: 'Ville de départ' },
  departureCountry: { en: 'Country of departure', fr: 'Pays de départ', converter: Travel.iso3166Country },
  destinationCity: { en: 'Destination city', fr: 'Ville de destination' },
  destinationCountry: { en: 'Country of destination', fr: 'Pays de destination', converter: Travel.iso3166Country },
  transportation: {
    en: 'Mode of transportation ',
    fr: 'Mode de déplacement',
    geslab: 'Moyens de transport',
    pattern: /(mode|moyens).*(transport|d.placement)/i },
  carpooling: {
    en: 'No of persons in the car',
    fr: 'Nb de personnes dans la voiture',
    geslab: 'Nb de pers. dans la voiture' },
  isRoundTrip: { en: 'Roundtrip', fr: 'Aller / Retour', pattern: /(Roundtrip|Aller.*Retour)/i },
  purpose: { en: 'Purpose of the trip', fr: 'Motif du déplacement', optional: true },
  status: {
    en: 'Agent position',
    fr: 'Statut de l\'agent',
    pattern: /(Agent position|Statut.*agent)/i,
    optional: true }
}

export const travelsCSVParser = {
  methods: {
    getSeparator (txt) {
      let headerLine = txt.split('\n')[0]
      const separators = [',', ';', '\t']
      let bestSep
      let bestSepCount = 0
      for (let sep of separators) {
        let count = (headerLine.match(new RegExp(sep, 'gi')) || []).length
        if (count > bestSepCount) {
          bestSepCount = count
          bestSep = sep
        }
      }
      return bestSep
    },
    autoParseCSV (txt, rowfunc = d => d) {
      let sep = this.getSeparator(txt)
      if (!sep) {
        throw new Error('error-msg-csv-separator')
      }
      let parser = d3.dsvFormat(sep)
      return parser.parse(txt, rowfunc)
    },
    parseTravels (txt) {
      let self = this
      let headerLine = txt.split('\n')[0]
      let headerData = this.autoParseCSV(headerLine)
      let headerMap = {}
      let labels = ['en', 'fr', 'geslab']
      let isGeslab = headerLine.search('Groupe labo') >= 0

      if (isGeslab && this.getSeparator(headerLine) !== '\t') {
        throw new Error('error-msg-csv-geslabsep')
      }

      Object.entries(columnDescriptions).forEach(function ([outName, desc]) {
        let pattern = ''
        if ('pattern' in desc) {
          pattern = desc.pattern
        } else {
          pattern = new RegExp('(' +
            Object.entries(desc)
              .filter(k => labels.includes(k[0]))
              .map(k => k[1].replace(/[éèë]/, '.'))
              .join('|') +
            ')', 'i')
        }
        let matchedName
        for (let inName of headerData.columns) {
          if (inName.search(pattern) >= 0) {
            matchedName = inName
            break
          }
        }
        if ((!('optional' in desc && desc.optional)) && (!matchedName)) {
          let name = ''
          if (self.$i18n.locale === 'fr') {
            name = isGeslab && 'geslab' in desc ? desc.geslab : desc.fr
          } else {
            name = desc.en
          }
          let err = new Error('error-msg-csv-missing')
          err.params = { 'name': name }
          throw err
        }
        headerMap[outName] = { inName: matchedName, converter: 'converter' in desc ? desc.converter : x => x }
      })

      if (isGeslab) {
        headerMap.transportation.converter = self.convertGeslabModeDeplacement
        headerMap.purpose.converter = self.getGeslabMotifDeplacement
      }

      let data = this.autoParseCSV(txt, function (inRow) {
        let outRow = {}
        Object.entries(headerMap).forEach(function ([outName, params]) {
          if (params.inName) {
            let value = inRow[params.inName].replace('"', '').trim()
            try {
              outRow[outName] = params.converter(value)
            } catch (error) {
              outRow[outName] = value
            }
          } else {
            outRow[outName] = ''
          }
        })
        return outRow
      })
      return data
    },
    convertGeslabModeDeplacement (transportation) {
      let finalMode = ''
      if (transportation === 'Avion') {
        finalMode = Travel.MODE_PLANE
      } else if (transportation === 'Bateau') {
        finalMode = Travel.MODE_FERRY
      } else if (transportation === 'Bus') {
        finalMode = Travel.MODE_BUS
      } else if (transportation === 'Divers') {
        throw new Error('error-msg-csv-divers')
      } else if (transportation === 'Location de véhicule') {
        finalMode = Travel.MODE_CAR
      } else if (transportation === 'Metro') {
        finalMode = Travel.MODE_SUBWAY
      } else if (transportation === 'Passager') {
        throw new Error('error-msg-csv-passager')
      } else if (transportation === 'Rer') {
        finalMode = Travel.MODE_RER
      } else if (transportation === 'Taxi') {
        finalMode = Travel.MODE_CAB
      } else if (transportation === 'Train') {
        finalMode = Travel.MODE_TRAIN
      } else if (transportation === 'Véhicule administratif') {
        throw new Error('error-msg-csv-admin')
      } else if (transportation === 'Véhicule personnel') {
        finalMode = Travel.MODE_CAR
      }
      return finalMode
    },
    getDominantModeDeplacement (allModes) {
      let finalMode = ''
      let modesOrder = [
        Travel.MODE_PLANE,
        Travel.MODE_TRAIN,
        Travel.MODE_FERRY,
        Travel.MODE_BUS,
        Travel.MODE_CAR,
        Travel.MODE_RER,
        Travel.MODE_SUBWAY,
        Travel.MODE_CAB
      ]
      for (let mode of modesOrder) {
        if (allModes.includes(mode)) {
          finalMode = mode
          break
        }
      }
      return finalMode
    },
    getGeslabModeDeplacement (transportation) {
      let allModes = transportation.split(',')
      let finalMode = ''
      if (allModes.length === 1) {
        finalMode = this.convertGeslabModeDeplacement(transportation)
      } else {
        let convertedAllModes = allModes.map(value => {
          try {
            return this.convertGeslabModeDeplacement(value)
          } catch {
            return ''
          }
        })
        // test if only blank value obtained from catch
        let nbBlank = 0
        for (let value of convertedAllModes) {
          if (value === '') {
            nbBlank += 1
          }
        }
        if (nbBlank === convertedAllModes.length) {
          let err = new Error('error-msg-csv-unknown')
          err.params = { 'transportation': transportation }
          throw err
        }
        finalMode = this.getDominantModeDeplacement(convertedAllModes)
      }
      finalMode = Travel.getTransportation(finalMode)
      if (finalMode === null) {
        let err = new Error('error-msg-csv-unknown')
        err.params = { 'transportation': transportation }
        throw err
      }
      return finalMode
    },
    getGeslabMotifDeplacement (motifDeplacement) {
      let md = Travel.PURPOSE_UNKNOWN
      if (motifDeplacement === 'Acq nouv connaissances&techniq') {
        md = Travel.PURPOSE_SEMINAR
      } else if (motifDeplacement === 'Administration de la recherche') {
        md = Travel.PURPOSE_RESEARCH_MANAGEMENT
      } else if (motifDeplacement === 'Autres') {
        md = Travel.PURPOSE_OTHER
      } else if (motifDeplacement === 'Colloques et congrés') {
        md = Travel.PURPOSE_CONFERENCE
      } else if (motifDeplacement === 'Enseignement dispensé') {
        md = Travel.PURPOSE_TEACHING
      } else if (motifDeplacement === 'Rech. doc. ou sur terrain') {
        md = Travel.PURPOSE_FIELD_STUDY
      } else if (motifDeplacement === 'Rech. en équipe, collaboration') {
        md = Travel.PURPOSE_COLLABORATION
      } else if (motifDeplacement === 'Visite, contact pour projet') {
        md = Travel.PURPOSE_VISIT
      }
      return md
    }
  }
}
