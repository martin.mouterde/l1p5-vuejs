/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

export const commute = {
  methods: {
    exportCommutes: function (GHGIyear) {
      this.$store.dispatch('commutes/compute')
      let results = this.$store.getters['commutes/results']
      let csvContent = 'data:text/csv;charset=utf-8,'
      let now = new Date()
      let month = now.getMonth() + 1
      csvContent += [
        ['seqID', 'Jours de déplacement', 'Statut',
          'Marche JT1 (Km)', 'Vélo JT1 (Km)', 'Vélo électrique JT1 (Km)', 'trottinette électrique JT1 (Km)',
          '2 roues motorisées JT1 (Km)', 'Voiture JT1 (Km)', 'Bus JT1 (Km)', 'Tramway JT1 (Km)', 'Train JT1 (Km)',
          'RER JT1 (Km)', 'Métro JT1 (Km)',
          'Jours JT2',
          'Marche JT2 (Km)', 'Vélo JT2 (Km)', 'Vélo électrique JT2 (Km)', 'trottinette électrique JT2 (Km)',
          '2 roues motorisées JT2 (Km)', 'Voiture JT2 (Km)', 'Bus JT2 (Km)', 'Tramway JT2 (Km)', 'Train JT2 (Km)',
          'RER JT2 (Km)', 'Métro JT2 (Km)',
          'Marche (kg eCO2)', 'Vélo (kg eCO2)', 'Vélo électrique (kg eCO2)', 'trottinette électrique (kg eCO2)',
          '2 roues motorisées (kg eCO2)', 'Voiture (kg eCO2)', 'Bus (kg eCO2)', 'Tramway (kg eCO2)', 'Train (kg eCO2)', 'RER (kg eCO2)',
          'Métro (kg eCO2)', 'Total (kg eCO2)'].join('\t'),
        ...results.totalPerCommute.map(function (item, index) {
          let toReturn = results.meta[index].seqID + '\t' + results.meta[index].nWorkingDay + '\t'
          toReturn += results.meta[index].position + '\t'
          toReturn += results.rawDistance1[index].map(val => Math.round(val)).join('\t')
          toReturn += '\t' + results.meta[index].nWorkingDay2 + '\t'
          toReturn += results.rawDistance2[index].map(val => Math.round(val)).join('\t')
          toReturn += '\t' + item.slice(1, item.length).map(obj => Math.round(obj.intensity)).join('\t')
          toReturn += '\t' + Math.round(item.slice(1, item.length).map(obj => obj.intensity).reduce(function (a, b) {
            return a + b
          }, 0))
          return toReturn
        })
      ]
        .join('\n')
        .replace(/(^\[)|(\]$)/gm, '')
      const data = encodeURI(csvContent)
      const link = document.createElement('a')
      link.setAttribute('href', data)
      link.setAttribute('download', 'GES1point5_ghgi' + GHGIyear + '_commutes_' + now.getFullYear() + '' + month + '' + now.getDate() + '.tsv')
      link.click()
    }
  }
}
