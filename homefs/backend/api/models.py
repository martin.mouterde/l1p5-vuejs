from django.db import models
from django.conf import settings

class Administration(models.Model):
    name = models.CharField(max_length=100)

class Discipline(models.Model):
    name = models.CharField(max_length=100)

class Laboratory(models.Model):
    referent = models.ForeignKey(settings.AUTH_USER_MODEL,
                                 on_delete=models.CASCADE,
                                 related_name='laboratory')
    name = models.CharField(max_length=50)
    administrations = models.ManyToManyField(Administration, related_name="laboratories")
    disciplines = models.ManyToManyField(Discipline, through='LaboratoryDiscipline')
    mainSite = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    area = models.CharField(max_length=50)
    citySize = models.IntegerField(null=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=7)
    longitude = models.DecimalField(max_digits=10, decimal_places=7)

class LaboratoryDiscipline(models.Model):
    discipline = models.ForeignKey(Discipline, on_delete=models.CASCADE)
    laboratory = models.ForeignKey(Laboratory, on_delete=models.CASCADE)
    percentage = models.IntegerField()

class Site(models.Model):
    name = models.CharField(max_length=50)
    laboratory = models.ForeignKey(Laboratory, related_name='sites', on_delete=models.CASCADE)
