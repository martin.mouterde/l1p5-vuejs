from rest_framework import serializers
from .models import Laboratory, Administration, Discipline, Site
from ..users.serializers import L1P5UserSerializer

class SitesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Site
        fields = '__all__'

class AdministrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Administration
        fields = '__all__'

class DisciplineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Discipline
        fields = '__all__'

class LaboratorySerializer(serializers.ModelSerializer):
    administrations = AdministrationSerializer(many=True)
    sites = SitesSerializer(many=True)
    disciplines = DisciplineSerializer(many=True)
    referent = L1P5UserSerializer(many=False)
    class Meta:
        model = Laboratory
        fields = '__all__'
