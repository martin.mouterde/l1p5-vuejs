from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status
from django.core.mail import send_mail
from django.conf import settings
import copy

from .models import Laboratory, Administration, Discipline, Site, LaboratoryDiscipline
from .serializers import AdministrationSerializer, DisciplineSerializer, LaboratorySerializer

# Serve Vue Application
index_view = never_cache(TemplateView.as_view(template_name='index.html'))

@api_view(['GET'])
@permission_classes([])
def get_administrations(request):
    administrations = Administration.objects.all()
    serializer = AdministrationSerializer(administrations, many=True)
    return Response(serializer.data)

@api_view(['GET'])
@permission_classes([])
def get_disciplines(request):
    sous_domaines = Discipline.objects.all()
    serializer = DisciplineSerializer(sous_domaines, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def save_laboratory(request):
    if request.user.is_authenticated:
        # sub objects data
        sites_data = request.data.pop('sites')
        administrations_data = request.data.pop('administrations')
        disciplines_data = request.data.pop('disciplines')
        request.data["referent"] = request.user
        # build the laboratory object if exists or create it
        laboratory = Laboratory.objects.filter(referent=request.user)
        lid = None
        if len(laboratory) == 1:
            lid = laboratory[0].id
        laboratory, created = Laboratory.objects.update_or_create(id=lid, defaults=request.data)
        laboratory.administrations.clear()
        for administration_data in administrations_data:
            if (type(administration_data) is str) :
                administration = Administration.objects.create(name=administration_data)
                laboratory.administrations.add(administration)
            else :
                administration = Administration.objects.get(id=administration_data['id'])
                laboratory.administrations.add(administration)
        laboratory.disciplines.clear()
        for discipline_data in disciplines_data:
            d, created = LaboratoryDiscipline.objects.update_or_create(
                discipline_id=discipline_data['id'],
                laboratory_id=laboratory.id,
                percentage=discipline_data['percentage']
            )
        lab_sites = Site.objects.filter(laboratory_id=laboratory.id)
        for lab_site in lab_sites:
            lab_site.delete()
        for site_data in sites_data:
            s = Site.objects.create(name=site_data, laboratory=laboratory)
            s.save()
        serializer = LaboratorySerializer(laboratory)
        disciplines = LaboratoryDiscipline.objects.filter(laboratory_id=laboratory.id)
        to_return = copy.deepcopy(serializer.data)
        to_return['disciplines'] = []
        for discipline in disciplines:
            disc_name = Discipline.objects.filter(id=discipline.discipline_id)[0].name
            to_return["disciplines"].append({
                'id': discipline.discipline_id,
                'name': disc_name,
                'percentage': discipline.percentage
            })
        return Response(to_return, status=status.HTTP_201_CREATED)
    return Response(Laboratory.objects.none())

@api_view(['GET'])
def get_laboratory(request):
    if request.user.is_authenticated:
        laboratory = Laboratory.objects.filter(referent=request.user)
        if laboratory.exists():
            serializer = LaboratorySerializer(laboratory[0])
            to_return = copy.deepcopy(serializer.data)
            disciplines = LaboratoryDiscipline.objects.filter(laboratory_id=laboratory[0].id)
            to_return["disciplines"] = []
            for discipline in disciplines:
                disc_name = Discipline.objects.filter(id=discipline.discipline_id)[0].name
                to_return["disciplines"].append({
                    'id': discipline.discipline_id,
                    'name': disc_name,
                    'percentage': discipline.percentage
                })
            return Response(to_return)
        else :
            return Response(None)
    return Response(None, status=status.HTTP_400_BAD_REQUEST)
