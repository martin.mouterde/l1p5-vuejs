"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""

from django.contrib import admin
from django.urls import path, include, re_path
import django.contrib.auth.views as auth_views
from rest_framework import routers

from .api import views as apiviews
from .carbon import views as carbonviews
from .users import views as usersviews
from .scenario import views as scenarioviews
from .transition import views as transitionviews

router = routers.DefaultRouter()

urlpatterns = [

    # http://localhost:8000/
    path('', apiviews.index_view, name='index'),

    # http://localhost:8000/api/<router-viewsets>
    path('api/', include(router.urls)),

    # handle users
    path('api/is_super_user/', usersviews.is_super_user, name='is_super_user'),
    path('api/auth/registration/', usersviews.RegisterView.as_view()),
    path('api/user_exists/', usersviews.user_exists, name='user_exists'),
    path('api/reset_password/', usersviews.PasswordResetView.as_view(), name='rest_password_reset'),
    path('accounts/reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('accounts/reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('accounts/activate_account/<uidb64>/<token>/', usersviews.activate_account, name='activate_account'),

    # api module
    path('api/save_laboratory/', apiviews.save_laboratory, name='save_laboratory'),
    path('api/get_laboratory/', apiviews.get_laboratory, name='get_laboratory'),    
    path('api/get_administrations/', apiviews.get_administrations, name='get_administrations'),
    path('api/get_disciplines/', apiviews.get_disciplines, name='get_disciplines'),

    # carbon module
    path('api/get_counts/', carbonviews.get_counts, name='get_counts'),
    path('api/save_ghgi/', carbonviews.save_ghgi, name='save_ghgi'),
    path('api/delete_ghgi/', carbonviews.delete_ghgi, name='delete_ghgi'),
    path('api/get_all_ghgi/', carbonviews.get_all_ghgi, name='get_all_ghgi'),
    path('api/get_all_ghgi_admin/', carbonviews.get_all_ghgi_admin, name='get_all_ghgi_admin'),
    path('api/get_all_ghgi_with_consumptions_admin/', carbonviews.get_all_ghgi_with_consumptions_admin, name='get_all_ghgi_with_consumptions_admin'),
    path('api/unsubmit_data/', carbonviews.unsubmit_data, name='unsubmit_data'),
    path('api/get_ghgi_consumptions/', carbonviews.get_ghgi_consumptions, name='get_ghgi_consumptions'),
    path('api/get_ghgi_consumptions_by_uuid/', carbonviews.get_ghgi_consumptions_by_uuid, name='get_ghgi_consumptions_by_uuid'),
    path('api/save_vehicles/', carbonviews.save_vehicles, name='save_vehicles'),
    path('api/get_all_vehicles/', carbonviews.get_all_vehicles, name='get_all_vehicles'),
    path('api/save_buildings/', carbonviews.save_buildings, name='save_buildings'),
    path('api/get_all_buildings/', carbonviews.get_all_buildings, name='get_all_buildings'),
    path('api/save_commutes/', carbonviews.save_commutes, name='save_commutes'),
    path('api/save_commute/', carbonviews.save_commute, name='save_commute'),
    path('api/save_travels/', carbonviews.save_travels, name='save_travels'),
    path('api/save_computer_devices/', carbonviews.save_computer_devices, name='save_computer_devices'),
    path('api/save_purchases/', carbonviews.save_purchases, name='save_purchases'),
    path('api/submit_data/', carbonviews.submit_data, name='submit_data'),

    path('api/get_ghgi_survey_info/', carbonviews.get_ghgi_survey_info, name='get_ghgi_survey_info'),
    path('api/activate_ghgi_survey/', carbonviews.activate_ghgi_survey, name='activate_ghgi_survey'),
    path('api/save_survey_message/', carbonviews.save_survey_message, name='save_survey_message'),
    path('api/clone_survey/', carbonviews.clone_survey, name='clone_survey'),
    
    # scenario module
    path('api/save_scenario/', scenarioviews.save_scenario, name='save_scenario'),
    path('api/get_scenario/', scenarioviews.get_scenario, name='get_scenario'),
    path('api/delete_scenario/', scenarioviews.delete_scenario, name='delete_scenario'),
    path('api/get_scenarios/', scenarioviews.get_scenarios, name='get_scenarios'),

    # transition module
    path('api/get_initiatives/', transitionviews.get_initiatives, name='get_initiatives'),
    path('api/get_initiatives_admin/', transitionviews.get_initiatives_admin, name='get_initiatives_admin'),
    path('api/validate_initiative/', transitionviews.validate_initiative, name='validate_initiative'),
    path('api/get_initiative/', transitionviews.get_initiative, name='get_initiative'),
    path('api/save_initiative/', transitionviews.save_initiative, name='save_initiative'),
    
    # admin
    path('api/admin/', admin.site.urls),

    # authentication and JWT Token
    path('api/token/', usersviews.L1P5TokenObtainPairView.as_view(), name='token_obtain_pair'),
    #path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    # Entry point of the VueJS web application
    re_path(r'^.*$', apiviews.index_view, name='entry_point')
]
