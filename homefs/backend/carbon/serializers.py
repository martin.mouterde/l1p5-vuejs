from rest_framework import serializers
from .models import GHGI, Electricity, Heating, Vehicle, VehicleConsumption, Building, SelfConsumption, Refrigerant, Commute, Travel, TravelSection, ComputerDevice, Purchase

from ..api.serializers import LaboratorySerializer

class HeatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Heating
        fields = '__all__'

class ElectricitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Electricity
        fields = '__all__'

class RefrigerantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Refrigerant
        fields = '__all__'

class SelfConsumptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = SelfConsumption
        fields = '__all__'

class BuildingSerializer(serializers.ModelSerializer):
    heatings = HeatingSerializer(many=True)
    electricity = ElectricitySerializer(many=True)
    refrigerants = RefrigerantSerializer(many=True)
    selfConsumption = SelfConsumptionSerializer(many=True)
    class Meta:
        model = Building
        fields = '__all__'

class BuildingWCSerializer(serializers.ModelSerializer):
    class Meta:
        model = Building
        fields = '__all__'

class CommuteSerializer(serializers.ModelSerializer):        
    class Meta:
        model = Commute
        fields = '__all__'

class TravelSectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TravelSection
        fields = ['id', 'distance', 'type', 'isRoundTrip', 'transportation', 'carpooling']

class TravelSerializer(serializers.ModelSerializer):
    sections = TravelSectionSerializer(many=True, read_only=True)
    names = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='name'
    )
    class Meta:
        model = Travel
        fields = ['id', 'ghgi', 'location', 'purpose', 'status',
                  'amount', 'sections', 'names']

class VehicleConsumptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleConsumption
        fields = '__all__'

class VehicleSerializer(serializers.ModelSerializer):
    consumption = VehicleConsumptionSerializer(many=True)
    class Meta:
        model = Vehicle
        fields = '__all__'

class VehicleWCSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = '__all__'

class PurchaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Purchase
        fields = '__all__'

class ComputerDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComputerDevice
        fields = '__all__'

class GHGISerializer(serializers.ModelSerializer):
    laboratory = LaboratorySerializer(many=False)
    class Meta:
        model = GHGI
        fields = '__all__'
