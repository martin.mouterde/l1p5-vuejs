import copy

from django.core.exceptions import ValidationError
from django.db.models import Q
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status

from .models import GHGI, Building, Electricity, Heating, Refrigerant, SelfConsumption, Vehicle, VehicleConsumption, Commute, Travel, TravelSection, TravelNames, ComputerDevice, Purchase
from .serializers import ElectricitySerializer, GHGISerializer, BuildingSerializer, BuildingWCSerializer, HeatingSerializer, RefrigerantSerializer, SelfConsumptionSerializer, VehicleSerializer, VehicleWCSerializer, CommuteSerializer, TravelSerializer, ComputerDeviceSerializer, PurchaseSerializer

from ..api.models import Laboratory
from ..api.serializers import LaboratorySerializer
from ..transition.models import Initiative
from ..users.models import L1P5User

@api_view(['GET'])
@permission_classes([])
def get_counts(request):
    labs_id = []
    all_labs_with_ghgi = GHGI.objects.only('laboratory')
    for ghgi in all_labs_with_ghgi:
        labs_id.append(ghgi.laboratory.id)
    response =  Response({
        'nbInitiative': Initiative.objects.filter(valid=1).count(),
        'nGHGI': GHGI.objects.count(),
        'nLaboratory': len(set(labs_id))
    },
    status=status.HTTP_201_CREATED)
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Allow-Methods"] = "GET, OPTIONS"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"
    return response

@api_view(['POST'])
def save_ghgi(request):
    ghgi_id = request.data.pop('id')
    try:
        laboratory = Laboratory.objects.get(referent_id=request.user.id)
        request.data["laboratory"] = laboratory
        ghgi, created = GHGI.objects.update_or_create(id=ghgi_id, defaults=request.data)
        serializer = GHGISerializer(ghgi)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    except Laboratory.DoesNotExist as err:
        return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def delete_ghgi(request):
    ghgi_id = request.data.pop('ghgi_id')
    try:
        ghgi = GHGI.objects.get(id=ghgi_id)
        ghgi.delete()
        laboratory = Laboratory.objects.get(referent_id=request.user.id)
        allghgi = GHGI.objects.filter(laboratory_id=laboratory.id).order_by('-year')
        serializer = GHGISerializer(allghgi, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    except GHGI.DoesNotExist as err:
        return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def get_all_ghgi(request):
    if request.user.is_authenticated:
        ghgi_to_return = []
        #try:
        laboratory = Laboratory.objects.get(referent_id=request.user.id)
        lserializer = LaboratorySerializer(laboratory)
        all_ghgi = GHGI.objects.filter(laboratory_id=laboratory.id).order_by('-year')
        for ghgi in all_ghgi:
            all_data = get_ghgi_consumption_data(ghgi.id)
            serializer = GHGISerializer(ghgi)
            final_ghgi = copy.deepcopy(serializer.data)
            final_ghgi["vehicles"] = copy.deepcopy(all_data["vehicles"])
            final_ghgi["buildings"] = copy.deepcopy(all_data["buildings"])
            final_ghgi["commutes"] = copy.deepcopy(all_data["commutes"])
            final_ghgi["travels"] = copy.deepcopy(all_data["travels"])
            final_ghgi["devices"] = copy.deepcopy(all_data["devices"])
            final_ghgi["purchases"] = copy.deepcopy(all_data["purchases"])
            ghgi_to_return.append(final_ghgi)
        return Response(ghgi_to_return, status=status.HTTP_201_CREATED)
        #except:
        #    return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def get_all_ghgi_with_consumptions_admin(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            ghgi_to_return = []
            filters = {}
            if request.data['buildingsSubmitted']:
                filters['buildingsSubmitted'] = True
            if request.data['vehiclesSubmitted']:
                filters['vehiclesSubmitted'] = True
            if request.data['purchasesSubmitted']:
                filters['purchasesSubmitted'] = True
            if request.data['devicesSubmitted']:
                filters['devicesSubmitted'] = True
            if request.data['travelsSubmitted']:
                filters['travelsSubmitted'] = True
            if request.data['commutesSubmitted']:
                filters['commutesSubmitted'] = True
            all_ghgi = GHGI.objects.filter(**filters)
            for ghgi in all_ghgi:
                all_data = get_ghgi_consumption_data(ghgi.id)
                serializer = GHGISerializer(ghgi)
                final_ghgi = copy.deepcopy(serializer.data)
                final_ghgi["vehicles"] = copy.deepcopy(all_data["vehicles"])
                final_ghgi["buildings"] = copy.deepcopy(all_data["buildings"])
                final_ghgi["commutes"] = copy.deepcopy(all_data["commutes"])
                final_ghgi["travels"] = copy.deepcopy(all_data["travels"])
                final_ghgi["devices"] = copy.deepcopy(all_data["devices"])
                final_ghgi["purchases"] = copy.deepcopy(all_data["purchases"])
                ghgi_to_return.append(final_ghgi)
            return Response(ghgi_to_return, status=status.HTTP_201_CREATED)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def get_all_ghgi_admin(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            all_ghgi = GHGI.objects.all()
            serializer = GHGISerializer(all_ghgi, many=True)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def unsubmit_data(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            GHGI.objects.update_or_create(
                id=request.data['ghgi'],
                defaults={ request.data['variable']: False }
            )
            all_ghgi = GHGI.objects.all()
            serializer = GHGISerializer(all_ghgi, many=True)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def save_vehicles(request):
    laboratory = Laboratory.objects.get(referent_id=request.user.id)
    ghgi_id = request.data.pop('ghgi_id')
    ghgi = GHGI.objects.get(id=ghgi_id)
    ghgi_consumptions = []
    for vehicle_data in request.data['vehicles']:
        vehicle_id = vehicle_data.pop('id')
        consumption_data = vehicle_data.pop('consumption')
        consumption_id = consumption_data.pop('id')
        vehicle_data['laboratory'] = laboratory
        vehicle, created = Vehicle.objects.update_or_create(id=vehicle_id, defaults=vehicle_data)
        consumption_data['ghgi'] = ghgi
        consumption_data['vehicle'] = vehicle
        consumption, created = VehicleConsumption.objects.update_or_create(id=consumption_id, defaults=consumption_data)
        ghgi_consumptions.append(consumption.id)
    # Delete consumptions no longer defined
    consumptions = VehicleConsumption.objects.filter(ghgi_id=ghgi_id)
    for consumption in consumptions:
        if not consumption.id in ghgi_consumptions:
            consumption.delete()
    # Delete vehicles with no consumption
    vehicles = Vehicle.objects.filter(laboratory_id=laboratory.id)
    for vehicle in vehicles:
        consumptions = VehicleConsumption.objects.filter(vehicle_id=vehicle.id)
        if len(consumptions) == 0:
            vehicle.delete()
    return Response(
        get_ghgi_consumption_data(ghgi_id)['vehicles'],
        status=status.HTTP_201_CREATED
    )

@api_view(['POST'])
def save_computer_devices(request):
    ghgi_id = request.data.pop('ghgi_id')
    ComputerDevice.objects.filter(ghgi_id=ghgi_id).delete()
    # ajouter les nouveaux
    for computer_device_data in request.data['devices']:
        computer_device_data['ghgi_id'] = ghgi_id
        computer_device = ComputerDevice.objects.create(**computer_device_data)
    computer_devices = ComputerDevice.objects.filter(ghgi_id=ghgi_id)
    serializer = ComputerDeviceSerializer(computer_devices, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def save_purchases(request):
    ghgi_id = request.data.pop('ghgi_id')
    Purchase.objects.filter(ghgi_id=ghgi_id).delete()
    # ajouter les nouveaux
    for purchase_data in request.data['purchases']:
        purchase_data['ghgi_id'] = ghgi_id
        purchase = Purchase.objects.create(**purchase_data)
    purchases = Purchase.objects.filter(ghgi_id=ghgi_id)
    serializer = PurchaseSerializer(purchases, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def get_all_vehicles(request):
    try:
        laboratory = Laboratory.objects.get(referent_id=request.user.id)
        vehicles = Vehicle.objects.filter(laboratory_id=laboratory.id)
        serializer = VehicleWCSerializer(vehicles, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    except:
        return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def save_buildings(request):
    laboratory = Laboratory.objects.get(referent_id=request.user.id)
    ghgi_id = request.data.pop('ghgi_id')
    ghgi = GHGI.objects.get(id=ghgi_id)
    electricities_id = []
    for building_data in request.data['buildings']:
        building_id = building_data.pop('id')
        building_data['laboratory'] = laboratory
        # Remove consumption data from building data to be able to create building
        electricity_data = building_data.pop('electricity')
        heatings_data = building_data.pop('heatings')
        refrigerants = building_data.pop('refrigerants')
        selfconsumption_data = building_data.pop('selfConsumption')
        # Create building if not exists
        building, created = Building.objects.update_or_create(id=building_id, defaults=building_data)
        # Electricity
        electricity_id = electricity_data.pop('id')
        electricity_data['ghgi'] = ghgi
        electricity_data['building'] = building
        electricity, created = Electricity.objects.update_or_create(id=electricity_id, defaults=electricity_data)
        electricities_id.append(electricity.id)
        # Heatings
        all_heating_ids = []
        for heating_data in heatings_data:
            heating_id = heating_data.pop('id')
            heating_data['ghgi'] = ghgi
            heating_data['building'] = building
            heating, created = Heating.objects.update_or_create(id=heating_id, defaults=heating_data)
            all_heating_ids.append(heating.id)
        # Refrigerant gas
        all_gas_ids = []
        for gas_data in refrigerants:
            gas_id = gas_data.pop('id')
            gas_data['ghgi'] = ghgi
            gas_data['building'] = building
            gas, created = Refrigerant.objects.update_or_create(id=gas_id, defaults=gas_data)
            all_gas_ids.append(gas.id)        
        # Selfconsumption
        SelfConsumption.objects.filter(building=building, ghgi=ghgi).delete()
        selfconsumption = SelfConsumption.objects.create(
            building=building,
            ghgi=ghgi,
            total=selfconsumption_data
        )
        # Remove gases and heatings that are no longer associated with the building and ghgi
        gases = Refrigerant.objects.filter(ghgi=ghgi, building=building)
        for gas in gases:
            if not gas.id in all_gas_ids:
                gas.delete()
        heatings = Heating.objects.filter(ghgi=ghgi, building=building)
        for heating in heatings:
            if not heating.id in all_heating_ids:
                heating.delete()
    # Remove electricity consumptions no longer present in the data being saved for the given ghgi
    electricities = Electricity.objects.filter(ghgi_id=ghgi_id, building_id__isnull=False)
    for electricity in electricities:
        if not electricity.id in electricities_id:
            electricity.delete()
    # Remove buildings no longer linked to a consumption thus to a given ghgi
    buildings = Building.objects.filter(laboratory_id=laboratory.id)
    for building in buildings:
        electricity = Electricity.objects.filter(building_id=building.id)
        heating = Heating.objects.filter(building_id=building.id)
        if len(electricity) == 0 and len(heating) == 0:
            building.delete()
    return Response(
        get_ghgi_consumption_data(ghgi_id)['buildings'],
        status=status.HTTP_201_CREATED
    )

@api_view(['POST'])
def get_all_buildings(request):
    try:
        laboratory = Laboratory.objects.get(referent_id=request.user.id)
        buildings = Building.objects.filter(laboratory_id=laboratory.id)
        serializer = BuildingWCSerializer(buildings, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    except:
        return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def save_commutes(request):
    ghgi_id = request.data.pop('ghgi_id')
    # Delete all commutes
    Commute.objects.filter(ghgi_id=ghgi_id).delete()
    # Add the new commutes
    for commute_data in request.data['commutes']:
        commute_data['ghgi_id'] = ghgi_id
        commute = Commute.objects.create(**commute_data)
    commutes = Commute.objects.filter(ghgi_id=ghgi_id)
    serializer = CommuteSerializer(commutes, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@permission_classes([])
def get_ghgi_survey_info(request):
    uuid = request.data.pop('uuid')
    try:
        ghgi = GHGI.objects.get(uuid=uuid)
        return Response({
            'year': ghgi.year,
            'surveyActive': ghgi.surveyActive,
            'surveyMessage': ghgi.surveyMessage,
            'contactEmail': ghgi.laboratory.referent.email,
            'labName': ghgi.laboratory.name,
            'citySize': ghgi.laboratory.citySize
            }, status=status.HTTP_201_CREATED)
    except ValidationError:
        return Response('Unvalid uuid', status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([])
def clone_survey(request):
    ghgi_id = request.data.pop('ghgi_id')
    surveyCloneYear = request.data.pop('surveyCloneYear')
    GHGI.objects.filter(id=ghgi_id).update(surveyCloneYear=surveyCloneYear)
    ghgi = GHGI.objects.get(id=ghgi_id)
    ghgiSerializer = GHGISerializer(ghgi)
    to_return = {}
    to_return['ghgi'] = ghgiSerializer.data
    to_return['commutes'] = get_ghgi_consumption_data(ghgi_id)['commutes']
    return Response(to_return, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def save_survey_message(request):
    ghgi_id = request.data.pop('ghgi_id')
    surveyMessage = request.data.pop('surveyMessage')
    GHGI.objects.filter(id=ghgi_id).update(surveyMessage=surveyMessage)
    return Response({}, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@permission_classes([])
def save_commute(request):
    uuid = request.data.pop('uuid')
    commute_data = request.data.pop('commute')
    ghgi = GHGI.objects.get(uuid=uuid)
    # add the new commute
    commute = Commute.objects.create(ghgi_id=ghgi.id,**commute_data)
    Commute.objects.filter(id=commute.id).update(seqID=commute.id)
    commute = Commute.objects.get(id=commute.id)
    serializer = CommuteSerializer(commute)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def save_travels(request):
    ghgi_id = request.data.pop('ghgi_id')
    # Delete all travels
    Travel.objects.filter(ghgi_id=ghgi_id).delete()
    # Add the new travels
    for travel_data in request.data['travels']:
        travel_data['ghgi_id'] = ghgi_id
        sections = travel_data.pop('sections')
        names = travel_data.pop('names')
        travel = Travel.objects.create(**travel_data)
        for section in sections:
            i = TravelSection.objects.create(
                travel=travel,
                distance=section['distance'],
                transportation=section['transportation'],
                carpooling=section['carpooling'],
                type=section['type'],
                isRoundTrip=section['isRoundTrip']
            )
        for name in names:
            i = TravelNames.objects.create(name=name, travel=travel)
    travels = Travel.objects.filter(ghgi_id=ghgi_id)
    serializer = TravelSerializer(travels, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def submit_data(request):
    ghgi_id = request.data.pop('ghgi_id')
    module = request.data.pop('module')
    if module == 'buildings':
        GHGI.objects.filter(id=ghgi_id).update(buildingsSubmitted=True)
    elif module == 'devices':
        GHGI.objects.filter(id=ghgi_id).update(devicesSubmitted=True)
    elif module == 'commutes':
        GHGI.objects.filter(id=ghgi_id).update(commutesSubmitted=True)
    elif module == 'vehicles':
        GHGI.objects.filter(id=ghgi_id).update(vehiclesSubmitted=True)
    elif module == 'travels':
        GHGI.objects.filter(id=ghgi_id).update(travelsSubmitted=True)
    elif module == 'purchases':
        GHGI.objects.filter(id=ghgi_id).update(purchasesSubmitted=True)
    elif module == 'total':
        GHGI.objects.filter(id=ghgi_id).update(buildingsSubmitted=True)
        GHGI.objects.filter(id=ghgi_id).update(devicesSubmitted=True)
        GHGI.objects.filter(id=ghgi_id).update(commutesSubmitted=True)
        GHGI.objects.filter(id=ghgi_id).update(vehiclesSubmitted=True)
        GHGI.objects.filter(id=ghgi_id).update(travelsSubmitted=True)
        GHGI.objects.filter(id=ghgi_id).update(purchasesSubmitted=True)
    ghgi = GHGI.objects.get(id=ghgi_id)
    serializer = GHGISerializer(ghgi)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def activate_ghgi_survey(request):
    ghgi_id = request.data.pop('ghgi_id')
    ghgi = GHGI.objects.get(id=ghgi_id)
    GHGI.objects.filter(id=ghgi_id).update(surveyActive=not ghgi.surveyActive)
    ghgi = GHGI.objects.get(id=ghgi_id)
    serializer = GHGISerializer(ghgi)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

def get_ghgi_consumption_data(ghgi_id):

    ghgi = GHGI.objects.get(id=ghgi_id)
    to_return = {
        'vehicles': [],
        'buildings': [],
        'commutes': [],
        'travels': [],
        'devices': [],
        'purchases': []
    }

    # add buildings and modify serializer representation from many consumptions to one consumption
    buildings = Building.objects.filter(Q(electricity__ghgi_id=ghgi_id) | Q(heatings__ghgi_id=ghgi_id) | Q(refrigerants__ghgi_id=ghgi_id)).distinct()
    buildings_data = BuildingSerializer(buildings, many=True).data
    for building_data in buildings_data:
        building_data['heatings'] = filter(
            lambda heating: heating['ghgi'] == ghgi_id,
            building_data['heatings']
        )
        filtered_electricity = list(filter(
            lambda electricity: electricity['ghgi'] == ghgi_id,
            building_data['electricity']
        ))
        if len(filtered_electricity) > 0:
            building_data['electricity'] = filtered_electricity[0]
        building_data['refrigerants'] = filter(
            lambda refrigerant: refrigerant['ghgi'] == ghgi_id,
            building_data['refrigerants']
        )
        filtered_selfconsumption = building_data['selfConsumption'] = list(filter(
            lambda sconsumption: sconsumption['ghgi'] == ghgi_id,
            building_data['selfConsumption']
        ))
        if len(filtered_selfconsumption) > 0:
            building_data['selfConsumption'] = filtered_selfconsumption[0]['total']
    to_return['buildings'] = buildings_data

    # add vehicles and modify serializer representation from many consumptions to one consumption
    vehicles = Vehicle.objects.filter(consumption__ghgi_id=ghgi_id).distinct()
    vehicles_data = VehicleSerializer(vehicles, many=True).data
    for vehicle_data in vehicles_data:
        filtered_vehicle = vehicle_data['consumption'] = list(filter(
            lambda vehicle: vehicle['ghgi'] == ghgi_id,
            vehicle_data['consumption']
        ))
        if len(filtered_vehicle) > 0:
            vehicle_data['consumption'] = filtered_vehicle[0]
    to_return['vehicles'] = vehicles_data

    # add commutes considering if its a clone survey
    if ghgi.surveyCloneYear is None:
        commutes = Commute.objects.filter(ghgi_id=ghgi_id)
        commutesSerializer = CommuteSerializer(commutes, many=True)
        to_return['commutes'] = commutesSerializer.data
    else:
        ghgiSurvey = GHGI.objects.get(annee=ghgi.surveyCloneYear, laboratory=ghgi.laboratory)
        commutes = Commute.objects.filter(ghgi_id=ghgiSurvey.id)
        commutesSerializer = CommuteSerializer(commutes, many=True)
        to_return['commutes'] = commutesSerializer.data
    
    # add travels
    travels = Travel.objects.filter(ghgi_id=ghgi_id)
    serializertravels = TravelSerializer(travels, many=True)
    to_return['travels'] = serializertravels.data

    # add computer devices
    computer_devices = ComputerDevice.objects.filter(ghgi_id=ghgi_id)
    serializercd = ComputerDeviceSerializer(computer_devices, many=True)
    to_return['devices'] = serializercd.data

    # add purchases
    purchases = Purchase.objects.filter(ghgi_id=ghgi_id)
    serializerp = PurchaseSerializer(purchases, many=True)
    to_return['purchases'] = serializerp.data

    return to_return

@api_view(['POST'])
def get_ghgi_consumptions(request):
    ghgi_id = request.data.pop('ghgi_id')
    ghgi = GHGI.objects.get(id=ghgi_id)
    to_return = get_ghgi_consumption_data(ghgi.id)
    to_return['ghgi'] = GHGISerializer(ghgi).data
    return Response(to_return, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@permission_classes([])
def get_ghgi_consumptions_by_uuid(request):
    uuid = request.data.pop('uuid')
    ghgi = GHGI.objects.get(uuid=uuid)
    to_return = get_ghgi_consumption_data(ghgi.id)
    to_return['ghgi'] = GHGISerializer(ghgi).data
    return Response(to_return, status=status.HTTP_201_CREATED)