import uuid
from django.db import models
from django.conf import settings


class GHGI(models.Model):
    uuid = models.UUIDField(
        null=True,
        unique = True,
        editable = False
    )
    year = models.IntegerField()
    created = models.DateField(auto_now_add=True)
    nResearcher = models.IntegerField(null=True)
    nProfessor = models.IntegerField(null=True)
    nEngineer = models.IntegerField(null=True)
    nStudent = models.IntegerField(null=True)
    budget = models.IntegerField(null=True)
    surveyMessage = models.CharField(max_length=500, null=True)
    surveyActive = models.BooleanField(default=False)
    laboratory = models.ForeignKey('api.Laboratory', related_name='bges', on_delete=models.CASCADE)
    buildingsSubmitted = models.BooleanField(default=False)
    commutesSubmitted = models.BooleanField(default=False)
    travelsSubmitted = models.BooleanField(default=False)
    vehiclesSubmitted = models.BooleanField(default=False)
    devicesSubmitted = models.BooleanField(default=False)
    purchasesSubmitted = models.BooleanField(default=False)
    surveyCloneYear = models.IntegerField(null=True)

    def save(self, *args, **kwargs):
        ''' On save, update uuid and avoid clash with existing code '''
        if not self.uuid:
            self.uuid = uuid.uuid4()
        return super(GHGI, self).save(*args, **kwargs)

class Commute(models.Model):
    ghgi = models.ForeignKey(GHGI, related_name='commute', on_delete=models.CASCADE)
    seqID = models.CharField(max_length=10, null=True)
    deleted = models.BooleanField(default=False)
    position = models.CharField(max_length=250)
    nWorkingDay = models.IntegerField()
    message = models.CharField(max_length=500, null=True)

    walking = models.IntegerField(default=0)
    bike = models.IntegerField(default=0)
    ebike = models.IntegerField(default=0)
    escooter = models.IntegerField(default=0)
    motorbike = models.IntegerField(default=0)
    car = models.IntegerField(default=0)
    bus = models.IntegerField(default=0)
    busintercity = models.IntegerField(default=0)
    tram = models.IntegerField(default=0)
    train = models.IntegerField(default=0)
    expressrailway = models.IntegerField(default=0)
    subway = models.IntegerField(default=0)
    motorbikepooling = models.IntegerField(default=1)
    carpooling = models.IntegerField(default=1)
    engine = models.CharField(max_length=25, null=True)

    hasWorkingDay2 = models.BooleanField(default=False)
    nWorkingDay2 = models.IntegerField(default=0)

    walking2 = models.IntegerField(default=0)
    bike2 = models.IntegerField(default=0)
    ebike2 = models.IntegerField(default=0)
    escooter2 = models.IntegerField(default=0)
    motorbike2 = models.IntegerField(default=0)
    car2 = models.IntegerField(default=0)
    bus2 = models.IntegerField(default=0)
    busintercity2 = models.IntegerField(default=0)
    tram2 = models.IntegerField(default=0)
    train2 = models.IntegerField(default=0)
    expressrailway2 = models.IntegerField(default=0)
    subway2 = models.IntegerField(default=0)
    motorbikepooling2 = models.IntegerField(default=0)
    carpooling2 = models.IntegerField(default=0)
    engine2 = models.CharField(max_length=25, null=True)

class Travel(models.Model):
    ghgi = models.ForeignKey(GHGI, related_name='travel', on_delete=models.CASCADE)
    location = models.CharField(max_length=250, null=True)
    purpose = models.CharField(max_length=250, null=True)
    status = models.CharField(max_length=250, null=True)
    amount = models.IntegerField()

class TravelNames(models.Model):
    name = models.CharField(max_length=50)
    travel = models.ForeignKey(Travel, related_name='names', on_delete=models.CASCADE)

class TravelSection(models.Model):
    travel = models.ForeignKey(Travel, related_name='sections', on_delete=models.CASCADE)
    type = models.CharField(max_length=2,choices=[('NA', 'Nationale'), ('IN', 'Internationale'), ('MX', 'Mixte')], default='NA')
    isRoundTrip = models.BooleanField(default=True)
    distance = models.FloatField()
    transportation = models.CharField(max_length=250)
    carpooling = models.IntegerField(null=True)

class Vehicle(models.Model):
    name = models.CharField(max_length=250)
    type = models.CharField(max_length=250)
    engine = models.CharField(max_length=250)
    unit = models.CharField(max_length=250)
    power = models.IntegerField(null=True)
    noEngine = models.IntegerField(null=True)
    shp = models.IntegerField(null=True)
    controled = models.BooleanField()
    laboratory = models.ForeignKey('api.Laboratory', related_name='vehicle', on_delete=models.CASCADE)

class Building(models.Model):
    laboratory = models.ForeignKey('api.Laboratory', related_name='building', on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    area = models.FloatField()
    share = models.FloatField()
    selfProduction = models.BooleanField()
    site = models.CharField(max_length=250, null=True)

class Consumption(models.Model):
    january = models.IntegerField(null=True)
    february = models.IntegerField(null=True)
    march = models.IntegerField(null=True)
    april = models.IntegerField(null=True)
    may = models.IntegerField(null=True)
    june = models.IntegerField(null=True)
    july = models.IntegerField(null=True)
    august = models.IntegerField(null=True)
    septembre = models.IntegerField(null=True)
    octobre = models.IntegerField(null=True)
    novembre = models.IntegerField(null=True)
    decembre = models.IntegerField(null=True)
    total = models.IntegerField(null=True)
    isMonthly = models.BooleanField()

    class Meta:
        abstract = True

class VehicleConsumption(Consumption):
    ghgi = models.ForeignKey(GHGI, related_name='consumption', on_delete=models.CASCADE)
    vehicle = models.ForeignKey(Vehicle, related_name='consumption', on_delete=models.CASCADE, null=True)

class Heating(Consumption):
    ghgi = models.ForeignKey(GHGI, related_name='heatings', on_delete=models.CASCADE)
    building = models.ForeignKey(Building, related_name='heatings', on_delete=models.CASCADE, null=True)
    type = models.CharField(max_length=250, null=True)
    urbanNetwork = models.CharField(max_length=250, null=True)
    isOwnedByLab = models.BooleanField()

class Electricity(Consumption):
    ghgi = models.ForeignKey(GHGI, related_name='electricity', on_delete=models.CASCADE)
    building = models.ForeignKey(Building, related_name='electricity', on_delete=models.CASCADE, null=True)

class Refrigerant(models.Model):
    ghgi = models.ForeignKey(GHGI, related_name='refrigerants', on_delete=models.CASCADE)
    building = models.ForeignKey(Building, related_name='refrigerants', on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=50)
    total = models.FloatField(null=True)

class SelfConsumption(models.Model):
    ghgi = models.ForeignKey(GHGI, related_name='selfConsumption', on_delete=models.CASCADE)
    building = models.ForeignKey(Building, related_name='selfConsumption', on_delete=models.CASCADE)
    total = models.IntegerField(null=True)

class ComputerDevice(models.Model):
    ghgi = models.ForeignKey(GHGI, related_name='device', on_delete=models.CASCADE)
    type = models.CharField(max_length=250)
    model = models.CharField(max_length=250, null=True)
    amount = models.IntegerField(null=True)
    acquisitionYear = models.IntegerField(null=True)

class Purchase(models.Model):
    ghgi = models.ForeignKey(GHGI, related_name='purchase', on_delete=models.CASCADE)
    code = models.CharField(max_length=250)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
