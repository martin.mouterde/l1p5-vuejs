import copy

from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status
from django.core.mail import send_mail
from django.conf import settings

from ..api.models import Laboratory
from ..users.models import L1P5User
from .models import Initiative
from .serializers import InitiativeSerializer

@api_view(['POST'])
@permission_classes([])
def save_initiative(request):
    if request.user.is_authenticated:
        initiative_id = request.data.pop('id')
        initiative, created = Initiative.objects.update_or_create(id=initiative_id, defaults=request.data)
        serializer = InitiativeSerializer(initiative)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get_initiative(request):
    if request.user.is_authenticated:
        try:
            laboratory = Laboratory.objects.get(referent_id=request.user.id)
            initiative = Initiative.objects.get(laboratory_id=laboratory.id)
            serializer = InitiativeSerializer(initiative)
            return Response(serializer.data)
        except:
            return Response(Initiative.objects.none())
    return Response(Initiative.objects.none())

@api_view(['GET'])
@permission_classes([])
def get_initiatives(request):
    initatives = Initiative.objects.filter(valid=1)
    serializer = InitiativeSerializer(initatives, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def get_initiatives_admin(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            initatives = Initiative.objects.filter()
            serializer = InitiativeSerializer(initatives, many=True)
            return Response(serializer.data)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def validate_initiative(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            initative = Initiative.objects.update_or_create(id=request.data["id"], defaults={"valid":1})
            send_mail(
                  'Votre initiative Labos 1point5',
                  '''Bonjour {0} {1},

merci pour l'initiative que vous avez proposé, elle est maintenant visible
sur notre site depuis la page https://apps.labos1point5.org/les-initiatives.

A très bientôt,
le collectif Labos 1point5'''.format(request.data["membre"]["prenom"], request.data["membre"]["nom"]),
                  getattr(settings, 'DEFAULT_FROM_EMAIL'),
                  [request.data["membre"]["email"]],
                  fail_silently=False
                  )
            initatives = Initiative.objects.filter(valid=0)
            serializer = InitiativeSerializer(initatives, many=True)
            return Response(serializer.data)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)
