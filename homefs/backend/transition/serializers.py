from rest_framework import serializers
from .models import Initiative

from ..api.serializers import LaboratorySerializer

class InitiativeSerializer(serializers.ModelSerializer):
    laboratory = LaboratorySerializer(many=False)
    class Meta:
        model = Initiative
        fields = '__all__'
