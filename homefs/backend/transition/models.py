from django.db import models

class Initiative(models.Model):
    laboratory = models.ForeignKey('api.Laboratory', related_name='initiative', on_delete=models.CASCADE)
    created = models.DateField(auto_now_add=True)
    last_update = models.DateField(auto_now=True)
    description = models.CharField(max_length=1000, null=True, blank=True)
    initiative_institutionnelle = models.BooleanField(default=False)
    BGESext = models.IntegerField(null=True)
    BGES_annee = models.CharField(max_length=50, null=True, blank=True)
    engagements = models.CharField(max_length=1000, null=True, blank=True)
    groupe_description = models.CharField(max_length=1000, null=True, blank=True)
    deplacementspro_description = models.CharField(max_length=1000, null=True, blank=True)
    deplacementsdt_description = models.CharField(max_length=1000, null=True, blank=True)
    actrecherche_description = models.CharField(max_length=1000, null=True, blank=True)
    batiments_description = models.CharField(max_length=1000, null=True, blank=True)
    numerique_description = models.CharField(max_length=1000, null=True, blank=True)
    ecoresponsable = models.CharField(max_length=1000, null=True, blank=True)
    sociale = models.CharField(max_length=1000, null=True, blank=True)
    enquetes = models.CharField(max_length=1000, null=True, blank=True)
    communication = models.CharField(max_length=1000, null=True, blank=True)
    autres_initiatives = models.CharField(max_length=1000, null=True, blank=True)
    valid = models.BooleanField(default=False)
