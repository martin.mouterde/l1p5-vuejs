
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from .models import Scenario, Variable
from .serializers import ScenarioSerializer

from ..api.models import Laboratory
from ..carbon.models import GHGI

@api_view(['POST'])
def save_scenario(request):
    ghgi_id = request.data.pop('ghgi_id')
    ghgi = GHGI.objects.get(id=ghgi_id)
    scenario_data = request.data.pop('scenario')
    variables_data = scenario_data.pop('variables')
    scenario_data['ghgi'] = ghgi
    scenario_id = scenario_data.pop('id')
    scenario, created = Scenario.objects.update_or_create(id=scenario_id, defaults=scenario_data)
    Variable.objects.filter(scenario=scenario).delete()
    for variable_data in variables_data:
        variable_data['scenario'] = scenario
        Variable.objects.create(**variable_data)
    scenario = Scenario.objects.get(id=scenario.id)
    serializer = ScenarioSerializer(scenario)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def delete_scenario(request):
    scenario_id = request.data.pop('id')
    scenario = Scenario.objects.get(id=scenario_id)
    scenario.delete()
    laboratory = Laboratory.objects.get(referent_id=request.user.id)
    ghgis = GHGI.objects.filter(laboratory_id=laboratory.id)
    scenarios = Scenario.objects.filter(ghgi_id__in=ghgis.values_list("id", flat=True))
    serializer = ScenarioSerializer(scenarios, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def get_scenarios(request):
    laboratory = Laboratory.objects.get(referent_id=request.user.id)
    ghgis = GHGI.objects.filter(laboratory_id=laboratory.id)
    scenarios = Scenario.objects.filter(ghgi_id__in=ghgis.values_list("id", flat=True))
    serializer = ScenarioSerializer(scenarios, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def get_scenario(request):
    uuid = request.data.pop('uuid')
    scenario = Scenario.objects.filter(uuid=uuid)
    serializer = ScenarioSerializer(scenario, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)
