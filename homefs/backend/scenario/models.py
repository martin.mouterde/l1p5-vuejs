import uuid
from django.db import models

from ..carbon.models import GHGI

class Scenario(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(null=True)
    ghgi = models.ForeignKey(GHGI, related_name='scenario', on_delete=models.CASCADE)
    uuid = models.UUIDField(
        null=False,
        unique = True,
        editable = False
    )

    def save(self, *args, **kwargs):
        ''' On save, update uuid and ovoid clash with existing code '''
        if not self.uuid:
            self.uuid = uuid.uuid4()
        return super(Scenario, self).save(*args, **kwargs)

class Variable(models.Model):
    name = models.CharField(max_length=50)
    level1 = models.JSONField(null=True)
    level2 = models.JSONField(null=True)
    scenario = models.ForeignKey(Scenario, related_name='variables', on_delete=models.CASCADE)
