from rest_framework import serializers
from .models import Scenario, Variable

class VariableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Variable
        fields = '__all__'

class ScenarioSerializer(serializers.ModelSerializer):
    variables = VariableSerializer(many=True, read_only=True)
    class Meta:
        model = Scenario
        fields = '__all__'
