- [Informations générales](#informations-générales)
- [Technologies utilisées](#technologies-utilisées)
- [Installation](#installation)
  - [Information importante](#information-importante)
  - [pré-requis](#pré-requis)
  - [Création de l'environnement virtuel python](#création-de-lenvironnement-virtuel-python)
  - [Installation des dépendances](#installation-des-dépendances)
- [Initialisation de la Base De Données (BDD)](#initialisation-de-la-base-de-données-bdd)
  - [Paramétrage des informations de connexion](#paramétrage-des-informations-de-connexion)
  - [Migration des données vers la BDD avec Django](#migration-des-données-vers-la-bdd-avec-django)
- [Compilation](#compilation)
- [Utilisation avec Docker](#utilisation-avec-docker)
- [Documentations et ressources](#documentations-et-ressources)

# Informations générales

GES 1point5, développé par [Labos 1point5](https://labos1point5.org/), est un outil permettant de calculer l’empreinte carbone et de construire le bilan gaz à effet de serre (BGES) réglementaire de votre laboratoire.

A travers cet outil l'objectif est double :

* Mener des études scientifiques relatives à l’empreinte carbone de la recherche publique française (notre champ d’investigation actuel est limité à la France, y compris les DOM-TOM).

* Nourrir la réflexion sur les leviers d'actions permettant de réduire l’impact des activités de recherche sur les émissions de gaz à effet de serre, tant à l'échelle nationale que locale au laboratoire.

# Technologies utilisées

* Django
* MariaDB
* VueJS

# Installation

## Information importante
Sur debian les paquets ```python3-dev default-libmysqlclient-dev build-essential``` sont nécessaires, avant l'installation de pip.

## pré-requis

Les paquets suivants sont nécessaires :

* Python >= 3.6 & < 3.9
* pip
* virtualenv
* npm
* MariaDB

## Création de l'environnement virtuel python
```
virtualenv -p python3 [nom de du virtualenv]
source [nom du virtualenv]/bin/activate
```

## Installation des dépendances

* PIP
```
pip install -r rootfs/opt/l1p5/requirements.txt
```

*  NPM
```
npm install
```

# Initialisation de la Base De Données (BDD)

## Paramétrage des informations de connexion

Le paramétrage de `conf.example.sh` est celui par défaut. La procédure ci-dessous est requise uniquement pour utiliser un paramétrage différent.

Renommer le fichier `conf.example.sh` en `conf.sh`. Ensuite modifier les section `Databases`  et `Django settings` avec vos propres paramètres:
```
# Database
# --------
MARIADB_ROOT_PASSWORD="mypassword"
DATABASE_NAME=l1p5
DATABASE_USER=l1p5
DATABASE_PASSWORD=mypassword
DATABASE_HOST=/var/run/mysqld/mysqld.sock
DATABASE_PORT=3306

...

# Django settings
# ---------------

# Email
EMAIL_HOST=localhost
EMAIL_PORT=25

# WARN: Disable debug mode for production
DEBUG=True

SECRET_KEY=unsafe-secret-key
```
Ensuite il faut générer le fichier de configuration pour Django avec la commande: `./generate_django_settings.sh`
Enfin il faut créer et initialiser la base de données sur MariaDB avec ces mêmes informations.

## Migration des données vers la BDD avec Django
On  peut maintenant migrer les les données vers la BDD, via l'ORM de Django avec la commande :
```
cd homefs
python manage.py migrate
```

# Compilation

* Compilation et changement à chaud (hot-reloads) :
    ```
    cd homefs
    npm run serve
    ```
* Compilation et minification des ressources (prod) :

    ```
    npm run build
    python manage.py runserver
    ```
* Mettre en évidence les erreurs :
    ```
    npm run lint
    ```

# Utilisation avec Docker

En production, il est plus simple d'exécuter le projet sous forme d'un conteneur Docker: [Accès à la documentation Docker](./doc/docker.md)

# Documentations et ressources
* Documentation Django : [https://www.djangoproject.com](https://www.djangoproject.com/)
* Documentation MariaDB : [https://devdocs.io/mariadb/](https://devdocs.io/mariadb/)
* Documentation VueJS : [https://vuejs.org/guide/introduction.html](https://vuejs.org/guide/introduction.html)

