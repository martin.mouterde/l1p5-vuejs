#!/bin/bash

set -euxo pipefail

echo "Configure apache"
a2enmod remoteip rewrite wsgi
a2enconf remoteip
# Use l1p5 website as unique VirtuaHost
a2dissite 000-default
a2ensite labo15-au1
