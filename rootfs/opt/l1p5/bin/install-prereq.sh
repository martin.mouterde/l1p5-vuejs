#!/bin/bash

set -euxo pipefail

apt-get update
apt-get dist-upgrade -y
echo "Install Apache and mariadb"
apt-get install -y apache2 \
  libapache2-mod-wsgi-py3 \
  python3-pip \
  libmariadb-dev

mkdir /var/run/mysqld