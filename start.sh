#!/bin/bash

# Run docker containers containing l1p5 website and database

# @author  Fabrice Jammes

set -euo pipefail

DIR=$(cd "$(dirname "$0")"; pwd -P)

if [ -e "$DIR/conf.sh" ]; then
    . $DIR/conf.sh
else
    error "Missing $DIR/conf.sh file, create it by renaming $DIR/conf.example.sh"
fi
. $DIR/include.sh

command=""
dev=false
mounts=""
readonly workdir="/home/labo15-au1"
readonly django_settings_file="$DIR/homefs/django-settings.txt"

usage() {
    cat << EOD
Usage: $(basename "$0") [options]
Available options:
  -h            This message
  -d            development mode: local homefs/ directory is mounted inside the l1p5 container

Run docker container containing l1p5 website and optionally its database
EOD
}

fail_if_started () {
    local containers=$(docker ps -a --format "{{.Names}}" --filter "label=$CONTAINER_L1P5")
    if [ -n "$containers" ]
    then
        error "l1p5 website and/or database already started. Run stop.sh first."
    fi
}

# Get the options
while getopts hd c ; do
    case $c in
        h) usage ; exit 0 ;;
        d) dev=true ;;
        \?) usage ; exit 2 ;;
    esac
done
shift "$((OPTIND-1))"

if [ $# -ne 0 ] ; then
    usage
    exit 2
fi

fail_if_started

echo "oOoOoOoOoOoOoOoOo"
echo "  l1p5 database"
echo "oOoOoOoOoOoOoOoOo"

if [ "$EXTERNAL_DB_SOCKET" = false ]; then

  echo "Create data volumes:"
  docker volume create l1p5-mariadb-socket --label "$L1P5"
  docker volume create l1p5-mariadb-data --label "$L1P5"

  echo "Start "$CONTAINER_L1P5_DB" container:"
  docker run --name "$CONTAINER_L1P5_DB" -e MARIADB_ROOT_PASSWORD="$MARIADB_ROOT_PASSWORD" \
    --label "$L1P5" \
    -v l1p5-mariadb-socket:/var/run/mysqld \
    -v l1p5-mariadb-data:/var/lib/mysql \
    -v $DIR/mariadb/init.sql:/init.sql:z \
    -d mariadb:10.9.2 --port 3306 \
    --init-file=/init.sql \
  || error "Failed start $CONTAINER_L1P5_DB"
else
    echo "Use external l1p5 database (host: $DATABASE_HOST, port: $DATABASE_PORT)"
fi

echo
echo "oOoOoOoOoOoOoOoO"
echo "  l1p5 website"
echo "oOoOoOoOoOoOoOoO"

$DIR/generate_django_settings.sh

if [ "$dev" = true ]; then
    echo "Running l1p5 in development mode"
    echo "Local source directory ($DIR/homefs) mounted inside l1p5 container ($workdir)"
    mounts="$mounts --volume $DIR/homefs:$workdir"
    command="$workdir/devstart.sh"
else
    # In development mode, Django settings are mounted through homefs/ mount
    mounts="$mounts --volume $django_settings_file:$workdir/django-settings.txt:ro"
fi

if [ "$NGINX_REVERSE_PROXY" = true ]; then
    net_opt="--net $DOCKER_NETWORK -e VIRTUAL_HOST=$VIRTUAL_HOST"
    msg_access="Access l1p5 website through nginx reverse proxy (NET: $DOCKER_NETWORK, VIRTUAL_HOST: $VIRTUAL_HOST)"
else
    net_opt="-p $PORT:80"
    msg_access="Access l1p5 website on http://localhost:$PORT"
fi

mounts="$mounts --volume $LOG_DIR:/var/log/apache2"

if [ "$EXTERNAL_DB_SOCKET" = true ]; then
    # if DATABASE_HOST is a socket, we mount it
    if [ -S "$DATABASE_HOST" ]; then
        #error "miss l1p5 database socket: $DATABASE_HOST"
        mounts="$mounts --volume $DATABASE_HOST:$DATABASE_HOST"
    fi
else
    container_path=$(dirname $DATABASE_HOST)
    mounts="$mounts --volume l1p5-mariadb-socket:$container_path"
fi

echo "Start "$CONTAINER_L1P5" website container:"
docker run --detach \
  --label "$L1P5" \
  --name "$CONTAINER_L1P5" \
  $mounts \
  $net_opt \
  --security-opt no-new-privileges \
  -- "$IMAGE" \
  $command \
  || error "Fail to start $CONTAINER_L1P5"

echo "$msg_access"
echo "Access l1p5 website logfiles at $LOG_DIR"

