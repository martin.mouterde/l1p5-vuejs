# Run l1p5-vuejs with Docker

## Pre-requisites

- Ubuntu LTS is recommended
- Internet access
- `sudo` access
- Install dependencies below:
```shell
sudo apt-get install docker.io
# then add current user to docker group
sudo usermod -a -G docker <USER>
# Run command below, or restart desktop session
newgrp docker
```
- Clone source code:
```shell
git clone https://framagit.org/labos1point5/l1p5-vuejs.git
cd l1p5-vuejs
```

## Build l1p5-vuejs image

```
./build-image.sh
```

## Run l1p5-vuejs

Spawn a container running the l1p5 website.
- Image name is `labo15appswsgi:latest`
- Container name is `l1p5apps`
- Apache2 logs are stored on docker host, inside `/var/log/l1p5-apps`

### Configuration

Rename `./conf.example.sh` to `./conf.sh` and then set application/database parameters inside it.
Documentation is written inside `./conf.sh` comments.

### Production

Use variables `NGINX_REVERSE_PROXY` and `EXTERNAL_DB_SOCKET` in `./conf.sh` to configure website and database accesses, and then run:

```shell
./start.sh
```

### Development

```shell
./start.sh -d
```
- Mount local source code directory (`homefs`) inside container, at `home/l1p5-au`
- Build source code and launch Apache2
- `build.sh` allow the to rebuild the code inside the container (Apache2 reload might be required)


Set-up a virtual environment on the Docker host:
```shell
sudo apt-get install -y libmariadb-dev python3.10 python3.10-dev python3.10-venv
python3.10 -m venv $PWD/envl1p5
pip3.10 install -r $PWD/rootfs/opt/l1p5/requirements.txt
```

### Stop application

```shell
./stop.sh
```

### Troubleshooting

- Open a shell inside the container:
```
docker exec -it l1p5apps bash
```

- Reload Apache2 configuration inside the container:
```
docker exec -it -- l1p5apps service apache2 reload
```

## Stop l1p5-vuejs
```
docker rm -f l1p5apps
```

## Access the website locally
```
curl http://localhost:8181
```

## Manage the database

When running the database inside a container, the data is stored inside volumes retrievable with the following command:

```shell
docker volume ls --filter "label=l1p5apps"
```

Volume `l1p5-mariadb-data` store the Mariadb data and `l1p5-mariadb-socket` store the Mariadb file socket, used by container `l1p5apps` to access the database.

### Initialize/upgrade the database

```
docker exec -t -- l1p5apps  python3 manage.py migrate
```

### Remove the database volumes/data

```
./stop.sh
./delete_volumes.sh
```

### Create a use in developer mode

In developer mode, access to a `smtp` server might be impossible. If so, the following hack allow to create a user account:

1. Access the l1p5 website at [http://localhost:8182](http://localhost:8182), and click on the upper right button to sign-up as a new user.

2. Then update the database to enable this new user account without email verification:

```shell
docker exec -it -- l1p5apps-db mysql -pmypassword -uroot -e "update l1p5.users_l1p5user set is_active=1 where id=1;"
# Retrieve use account details
docker exec -it -- l1p5apps-db mysql -pmypassword -uroot -e "select * from l1p5.users_l1p5user;"
```
